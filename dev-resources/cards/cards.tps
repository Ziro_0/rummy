<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>5.3.0</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>phaser-json-array</string>
        <key>textureFileName</key>
        <filename>../../src/assets/game-rummy/images/cards.png</filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>1024</int>
            <key>height</key>
            <int>1024</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../src/assets/game-rummy/images/cards.json</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <true/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Trim</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">card_10c.png</key>
            <key type="filename">card_10d.png</key>
            <key type="filename">card_10h.png</key>
            <key type="filename">card_10s.png</key>
            <key type="filename">card_2c.png</key>
            <key type="filename">card_2d.png</key>
            <key type="filename">card_2h.png</key>
            <key type="filename">card_2s.png</key>
            <key type="filename">card_3c.png</key>
            <key type="filename">card_3d.png</key>
            <key type="filename">card_3h.png</key>
            <key type="filename">card_3s.png</key>
            <key type="filename">card_4c.png</key>
            <key type="filename">card_4d.png</key>
            <key type="filename">card_4h.png</key>
            <key type="filename">card_4s.png</key>
            <key type="filename">card_5c.png</key>
            <key type="filename">card_5d.png</key>
            <key type="filename">card_5h.png</key>
            <key type="filename">card_5s.png</key>
            <key type="filename">card_6c.png</key>
            <key type="filename">card_6d.png</key>
            <key type="filename">card_6h.png</key>
            <key type="filename">card_6s.png</key>
            <key type="filename">card_7c.png</key>
            <key type="filename">card_7d.png</key>
            <key type="filename">card_7h.png</key>
            <key type="filename">card_7s.png</key>
            <key type="filename">card_8c.png</key>
            <key type="filename">card_8d.png</key>
            <key type="filename">card_8h.png</key>
            <key type="filename">card_8s.png</key>
            <key type="filename">card_9c.png</key>
            <key type="filename">card_9d.png</key>
            <key type="filename">card_9h.png</key>
            <key type="filename">card_9s.png</key>
            <key type="filename">card_ac.png</key>
            <key type="filename">card_ad.png</key>
            <key type="filename">card_ah.png</key>
            <key type="filename">card_as.png</key>
            <key type="filename">card_back_red.png</key>
            <key type="filename">card_jc.png</key>
            <key type="filename">card_jd.png</key>
            <key type="filename">card_jh.png</key>
            <key type="filename">card_js.png</key>
            <key type="filename">card_kc.png</key>
            <key type="filename">card_kd.png</key>
            <key type="filename">card_kh.png</key>
            <key type="filename">card_ks.png</key>
            <key type="filename">card_qc.png</key>
            <key type="filename">card_qd.png</key>
            <key type="filename">card_qh.png</key>
            <key type="filename">card_qs.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>25,36,50,73</rect>
                <key>scale9Paddings</key>
                <rect>25,36,50,73</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">deck.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>27,38,54,77</rect>
                <key>scale9Paddings</key>
                <rect>27,38,54,77</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>card_10c.png</filename>
            <filename>card_10d.png</filename>
            <filename>card_10h.png</filename>
            <filename>card_10s.png</filename>
            <filename>card_2c.png</filename>
            <filename>card_2d.png</filename>
            <filename>card_2h.png</filename>
            <filename>card_2s.png</filename>
            <filename>card_3c.png</filename>
            <filename>card_3d.png</filename>
            <filename>card_3h.png</filename>
            <filename>card_3s.png</filename>
            <filename>card_4c.png</filename>
            <filename>card_4d.png</filename>
            <filename>card_4h.png</filename>
            <filename>card_4s.png</filename>
            <filename>card_5c.png</filename>
            <filename>card_5d.png</filename>
            <filename>card_5h.png</filename>
            <filename>card_5s.png</filename>
            <filename>card_6c.png</filename>
            <filename>card_6d.png</filename>
            <filename>card_6h.png</filename>
            <filename>card_6s.png</filename>
            <filename>card_7c.png</filename>
            <filename>card_7d.png</filename>
            <filename>card_7h.png</filename>
            <filename>card_7s.png</filename>
            <filename>card_8c.png</filename>
            <filename>card_8d.png</filename>
            <filename>card_8h.png</filename>
            <filename>card_8s.png</filename>
            <filename>card_9c.png</filename>
            <filename>card_9d.png</filename>
            <filename>card_9h.png</filename>
            <filename>card_9s.png</filename>
            <filename>card_ac.png</filename>
            <filename>card_ad.png</filename>
            <filename>card_ah.png</filename>
            <filename>card_as.png</filename>
            <filename>card_back_red.png</filename>
            <filename>card_jc.png</filename>
            <filename>card_jd.png</filename>
            <filename>card_jh.png</filename>
            <filename>card_js.png</filename>
            <filename>card_kc.png</filename>
            <filename>card_kd.png</filename>
            <filename>card_kh.png</filename>
            <filename>card_ks.png</filename>
            <filename>card_qc.png</filename>
            <filename>card_qd.png</filename>
            <filename>card_qh.png</filename>
            <filename>card_qs.png</filename>
            <filename>deck.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
