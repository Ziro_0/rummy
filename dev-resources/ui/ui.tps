<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>4</int>
        <key>texturePackerVersion</key>
        <string>5.4.0</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>phaser-json-array</string>
        <key>textureFileName</key>
        <filename>../../src/assets/game-rummy/images/ui.png</filename>
        <key>flipPVR</key>
        <false/>
        <key>pvrCompressionQuality</key>
        <enum type="SettingsBase::PvrCompressionQuality">PVR_QUALITY_NORMAL</enum>
        <key>atfCompressData</key>
        <false/>
        <key>mipMapMinSize</key>
        <uint>32768</uint>
        <key>etc1CompressionQuality</key>
        <enum type="SettingsBase::Etc1CompressionQuality">ETC1_QUALITY_LOW_PERCEPTUAL</enum>
        <key>etc2CompressionQuality</key>
        <enum type="SettingsBase::Etc2CompressionQuality">ETC2_QUALITY_LOW_PERCEPTUAL</enum>
        <key>dxtCompressionMode</key>
        <enum type="SettingsBase::DxtCompressionMode">DXT_PERCEPTUAL</enum>
        <key>jxrColorFormat</key>
        <enum type="SettingsBase::JpegXrColorMode">JXR_YUV444</enum>
        <key>jxrTrimFlexBits</key>
        <uint>0</uint>
        <key>jxrCompressionLevel</key>
        <uint>0</uint>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>0</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>1</uint>
        <key>webpQualityLevel</key>
        <uint>101</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>atfFormats</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>0</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>2048</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">AnySize</enum>
            <key>forceSquared</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
            <key>polygon</key>
            <struct type="AlgorithmPolygonSettings">
                <key>alignToGrid</key>
                <uint>1</uint>
            </struct>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>../../src/assets/game-rummy/images/ui.json</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>alphaHandling</key>
        <enum type="SettingsBase::AlphaHandling">ClearTransparentPixels</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <true/>
        <key>prependSmartFolderName</key>
        <false/>
        <key>autodetectAnimations</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>extrude</key>
            <uint>1</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMargin</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Trim</enum>
            <key>tracerTolerance</key>
            <int>200</int>
            <key>heuristicMask</key>
            <false/>
            <key>defaultPivotPoint</key>
            <point_f>0.5,0.5</point_f>
            <key>writePivotPoints</key>
            <false/>
        </struct>
        <key>individualSpriteSettings</key>
        <map type="IndividualSpriteSettingsMap">
            <key type="filename">../meld-icons/meld-icon-10-black.png</key>
            <key type="filename">../meld-icons/meld-icon-10-red.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>9,7,17,13</rect>
                <key>scale9Paddings</key>
                <rect>9,7,17,13</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">../meld-icons/meld-icon-2-black.png</key>
            <key type="filename">../meld-icons/meld-icon-2-red.png</key>
            <key type="filename">../meld-icons/meld-icon-3-black.png</key>
            <key type="filename">../meld-icons/meld-icon-3-red.png</key>
            <key type="filename">../meld-icons/meld-icon-4-black.png</key>
            <key type="filename">../meld-icons/meld-icon-4-red.png</key>
            <key type="filename">../meld-icons/meld-icon-5-black.png</key>
            <key type="filename">../meld-icons/meld-icon-5-red.png</key>
            <key type="filename">../meld-icons/meld-icon-6-black.png</key>
            <key type="filename">../meld-icons/meld-icon-6-red.png</key>
            <key type="filename">../meld-icons/meld-icon-7-black.png</key>
            <key type="filename">../meld-icons/meld-icon-7-red.png</key>
            <key type="filename">../meld-icons/meld-icon-8-black.png</key>
            <key type="filename">../meld-icons/meld-icon-8-red.png</key>
            <key type="filename">../meld-icons/meld-icon-9-black.png</key>
            <key type="filename">../meld-icons/meld-icon-9-red.png</key>
            <key type="filename">../meld-icons/meld-icon-a-black.png</key>
            <key type="filename">../meld-icons/meld-icon-a-red.png</key>
            <key type="filename">../meld-icons/meld-icon-c.png</key>
            <key type="filename">../meld-icons/meld-icon-d.png</key>
            <key type="filename">../meld-icons/meld-icon-h.png</key>
            <key type="filename">../meld-icons/meld-icon-j-black.png</key>
            <key type="filename">../meld-icons/meld-icon-j-red.png</key>
            <key type="filename">../meld-icons/meld-icon-k-black.png</key>
            <key type="filename">../meld-icons/meld-icon-k-red.png</key>
            <key type="filename">../meld-icons/meld-icon-q-black.png</key>
            <key type="filename">../meld-icons/meld-icon-q-red.png</key>
            <key type="filename">../meld-icons/meld-icon-s.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>6,7,13,13</rect>
                <key>scale9Paddings</key>
                <rect>6,7,13,13</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">background.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>192,256,384,512</rect>
                <key>scale9Paddings</key>
                <rect>192,256,384,512</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">buttons/game_button_back_down.png</key>
            <key type="filename">buttons/game_button_back_norm.png</key>
            <key type="filename">buttons/game_button_back_over.png</key>
            <key type="filename">buttons/game_button_cancel_meld_down.png</key>
            <key type="filename">buttons/game_button_cancel_meld_norm.png</key>
            <key type="filename">buttons/game_button_cancel_meld_over.png</key>
            <key type="filename">buttons/game_button_close_down.png</key>
            <key type="filename">buttons/game_button_close_norm.png</key>
            <key type="filename">buttons/game_button_close_over.png</key>
            <key type="filename">buttons/game_button_deal_down.png</key>
            <key type="filename">buttons/game_button_deal_norm.png</key>
            <key type="filename">buttons/game_button_deal_over.png</key>
            <key type="filename">buttons/game_button_finish_meld_disabled.png</key>
            <key type="filename">buttons/game_button_finish_meld_down.png</key>
            <key type="filename">buttons/game_button_finish_meld_norm.png</key>
            <key type="filename">buttons/game_button_finish_meld_over.png</key>
            <key type="filename">buttons/game_button_meld_lay_off_disabled.png</key>
            <key type="filename">buttons/game_button_meld_lay_off_down.png</key>
            <key type="filename">buttons/game_button_meld_lay_off_norm.png</key>
            <key type="filename">buttons/game_button_meld_lay_off_over.png</key>
            <key type="filename">buttons/game_button_next_down.png</key>
            <key type="filename">buttons/game_button_next_norm.png</key>
            <key type="filename">buttons/game_button_next_over.png</key>
            <key type="filename">buttons/game_button_skip_down.png</key>
            <key type="filename">buttons/game_button_skip_norm.png</key>
            <key type="filename">buttons/game_button_skip_over.png</key>
            <key type="filename">buttons/game_button_sort_hand_disabled.png</key>
            <key type="filename">buttons/game_button_sort_hand_down.png</key>
            <key type="filename">buttons/game_button_sort_hand_norm.png</key>
            <key type="filename">buttons/game_button_sort_hand_over.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>53,15,106,29</rect>
                <key>scale9Paddings</key>
                <rect>53,15,106,29</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">message-text-panel.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>187,95,374,190</rect>
                <key>scale9Paddings</key>
                <rect>187,95,374,190</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">time-bar-back.png</key>
            <key type="filename">time-bar-front.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>31,5,61,9</rect>
                <key>scale9Paddings</key>
                <rect>31,5,61,9</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">time-bar-borders.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>32,6,64,12</rect>
                <key>scale9Paddings</key>
                <rect>32,6,64,12</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
            <key type="filename">tutorial-arrow.png</key>
            <struct type="IndividualSpriteSettings">
                <key>pivotPoint</key>
                <point_f>0.5,0.5</point_f>
                <key>spriteScale</key>
                <double>1</double>
                <key>scale9Enabled</key>
                <false/>
                <key>scale9Borders</key>
                <rect>24,23,48,46</rect>
                <key>scale9Paddings</key>
                <rect>24,23,48,46</rect>
                <key>scale9FromFile</key>
                <false/>
            </struct>
        </map>
        <key>fileList</key>
        <array>
            <filename>background.png</filename>
            <filename>../meld-icons/meld-icon-10-black.png</filename>
            <filename>../meld-icons/meld-icon-10-red.png</filename>
            <filename>../meld-icons/meld-icon-2-black.png</filename>
            <filename>../meld-icons/meld-icon-2-red.png</filename>
            <filename>../meld-icons/meld-icon-3-black.png</filename>
            <filename>../meld-icons/meld-icon-3-red.png</filename>
            <filename>../meld-icons/meld-icon-4-black.png</filename>
            <filename>../meld-icons/meld-icon-4-red.png</filename>
            <filename>../meld-icons/meld-icon-5-black.png</filename>
            <filename>../meld-icons/meld-icon-5-red.png</filename>
            <filename>../meld-icons/meld-icon-6-black.png</filename>
            <filename>../meld-icons/meld-icon-6-red.png</filename>
            <filename>../meld-icons/meld-icon-7-black.png</filename>
            <filename>../meld-icons/meld-icon-7-red.png</filename>
            <filename>../meld-icons/meld-icon-8-black.png</filename>
            <filename>../meld-icons/meld-icon-8-red.png</filename>
            <filename>../meld-icons/meld-icon-9-black.png</filename>
            <filename>../meld-icons/meld-icon-9-red.png</filename>
            <filename>../meld-icons/meld-icon-a-black.png</filename>
            <filename>../meld-icons/meld-icon-a-red.png</filename>
            <filename>../meld-icons/meld-icon-c.png</filename>
            <filename>../meld-icons/meld-icon-d.png</filename>
            <filename>../meld-icons/meld-icon-h.png</filename>
            <filename>../meld-icons/meld-icon-j-black.png</filename>
            <filename>../meld-icons/meld-icon-j-red.png</filename>
            <filename>../meld-icons/meld-icon-k-black.png</filename>
            <filename>../meld-icons/meld-icon-k-red.png</filename>
            <filename>../meld-icons/meld-icon-q-black.png</filename>
            <filename>../meld-icons/meld-icon-q-red.png</filename>
            <filename>../meld-icons/meld-icon-s.png</filename>
            <filename>time-bar-back.png</filename>
            <filename>time-bar-borders.png</filename>
            <filename>time-bar-front.png</filename>
            <filename>buttons/game_button_back_down.png</filename>
            <filename>buttons/game_button_back_norm.png</filename>
            <filename>buttons/game_button_back_over.png</filename>
            <filename>buttons/game_button_cancel_meld_down.png</filename>
            <filename>buttons/game_button_cancel_meld_norm.png</filename>
            <filename>buttons/game_button_cancel_meld_over.png</filename>
            <filename>buttons/game_button_close_down.png</filename>
            <filename>buttons/game_button_close_norm.png</filename>
            <filename>buttons/game_button_close_over.png</filename>
            <filename>buttons/game_button_deal_down.png</filename>
            <filename>buttons/game_button_deal_norm.png</filename>
            <filename>buttons/game_button_deal_over.png</filename>
            <filename>buttons/game_button_finish_meld_disabled.png</filename>
            <filename>buttons/game_button_finish_meld_down.png</filename>
            <filename>buttons/game_button_finish_meld_norm.png</filename>
            <filename>buttons/game_button_finish_meld_over.png</filename>
            <filename>buttons/game_button_meld_lay_off_disabled.png</filename>
            <filename>buttons/game_button_meld_lay_off_down.png</filename>
            <filename>buttons/game_button_meld_lay_off_norm.png</filename>
            <filename>buttons/game_button_meld_lay_off_over.png</filename>
            <filename>buttons/game_button_next_down.png</filename>
            <filename>buttons/game_button_next_norm.png</filename>
            <filename>buttons/game_button_next_over.png</filename>
            <filename>buttons/game_button_sort_hand_disabled.png</filename>
            <filename>buttons/game_button_sort_hand_down.png</filename>
            <filename>buttons/game_button_sort_hand_norm.png</filename>
            <filename>buttons/game_button_sort_hand_over.png</filename>
            <filename>message-text-panel.png</filename>
            <filename>buttons/game_button_skip_down.png</filename>
            <filename>buttons/game_button_skip_norm.png</filename>
            <filename>buttons/game_button_skip_over.png</filename>
            <filename>tutorial-arrow.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
        <key>packNormalMaps</key>
        <false/>
        <key>autodetectNormalMaps</key>
        <true/>
        <key>normalMapFilter</key>
        <string></string>
        <key>normalMapSuffix</key>
        <string></string>
        <key>normalMapSheetFileName</key>
        <filename></filename>
        <key>exporterProperties</key>
        <map type="ExporterProperties"/>
    </struct>
</data>
