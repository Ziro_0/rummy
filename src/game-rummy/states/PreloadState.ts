export default class PreloadState {
    game: Phaser.Game;
    preloadBar: Phaser.Image;

    //=========================================================================
    // constructor
    //=========================================================================
    /** @constructor */
    constructor(game: Phaser.Game) {
        this.game = game;
    }

    //=========================================================================
    // public
    //=========================================================================

    //-------------------------------------------------------------------------
    create() {
        this.game.state.start('GameState');
    }

    //-------------------------------------------------------------------------
    preload() {
        this.preloadBar = this.game.add.image(
            this.game.world.centerX,
            this.game.world.centerY,
            'preload_bar');

        this.game.load.setPreloadSprite(this.preloadBar);

        this._loadJson('audio');
        this._loadJson('metrics');
        this._loadJson('tutorialSteps');

        this._loadAtlasJsonArray('cards');
        this._loadAtlasJsonArray('ui');

        this._loadAudio('sndButtonPress');
        this._loadAudio('sndDealCard0');
        this._loadAudio('sndDealCard1');
        this._loadAudio('sndDealCard2');
        this._loadAudio('sndDiscard');
        this._loadAudio('sndDraw1');
        this._loadAudio('sndDraw2');
        this._loadAudio('sndLayOffMeld0');
        this._loadAudio('sndLayOffMeld1');
        this._loadAudio('sndNewMeld0');
        this._loadAudio('sndNewMeld1');
        this._loadAudio('sndNewMeld2');
        this._loadAudio('sndNewMeld3');
        this._loadAudio('sndPlayerLoses');
        this._loadAudio('sndPlayerTimeOver');
        this._loadAudio('sndPlayerWins');
        this._loadAudio('sndShuffleDeck');

        this.game.load.bitmapFont('modenine',
            'assets/fonts/modenine.png',
            'assets/fonts/modenine.xml');

        this.game.load.bitmapFont('krutiDev010Regular',
            'assets/fonts/Kruti-Dev-010-Regular.png',
            'assets/fonts/Kruti-Dev-010-Regular.xml');

        this.game.load.bitmapFont('raleway-regular',
            'assets/fonts/raleway-regular.png',
            'assets/fonts/raleway-regular.xml');

        this.game.load.image('help-page-0', 'assets/game-rummy/images/help-page-0.png');
        this.game.load.image('help-page-1', 'assets/game-rummy/images/help-page-1.png');
        this.game.load.image('help-page-2', 'assets/game-rummy/images/help-page-2.png');
        this.game.load.image('help-page-3', 'assets/game-rummy/images/help-page-3.png');
    }

    //=========================================================================
    // private
    //=========================================================================

    //-------------------------------------------------------------------------
    _loadAtlasJsonArray(key: string) {
        const path = 'assets/game-rummy/images/';
        this.game.load.atlasJSONArray(key,
            `${path}${key}.png`,
            `${path}${key}.json`);
    }

    //-------------------------------------------------------------------------
    _loadAudio(key: string) {
        const path = 'assets/game-rummy/audio/';
        this.game.load.audio(key,
            [
                `${path}${key}.mp3`,
                `${path}${key}.ogg`
            ]);
    };

    //-------------------------------------------------------------------------
    _loadJson(key: string) {
        const path = 'assets/game-rummy/data/';
        this.game.load.json(key, `${path}${key}.json`);
    }        
}
