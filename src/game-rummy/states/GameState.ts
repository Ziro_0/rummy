import MainView from "../classes/MainView";
import { Game } from "../game";
import CardsManager from "../classes/CardsManager";
import PlayerData from "../classes/PlayerData";
import CardImage from "../classes/CardImage";
import IGameState from "../interfaces/IGameState";
import MeldPrepLogic from "../classes/MeldPrepLogic";
import CardData from "../classes/CardData";
import Melds from "../classes/Melds";
import MeldImage from "../classes/MeldImage";
import Ai from "../classes/ai/Ai";
import { JsonKey } from "../classes/JsonKey";
import AudioPlayer from "../classes/audio/AudioPlayer";
import { AudioEventKey } from "../classes/audio/AudioEventKey";
import TurnTimer from "../classes/TurnTimer";
import TimerBar from "../classes/TimerBar";
import { MeldType } from "../classes/MeldType";
import CardValuePoints from "../classes/CardValuePoints";
import { MainViewbuttonId } from '../classes/MainViewbuttonId';
import Button from '../classes/Button';

export default class GameState implements IGameState {
    static readonly THIS_PLAYER_INDEX = 0;
    static readonly AI_PLAYER_INDEX = 1;

    game: Game;
    
    onCardImageInputDownSgn: Phaser.Signal;
    onDeckInputDownSgn: Phaser.Signal;
    onDiscardCardImageInputDownSgn: Phaser.Signal;

    _playersData: PlayerData[];

    _mainView: MainView;
    _cardsManager: CardsManager;
    _meldPrepLogic: MeldPrepLogic;
    _audioPlayer: AudioPlayer;
    _ai: Ai;
    _turnTimer: TurnTimer;
    _timerBar: TimerBar;

    _timeExpiredDelayTimer: Phaser.Timer;
    _autoDealTimer: Phaser.Timer;

    _turnPlayerIndex: number;
    _riggedInPlayersFavorIndex: number;
    _numDeckRecycles: number;
    _firstPlayerToCreateMeldThisRound: number;
    _playerTurnStartTime: number;

    _canTurnPlayerSelectCardFromHand: boolean;
    _canTurnPlayerDraw: boolean;
    _canTurnPlayerPickFromDiscard: boolean;
    _isMeldMode: boolean;
    _didTurnPlayerDrawOrPickDiscard: boolean;
    _hasTurnPlayerTimeExpired: boolean;
    _isDealingOpeningHands: boolean;
    _isGameOver: boolean;

    //=========================================================================
    // constructor
    //=========================================================================
    /** @constructor */
    constructor(game: Game) {
        this.game = game;
    }

    //=========================================================================
    // public
    //=========================================================================
    
    //-------------------------------------------------------------------------
    addDiscardedCardToHand() {
        this._turnTimer.paused = true;
        this._cardsManager.onCardsMoveComplete.addOnce(
            this._onCardsManagerAddDiscardedCardToHandComplete, this);

        this._cardsManager.addDiscardedCardToHand(this._turnPlayerIndex);
    }

    //-------------------------------------------------------------------------
    addMeld(cardsData: CardData[], meldType: MeldType, targetMeld?: MeldImage): MeldImage {
        return(this._mainView.addMeld(cardsData, meldType, targetMeld));
    }

    //-------------------------------------------------------------------------
    get audioJsonData(): any {
        return(this.game.cache.getJSON(JsonKey.AUDIO));
    }

    //-------------------------------------------------------------------------
    get canSeeOtherPlayersCards(): boolean {
        return(this.game.config.debugCanSeeOtherPlayersCards);
    }

    //-------------------------------------------------------------------------
    get canControlAi(): boolean {
        return(this.game.config.debugCanControlAi);
    }

    //-------------------------------------------------------------------------
    get cardsGroup(): Phaser.Group {
        return(this._mainView.cardsGroup);
    }

    //-------------------------------------------------------------------------
    get cardsManager(): CardsManager {
        return(this._cardsManager);
    }

    //-------------------------------------------------------------------------
    create() {
        console.log('Game state [create] started');
        this._init();
        this._callListenerMap('ready', this.game);
    }

    //-------------------------------------------------------------------------
    get deckAndDiscardGroup(): Phaser.Group {
        return(this._mainView.deckAndDiscardGroup);
    }
    
    //-------------------------------------------------------------------------
    discardCardFromTurnPlayer(cardImage: CardImage) {
        const AUTO_PLAY = true;
        this._turnTimer.paused = true;
        
        const playerIndex: number = this.turnPlayerIndex;
        this._cardsManager.discardCardFromPlayer(playerIndex, cardImage, AUTO_PLAY);
        this._cardsManager.onCardsMoveComplete.addOnce(
            this._onCardsManagerDiscardPlayerCardComplete, this);
    }

    //-------------------------------------------------------------------------
    drawCard() {
       const AUTO_PLAY = true;
       const PLAY_SOUND = true;
       const useRigger = this.isRiggedInPlayersFavor !== null;
       this._cardsManager.onCardsMoveComplete.addOnce(
           this._onCardsManagerDrawCardCompleteComplete, this);
       this._cardsManager.drawCard(this._turnPlayerIndex, AUTO_PLAY, PLAY_SOUND, useRigger);
    }

    //-------------------------------------------------------------------------
    get discardPilePosition(): Phaser.Point {
        return(this._mainView.discardPilePosition);
    }
    
    //-------------------------------------------------------------------------
    endOfRoundProc(tutorialLosingPlayerIndex?: number) {
        this._revealAllPlayerCards(tutorialLosingPlayerIndex);
        this._updatePlayerScores(tutorialLosingPlayerIndex);
    }

    //-------------------------------------------------------------------------
    set finishMeldButtonVisible(value: boolean) {
        this._mainView.finishMeldButtonVisible = value;
    }

    //-------------------------------------------------------------------------
    getCardImage(cardData: CardData): CardImage {
        return(this._cardsManager.getCardImage(cardData));
    }

    //-------------------------------------------------------------------------
    getMainViewButton(buttonId: MainViewbuttonId): Button {
        return(this._mainView.getButton(buttonId));
    }

    //-------------------------------------------------------------------------
    getMeldImage(meldIndex: number): MeldImage {
        return(this._mainView.melds.getMeldImage(meldIndex));
    }

    //-------------------------------------------------------------------------
    getPlayerDataAt(index: number): PlayerData {
        return(this._playersData[index]);
    }

    //-------------------------------------------------------------------------
    handleMeldFinished(existingMeld?: MeldImage) {
        if (existingMeld) {
            this._layOffMeld(existingMeld);
        } else {
            this._createMeldFromPrep();
        }
    }

    //-------------------------------------------------------------------------
    get isDealingOpeningHands(): boolean {
        return(this._isDealingOpeningHands);
    }

    //-------------------------------------------------------------------------
    get isRiggedInPlayersFavor(): boolean {
        if (typeof this.game.config.isRiggedInPlayersFavor !== 'boolean') {
            return(null);
        }

        const riggedPlayersFavorArray = this.riggedPlayersFavorArray;
        return(riggedPlayersFavorArray ?
            riggedPlayersFavorArray[this._riggedInPlayersFavorIndex] :
            null);
    }

    //-------------------------------------------------------------------------
    isThisPlayersTurn(): boolean {
        return(this._turnPlayerIndex === this.thisPlayerIndex);
    }

    //-------------------------------------------------------------------------
    isTutorialOpen(): boolean {
        return(!!this._mainView.tutorialView);
    }

    //-------------------------------------------------------------------------
    get meldPrepCardImages(): CardImage[] {
        return(this._meldPrepLogic.meldPrepCardImages);
    }

    //-------------------------------------------------------------------------
    get melds(): Melds {
        return(this._mainView.melds);
    }

    //-------------------------------------------------------------------------
    get metricsJsonData(): any {
        return(this.game.cache.getJSON(JsonKey.METRICS));
    }

    //-------------------------------------------------------------------------
    get playersData(): PlayerData[] {
        return(this._playersData);
    }

    //-------------------------------------------------------------------------
    playSound(key: AudioEventKey, callback?: Function, context?: any): boolean {
        return(this._audioPlayer.play(key, callback, context))
    }

    //-------------------------------------------------------------------------
    /*
    presentAiLayoffMeld(cardImage: CardImage, meldImage: MeldImage) {
        this._cardsManager.meldPrepCardAdd(
            this._turnPlayerIndex,
            cardImage);

        this._layOffMeld(meldImage);
    }
    */

    //-------------------------------------------------------------------------
    presentAiMeldPrepCards(cardsData: CardData[], meldImage?: MeldImage, meldType?: MeldType) {
        if (meldType) {
            this._meldPrepLogic.meldType = meldType;
        }

        cardsData.forEach(function(cardData) {
            this._cardsManager.onCardsMoveComplete.addOnce(
                this._onCardsManagerAddAiMeldPrepsCardComplete, this, 0, meldImage);
                
            this._cardsManager.meldPrepCardAdd(
                this._turnPlayerIndex,
                this.getCardImage(cardData));
        }, this);
    }

    //-------------------------------------------------------------------------
    get riggedPlayersFavorArray(): boolean[] {
        const config = this.game.config;
        if (typeof config.isRiggedInPlayersFavor !== 'boolean') {
            return(null);
        }

        const riggedInPlayersFavorArrays: boolean[][] = config.riggedInPlayersFavorArrays;
        const playersFavorArray: boolean[] = 
            config.isRiggedInPlayersFavor === true ?
                riggedInPlayersFavorArrays[0] :
                riggedInPlayersFavorArrays[1]

        return(playersFavorArray);
    }

    //-------------------------------------------------------------------------
    setNextRiggedInPlayersFavorIndex() {
        const riggedPlayersFavorArray = this.riggedPlayersFavorArray;
        this._riggedInPlayersFavorIndex =
            (this._riggedInPlayersFavorIndex + 1) % riggedPlayersFavorArray.length;
    }

    //-------------------------------------------------------------------------
    shutdown() {
        this.onCardImageInputDownSgn.dispose();
        this.onDeckInputDownSgn.dispose();
        this.onDiscardCardImageInputDownSgn.dispose();

        this._cardsManager.dispose();
        this._mainView.dispose();
        this._ai.dispose();
        this._audioPlayer.dispose();
    }

    //-------------------------------------------------------------------------
    get thisPlayerIndex(): number {
        return(GameState.THIS_PLAYER_INDEX);
    }

    //-------------------------------------------------------------------------
    get turnPlayerIndex(): number {
        return(this._turnPlayerIndex);
    }

    //-------------------------------------------------------------------------
    get turnPlayerData(): PlayerData {
        return(this.getPlayerDataAt(this.turnPlayerIndex));
    }

    //-------------------------------------------------------------------------
    get tutorialStepsJsonData(): any {
        return(this.game.cache.getJSON(JsonKey.TUTORIAL_STEPS));
    }

    //-------------------------------------------------------------------------
    update() {
        if (this._turnTimer.isRunning) {
            this._turnTimer.update();
        }
    }

    //=========================================================================
    // private
    //=========================================================================
    
    //-------------------------------------------------------------------------
    _addToPlayerScore(playerIndex: number, points: number) {
        const playerData = this.getPlayerDataAt(playerIndex);
        if (playerData) {
            this._setPlayerScore(playerIndex, playerData.score + points);
        } else {
            console.warn(`Invalid player data index: ${playerIndex}.`);
        }
    }

    //-------------------------------------------------------------------------
    _addPlayerTurnTime() {
        const playerTurn = this.turnPlayerData;
        const elapsed = Date.now() - this._playerTurnStartTime;
        playerTurn.totalTurnTime += elapsed;
    }

    //-------------------------------------------------------------------------
    _beginRound() {
        if (this.isTutorialOpen()) {
            this._mainView.tutorialView.onButtonPressed(MainViewbuttonId.DEAL);
            return;
        }

        this._callListenerMap('gameStart', this.game);
        
        this._mainView.dealButtonVisible = false;
        this._isDealingOpeningHands = true;

        this._numDeckRecycles = 0;
        this._firstPlayerToCreateMeldThisRound = -1;

        if (this._isGameOver) {
            this._isGameOver = false;
            this._cardsManager.putAllCardsBackIntoDeck();
            this._ai.reset();
            if (!this._shuffleDeck(true, true)) {
                this._resumeDealOpeningCards();
            }
        } else {
            this._resumeDealOpeningCards();
        }
    }

    //-------------------------------------------------------------------------
    _calcPlayerHandScore(playerIndex: number): number {
        const playerData = this.getPlayerDataAt(playerIndex);
        if (!playerData) {
            console.warn(`Invalid player data index: ${playerIndex}.`);
            return(-1);
        }

        let score = 0;
        playerData.cardsData.forEach(function(cardData) {
            score += CardValuePoints.PointsOf(cardData.value);
        });

        return(score);
    }

    //-------------------------------------------------------------------------
    _callListenerMap(key: string, ...args) {
        const gameObject: any = this.game;
        const callback: Function = gameObject.listenerMapping[key];
        if (callback) {
            callback.apply(null, args);
        }
    }

    //-------------------------------------------------------------------------
    _createMeldFromPrep() {
        const cardsData: CardData[] = this._meldPrepLogic.getCardsDataFromPrep();
        
        const meldType: MeldType = this._isAisTurn ? this._ai.meldType : this._meldPrepLogic.meldType;
        const targetMeldImage: MeldImage = this._mainView.addMeld(cardsData, meldType);
        targetMeldImage.visible = false;

        this._turnTimer.paused = true;

        this._cardsManager.onCardsMoveComplete.addOnce(
            this._onCardsManagerCreateMeldMoveCardsComplete, this);
        this._cardsManager.moveMeldPrepCardsToMeld(targetMeldImage);
        
        this.playSound(AudioEventKey.NEW_MELD);
        
        this._addToPlayerScore(this.turnPlayerIndex, cardsData.length);
        
        if (this._firstPlayerToCreateMeldThisRound === -1) {
            this._firstPlayerToCreateMeldThisRound = this.turnPlayerIndex;
        }
    }

    //-------------------------------------------------------------------------
    _debugDeck() {
        this._cardsManager.applyDebugDeckCardIds(this.game.config.debugDeckCardIds);
    }

    //-------------------------------------------------------------------------
    _determineWinner(): number {
        console.log('determining winner by highest score...');
        let winningPlayerIndex = this._determineWinnerHighestScore();
        
        if (winningPlayerIndex === -1) {
            console.log('determining winner by which, if either, player the game is rigged for...');
            winningPlayerIndex = this._determineWinnerRigged();
        }
        
        if (winningPlayerIndex === -1) {
            console.log('determining winner by fewest cards in hand...');
            winningPlayerIndex = this._determineWinnerFewestCardsInHand();
        }

        if (winningPlayerIndex === -1) {
            console.log('determining winner by lowest hand value...');
            winningPlayerIndex = this._determineWinnerLowestHandValue();
        }

        if (winningPlayerIndex === -1) {
            console.log('determining winner by first to create a meld...');
            winningPlayerIndex = this._determineWinnerFirstToCreateMeld();
        }

        if (winningPlayerIndex === -1) {
            console.log('determining winner by shortest total turn time...');
            winningPlayerIndex = this._determineWinnerShortestTurnTime();
        }

        console.log(`determining winner player index is ${winningPlayerIndex}`);
        return(winningPlayerIndex);
    }

    //-------------------------------------------------------------------------
    _determineWinnerFewestCardsInHand(): number {
        const numCards1 = this.playersData[0].cardsData.length;
        const numCards2 = this.playersData[1].cardsData.length;
        
        if (numCards1 < numCards2) {
            return(0);
        } else if (numCards1 > numCards2) {
            return(1);
        }

        return(-1);
    }

    //-------------------------------------------------------------------------
    _determineWinnerFirstToCreateMeld(): number {
        return(this._firstPlayerToCreateMeldThisRound);
    }

    //-------------------------------------------------------------------------
    _determineWinnerHighestScore(): number {
        const score1 = this.playersData[0].score;
        const score2 = this.playersData[1].score;
        
        if (score1 > score2) {
            return(0);
        } else if (score1 < score2) {
            return(1);
        }

        return(-1);
    }

    //-------------------------------------------------------------------------
    _determineWinnerLowestHandValue(): number {
        const score1 = this._calcPlayerHandScore(0);
        const score2 = this._calcPlayerHandScore(1);
        
        if (score1 < score2) {
            return(0);
        } else if (score1 > score2) {
            return(1);
        }

        return(-1);
    }

    //-------------------------------------------------------------------------
    _determineWinnerRigged(): number {
        if (typeof this.isRiggedInPlayersFavor !== 'boolean') {
            return(-1);
        }
        
        return(!!this.isRiggedInPlayersFavor ? 0 : 1);
    }

    //-------------------------------------------------------------------------
    _determineWinnerShortestTurnTime(): number {
        const totalTurnTime1 = this.playersData[0].totalTurnTime;
        const totalTurnTime2 = this.playersData[1].totalTurnTime;
        
        if (totalTurnTime1 < totalTurnTime2) {
            return(0);
        } else if (totalTurnTime1 > totalTurnTime2) {
            return(1);
        }

        return(-1);
    }

    //-------------------------------------------------------------------------
    _discardRandomCardFromTurnPlayer() {
        const playerData = this.turnPlayerData;
        
        let cardSlotIndex: number = Math.floor(Math.random() * playerData.numCards);

        if (playerData.pickedDiscardedCard && playerData.numCards > 1) {
            const pickedDiscardedIndex =
                playerData.cardsData.indexOf(playerData.pickedDiscardedCard);
            
            while (cardSlotIndex === pickedDiscardedIndex) {
                cardSlotIndex = Math.floor(Math.random() * playerData.numCards);
            }
        }

        const cardData = playerData.getCardDataAt(cardSlotIndex);
        const cardImage = this.getCardImage(cardData);

        const AUTO_PLAY = true;
        this._turnTimer.paused = true;
        this._cardsManager.discardCardFromPlayer(playerData.index, cardImage, AUTO_PLAY);
        this._cardsManager.onCardsMoveComplete.addOnce(
            this._onCardsManagerTimeExpiredDiscardPlayerCardComplete, this);
    }

    //-------------------------------------------------------------------------
    _endRoundInSuddenDeath() {
        this._isGameOver = true;

        this._turnTimer.stop();

        this._canTurnPlayerSelectCardFromHand = false;
        this._canTurnPlayerDraw = false;
        this._canTurnPlayerPickFromDiscard = false;
        
        this._mainView.meldLayOfftButtonVisible = false;
        this._mainView.cancelMeldButtonVisible = false;
        this._mainView.sortButtonVisible = false;
        this._mainView.finishMeldButtonVisible = false;

        this._revealAllPlayerCards();
        
        const winningPlayerIndex = this._determineWinner();

        this._callListenerMap('gameOver', this.game);

        const LISTENER_KEY = 'gameOverPlayerWonWholeThing';
        const callGameOverListener = function() {
            this._callListenerMap(LISTENER_KEY, this.game, winningPlayerIndex);            
        }

        if (!this._audioPlayer.play(AudioEventKey.PLAYER_WINS, callGameOverListener, this)) {
            callGameOverListener.call(this);
        }
    }

    //-------------------------------------------------------------------------
    _enforceTimeExpiredDelay() {
        this._timeExpiredDelayTimer = this.game.time.create();
        this._timeExpiredDelayTimer.add(this.game.config.timeExpiredDelay,
            this._onTimeExpiredDelayTimerComplete, this);
        this._timeExpiredDelayTimer.start();
    }

    //-------------------------------------------------------------------------
    _enterMeldMode() {
        this._isMeldMode = true;

        this._canTurnPlayerSelectCardFromHand = true;
        this._canTurnPlayerDraw = false;
        this._canTurnPlayerPickFromDiscard = false;
        
        if (this.isThisPlayersTurn()) {
            this._mainView.meldLayOfftButtonVisible = false;
            this._mainView.cancelMeldButtonVisible = true;
            this._mainView.sortButtonVisible = false;
        }

        this._mainView.melds.onMeldImageInputDown.addOnce(this._onMeldImageInputDown, this);
    }

    //-------------------------------------------------------------------------
    _exitMeldMode() {
        this._isMeldMode = false;
        
        this._turnTimer.paused = true;

        this._cardsManager.onCardsMoveComplete.addOnce(
            this._onCardsManagerMeldPrepCancelMoveCardsComplete, this);
        this._meldPrepLogic.meldPrepCancel(this._turnPlayerIndex);
        
        this._mainView.melds.onMeldImageInputDown.removeAll();

        if (this.isThisPlayersTurn()) {
            this._mainView.meldLayOfftButtonVisible = true;
            this._mainView.cancelMeldButtonVisible = false;
            this._mainView.sortButtonVisible = true;
            this._mainView.finishMeldButtonVisible = false;
        }
    }

    //-------------------------------------------------------------------------
    _handleAiAddedDiscardedCardToHand() {
        if (!this.game.config.debugCanControlAi) {
            this._ai.play();
        }
    }

    //-------------------------------------------------------------------------
    _handlePlayerAddedDiscardedCardToHand() {
        const playerData = this.turnPlayerData;
        playerData.pickedDiscardedCard = playerData.cardsData[playerData.numCards - 1];
    }

    //-------------------------------------------------------------------------
    _init() {
        this.onCardImageInputDownSgn = new Phaser.Signal();
        this.onCardImageInputDownSgn.add(this._onCardImageInputDown, this);

        this.onDeckInputDownSgn = new Phaser.Signal();
        this.onDeckInputDownSgn.add(this._onDeckInputDown, this);
    
        this.onDiscardCardImageInputDownSgn = new Phaser.Signal();
        this.onDiscardCardImageInputDownSgn.add(this._onDiscardCardImageInputDown, this);

        this._initPlayersData();
        this._initMainView();
        this._initPlayersTexts();

        this._turnTimer = new TurnTimer(this.game);
        this._turnTimer.onTimerComplete.add(this._onTurnTimerComplete, this);

        this._cardsManager = new CardsManager(this);
        this._meldPrepLogic = new MeldPrepLogic(this);
        this._audioPlayer = new AudioPlayer(this);

        this._riggedInPlayersFavorIndex = 0;

        this._initAi();

        if (this.game.config.showTutorial) {
            this._openTutorial();
        } else {
            this._prepSession();
        }
    }

    //-------------------------------------------------------------------------
    _initAi() {
        this._ai = new Ai(this, GameState.AI_PLAYER_INDEX);
        this._ai.onEndTurn.add(this._onAiEndTurn, this);
    }

    //-------------------------------------------------------------------------
    _initMainView() {
        this._mainView = new MainView(this);
        this._mainView.dealButtonPressed.add(this._onMainViewDealPressed, this);
        this._mainView.cancelMeldButtonPressed.add(this._onMainViewCancelMeldPressed, this);
        this._mainView.meldLayOffButtonPressed.add(this._onMainViewMedLayOffPressed, this);
        this._mainView.finishMeldButtonPressed.add(this._onMainViewFinishMeldPressed, this);
        this._mainView.sortButtonPressed.add(this._onMainViewSortPressed, this);
        this._mainView.tutorialViewClosed.add(this._onMainViewTutorialViewClosed, this)
    }

    //-------------------------------------------------------------------------
    _initPlayersData() {
        const jsonData = this.metricsJsonData;
        const jPlayersData: any[] = jsonData.playersData;
        
        this._playersData = [];

        jPlayersData.forEach(function(jPlayerData, index) {
            this._playersData.push(new PlayerData(`Player ${index}`, index, jPlayerData));
        }, this);
    }

    //-------------------------------------------------------------------------
    _initPlayersTexts() {
        for (let playerIndex = 0; playerIndex < this.game.config.playerNames.length; playerIndex += 1) {
            this._mainView.playersNameText[playerIndex].text = this.game.config.playerNames[playerIndex];
            this._setPlayerScore(playerIndex, this.getPlayerDataAt(playerIndex).score);
        }
    }

    //-------------------------------------------------------------------------
    _isAisTurn(): boolean {
        return(this._turnPlayerIndex === this._ai.playerIndex);
    }

    //-------------------------------------------------------------------------
    _isTurnPlayerHandEmpty(): boolean {
        const playerData = this.playersData[this._turnPlayerIndex];
        return(playerData.numCards === 0);
    }

    //-------------------------------------------------------------------------
    _isTurnPlayersCard(cardImage: CardImage): boolean {
        const turnPlayerData = this.turnPlayerData;
        return(cardImage && turnPlayerData.cardsData.indexOf(cardImage.cardData) > -1);
    }

    //-------------------------------------------------------------------------
    _onAiEndTurn(playerIndex: number) {
        this._setNextTurnPlayer();
    }

    //-------------------------------------------------------------------------
    _onCardImageInputDown(cardImage: CardImage) {
        if (!this.isThisPlayersTurn() && !this.canSeeOtherPlayersCards) {
            return;
        }

        if (!this._canTurnPlayerSelectCardFromHand) {
            return;
        }

        if (!this._isTurnPlayersCard(cardImage)) {
            return;
        }

        if (this._isMeldMode) {
            this._turnTimer.paused = true;
            this._cardsManager.onCardsMoveComplete.addOnce(
                this._onCardsManagerMeldPrepMoveCardsComplete, this);
            this._meldPrepLogic.meldPrepCard(this._turnPlayerIndex, cardImage);
            return;
        }

        const turnPlayerData = this.turnPlayerData;
        if (turnPlayerData.pickedDiscardedCard &&
        cardImage.cardData === turnPlayerData.pickedDiscardedCard &&
        turnPlayerData.numCards > 1) {
            //cant discard the card that was picked from the discard card this turn, unless it's the
            // only card in the hand
            return;
        }

        this.discardCardFromTurnPlayer(cardImage);
    }

    //-------------------------------------------------------------------------
    _onCardsManagerAddAiMeldPrepsCardComplete(cardsManager: CardsManager) {
        this._ai.play();
    }

    //-------------------------------------------------------------------------
    _onCardsManagerAddDiscardedCardToHandComplete(cardsManager: CardsManager) {
        this._turnTimer.paused = false;
        if (this._isAisTurn()) {
            this._handleAiAddedDiscardedCardToHand();
        } else {
            this._handlePlayerAddedDiscardedCardToHand();
        }
    }

    //-------------------------------------------------------------------------
    _onCardsManagerCreateMeldMoveCardsComplete(cardsManager: CardsManager,
    cardsData?: CardData[], existingMeldImage?: MeldImage) {
        this._exitMeldMode();

        if (cardsData && existingMeldImage) {
            this._mainView.addMeld(cardsData, null, existingMeldImage);
        }

        if (this._isTurnPlayerHandEmpty()) {
            this._turnPlayerWinsGame();
            return;
        }

        if (this._isAisTurn()) {
            this._ai.play();
            return;
        }

        this._canTurnPlayerSelectCardFromHand = true;
        this._canTurnPlayerDraw = false;
        this._canTurnPlayerPickFromDiscard = false;
        
        this._turnTimer.paused = false;
    }

    //-------------------------------------------------------------------------
    _onCardsManagerDiscardOpeningCardComplete(cardsManager: CardsManager) {
        this._setTurnPlayer(0);
    }
    
    //-------------------------------------------------------------------------
    _onCardsManagerDiscardPlayerCardComplete(cardsManager: CardsManager) {
        if (this._isTurnPlayerHandEmpty()) {
            this._turnPlayerWinsGame();
        } else {
            this._setNextTurnPlayer();
        }
    }

    //-------------------------------------------------------------------------
    _onCardsManagerDrawCardCompleteComplete(cardsManager: CardsManager) {
        if (this._hasTurnPlayerTimeExpired) {
            this._discardRandomCardFromTurnPlayer();
        } else {
            this._resumeTurnPlayer();
        }
    }

    //-------------------------------------------------------------------------
    _onCardsManagerMeldPrepCancelMoveCardsComplete(cardsManager: CardsManager) {
        this._turnTimer.paused = false;
    }
    
    //-------------------------------------------------------------------------
    _onCardsManagerMeldPrepMoveCardsComplete(cardsManager: CardsManager) {
        this._turnTimer.paused = false;
        this._meldPrepLogic.handleMeldPrepMoveCardsComplete();
    }
        
    //-------------------------------------------------------------------------
    _onCardsManagerOpeningDealComplete(cardsManager: CardsManager) {
        this._isDealingOpeningHands = false;
        
        this._cardsManager.onCardsMoveComplete.addOnce(
            this._onCardsManagerDiscardOpeningCardComplete, this);
        this._cardsManager.discardCardFromDeck();
    }

    //-------------------------------------------------------------------------
    _onCardsManagerSortCardsMoveCompleteComplete(cardsManager: CardsManager) {
        this._resumeTurnPlayer();
    }

    //-------------------------------------------------------------------------
    _onCardsManagerTimeExpiredDiscardPlayerCardComplete(cardsManager: CardsManager) {
        this._setNextTurnPlayer();
    }

    //-------------------------------------------------------------------------
    _onDeckInputDown() {
        if (this.isTutorialOpen()) {
            return;
        }

        if (!this._canTurnPlayerDraw) {
            return;
        }

        this._didTurnPlayerDrawOrPickDiscard = true;

        this.drawCard();
        this._canTurnPlayerDraw = false;
        this._canTurnPlayerSelectCardFromHand = true;
        this._canTurnPlayerPickFromDiscard = false;
        
        this._mainView.meldLayOfftButtonVisible = true;
    }

    //-------------------------------------------------------------------------
    _onDiscardCardImageInputDown() {
        if (!this._canTurnPlayerPickFromDiscard) {
            return;
        }

        this._didTurnPlayerDrawOrPickDiscard = true;

        this._canTurnPlayerPickFromDiscard = false;
        this._canTurnPlayerDraw = false;
        this._canTurnPlayerSelectCardFromHand = true;
        
        this._mainView.meldLayOfftButtonVisible = true;

        this.addDiscardedCardToHand();
    }

    //-------------------------------------------------------------------------
    _onMainViewCancelMeldPressed() {
        if (this.isTutorialOpen()) {
            this._mainView.tutorialView.onButtonPressed(MainViewbuttonId.CANCEL_MELD);
        } else {
            this._exitMeldMode();
        }
    }

    //-------------------------------------------------------------------------
    _onMainViewDealPressed() {
        this._beginRound();
    }

    //-------------------------------------------------------------------------
    _onMainViewFinishMeldPressed() {
        if (this.isTutorialOpen()) {
            this._mainView.tutorialView.onButtonPressed(MainViewbuttonId.FINISH_MELD);
        } else {
            this.handleMeldFinished();
        }
    }

    //-------------------------------------------------------------------------
    _onMainViewMedLayOffPressed() {
        if (this.isTutorialOpen()) {
            this._mainView.tutorialView.onButtonPressed(MainViewbuttonId.MELD_LAY_OFF);
        } else {
            this._enterMeldMode();
        }
    }

    //-------------------------------------------------------------------------
    _onMainViewSortPressed() {
        if (this.isTutorialOpen()) {
            this._mainView.tutorialView.onButtonPressed(MainViewbuttonId.SORT);
        } else {
            this._sortTurnPlayerCards();
        }
    }

    //-------------------------------------------------------------------------
    _onMainViewTutorialViewClosed() {
        this._mainView.closeTutorialView();
        this._cardsManager.putAllCardsBackIntoDeck();
        this._resetPlayerScores();
        this._prepSession();
    }

    //-------------------------------------------------------------------------
    _openTutorial() {
        this._mainView.dealButtonVisible = false;
        this._mainView.sortButtonVisible = false;
        this._mainView.meldLayOfftButtonVisible = false;
        this._mainView.cancelMeldButtonVisible = false;
        this._mainView.finishMeldButtonVisible = false;
        this._mainView.openTutorial();
    }

    //-------------------------------------------------------------------------
    _layOffMeld(meldImage: MeldImage) {
        const cardsData: CardData[] = this._meldPrepLogic.getCardsDataFromPrep();

        this._turnTimer.paused = true;

        this._cardsManager.onCardsMoveComplete.addOnce(
            this._onCardsManagerCreateMeldMoveCardsComplete, this, 0,
            cardsData, meldImage);
        this._cardsManager.moveMeldPrepCardsToMeld(meldImage);

        this.playSound(AudioEventKey.LAY_OFF_MELD);

        this._addToPlayerScore(this.turnPlayerIndex, cardsData.length);
    }
    
    //-------------------------------------------------------------------------
    _onMeldImageInputDown(melds: Melds, meldImage: MeldImage) {
        this.handleMeldFinished(meldImage);
    }
    
    //-------------------------------------------------------------------------
    _onTimeAutoDealTimerComplete() {
        this._beginRound();
    }

    //-------------------------------------------------------------------------
    _onTimeExpiredDelayTimerComplete() {
        if (this._didTurnPlayerDrawOrPickDiscard) {
            this._discardRandomCardFromTurnPlayer();
        } else {
            this.drawCard();
        }
    }

    //-------------------------------------------------------------------------
    _onTurnTimerComplete() {
        this._playerTimeExpired();    
    }

    //-------------------------------------------------------------------------
    _playerTimeExpired() {
        this._turnTimer.stop();
        
        this._hasTurnPlayerTimeExpired = true;

        if (this._isMeldMode) {
            this._exitMeldMode();
        }

        this._canTurnPlayerSelectCardFromHand = false;
        this._canTurnPlayerDraw = false;
        this._canTurnPlayerPickFromDiscard = false;
        
        this._mainView.sortButtonVisible = false;
        this._mainView.meldLayOfftButtonVisible = false;
        this._mainView.cancelMeldButtonVisible = false;
        this._mainView.finishMeldButtonVisible = false;

        if (!this._audioPlayer.play(AudioEventKey.PLAYER_TIME_OVER,
        function() {
            this._enforceTimeExpiredDelay();
        }, this)) {
            this._enforceTimeExpiredDelay();
        }
    }

    //-------------------------------------------------------------------------
    _prepSession() {
        this._canTurnPlayerSelectCardFromHand = false;
        this._canTurnPlayerDraw = false;
        this._canTurnPlayerPickFromDiscard = false;
        
        this._mainView.sortButtonVisible = false;
        this._mainView.meldLayOfftButtonVisible = false;
        this._mainView.cancelMeldButtonVisible = false;
        this._mainView.finishMeldButtonVisible = false;

        if (!this.game.config.debugDontShuffleDeck) {
            this._audioPlayer.play(AudioEventKey.SHUFFLE_DECK, null);
            this._shuffleDeck();
        }

        this._debugDeck();

        if (this.game.config.autoDealDelay > 0) {
            this._mainView.dealButtonVisible = false;
            this._autoDealTimer = this.game.time.create();
            this._autoDealTimer.add(this.game.config.autoDealDelay,
                this._onTimeAutoDealTimerComplete, this);
            this._autoDealTimer.start();
        } else {
            this._mainView.dealButtonVisible = true;
        }
    }

    //-------------------------------------------------------------------------
    _resetPlayerScores() {
        this._playersData.forEach(function(playerData) {
            this._setPlayerScore(playerData.index, 0);
        }, this);
    }

    //-------------------------------------------------------------------------
    _resumeDealOpeningCards() {
        this._cardsManager.onCardsMoveComplete.addOnce(
            this._onCardsManagerOpeningDealComplete, this);
        this._cardsManager.dealOpeningCards();
    }

    //-------------------------------------------------------------------------
    _resumeTurnPlayer() {
        const isThisPlayersTurn =
            this.isThisPlayersTurn() ||
            this.canControlAi;

        this._canTurnPlayerDraw =
            isThisPlayersTurn && !this._didTurnPlayerDrawOrPickDiscard;
        
        this._canTurnPlayerPickFromDiscard =
            isThisPlayersTurn && !this._didTurnPlayerDrawOrPickDiscard;
        
        this._mainView.sortButtonVisible = isThisPlayersTurn;

        if (!isThisPlayersTurn) {
            if (!this.game.config.debugCanControlAi) {
                this._ai.play();
            }
        }

        this._setTurnTimer();
    }

    //-------------------------------------------------------------------------
    _revealAllPlayerCards(tutorialLosingPlayerIndex?: number) {
        const index = tutorialLosingPlayerIndex !== undefined ?
            tutorialLosingPlayerIndex :
            (this._turnPlayerIndex === 0 ? 1 : 0);

        const losingPlayerData = this.getPlayerDataAt(index);
        losingPlayerData.cardsData.forEach(function(cardData) {
            this.getCardImage(cardData).isFaceDown = false;
        }, this);

    }

    //-------------------------------------------------------------------------
    _setNextTurnPlayer() {
        this._addPlayerTurnTime();

        this.turnPlayerData.pickedDiscardedCard = null;
        const nextPlayerIndex = (this._turnPlayerIndex + 1) % this._playersData.length;
        this._setTurnPlayer(nextPlayerIndex);
    }

    //-------------------------------------------------------------------------
    _setPlayerScore(playerIndex: number, score: number) {
        const playerData = this.getPlayerDataAt(playerIndex);
        if (!playerData) {
            console.warn(`Invalid player data index: ${playerIndex}.`);
            return;
        }

        playerData.score = score;

        const playerScoreText = this._mainView.playersScoreText[playerIndex];
        if (playerScoreText) {
            playerScoreText.text = playerData.score.toString();
        }
    }

    //-------------------------------------------------------------------------
    _setTurnPlayer(playerIndex: number) {
        this._turnPlayerIndex = playerIndex;

        this._hasTurnPlayerTimeExpired = false;
        this._didTurnPlayerDrawOrPickDiscard = false;

        this._mainView.meldLayOfftButtonVisible = false;
        this._mainView.sortButtonVisible = this.isThisPlayersTurn() || this.canControlAi;
        
        this._canTurnPlayerSelectCardFromHand = false;
        this._canTurnPlayerDraw = false;
        this._canTurnPlayerPickFromDiscard = false;

        this._turnTimer.stop();

        if (this._cardsManager.isDeckEmpty()) {
           this._endRoundInSuddenDeath();
        } else {
            this._resumeTurnPlayer();
        }

        this._playerTurnStartTime = Date.now();
    }

    //-------------------------------------------------------------------------
    _setTurnTimer() {
        if (this._turnTimer.paused) {
            this._turnTimer.paused = false;
            return;
        }

        this._timerBar = this._mainView.getTimerBar(this._turnPlayerIndex);
        this._turnTimer.start(this.game.config.timePerTurn, this._timerBar);
    }

    //-------------------------------------------------------------------------
    _shuffleDeck(playSound: boolean = false, isStartingNewGame: boolean = false): boolean {
        this._cardsManager.shuffleDeck();

        if (!playSound) {
            return(false);
        }

        if (!this._audioPlayer.play(AudioEventKey.SHUFFLE_DECK, function() {
            if (isStartingNewGame) {
                this._resumeDealOpeningCards();
            } else {
                this._resumeTurnPlayer();
            }
        }, this)) {
            return(false);
        }

        return(true);
    }

    //-------------------------------------------------------------------------
    _sortTurnPlayerCards() {
        this._canTurnPlayerDraw = false;
        this._canTurnPlayerPickFromDiscard = false;
        this._mainView.sortButtonVisible = false;

        this._turnTimer.paused = true;

        this._cardsManager.onCardsMoveComplete.addOnce(
            this._onCardsManagerSortCardsMoveCompleteComplete, this);
        this._cardsManager.sortCards(this._turnPlayerIndex);
    }

    //-------------------------------------------------------------------------
    _stopTurnTimer() {
        this._turnTimer.start(this.game.config.timePerTurn, this._timerBar);
    }

    //-------------------------------------------------------------------------
    _turnPlayerWinsGame() {
        this._isGameOver = true;

        this._turnTimer.stop();

        this._canTurnPlayerSelectCardFromHand = false;
        this._canTurnPlayerDraw = false;
        this._canTurnPlayerPickFromDiscard = false;
            
        this._mainView.meldLayOfftButtonVisible = false;
        this._mainView.cancelMeldButtonVisible = false;
        this._mainView.sortButtonVisible = false;
        this._mainView.finishMeldButtonVisible = false;

        this._revealAllPlayerCards();

        this._callListenerMap('gameOver', this.game);

        this._revealAllPlayerCards();
        
        const winningPlayerIndex = this.turnPlayerIndex;

        this._callListenerMap('gameOver', this.game);

        const LISTENER_KEY = 'gameOverPlayerWonWholeThing';
        const callGameOverListener = function() {
            this._callListenerMap(LISTENER_KEY, this.game, winningPlayerIndex);            
        }

        if (!this._audioPlayer.play(AudioEventKey.PLAYER_WINS, callGameOverListener, this)) {
            callGameOverListener.call(this);
        }
    }

    //-------------------------------------------------------------------------
    _updatePlayerScores(tutorialLosingPlayerIndex?: number) {
        const losingPlayerIndex = tutorialLosingPlayerIndex !== undefined ?
            tutorialLosingPlayerIndex :
            (this._turnPlayerIndex === 0 ? 1 : 0);
        let handPoints = this._calcPlayerHandScore(losingPlayerIndex);
        if (handPoints > -1) {
            this._addToPlayerScore(losingPlayerIndex, handPoints);
        }

        const winningPlayerIndex = losingPlayerIndex === 0 ? 1 : 0;
        handPoints = this._calcPlayerHandScore(winningPlayerIndex);
        if (handPoints > -1) {
            this._addToPlayerScore(winningPlayerIndex, handPoints);
        }
    }
}
