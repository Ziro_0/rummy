export default class BootState {
    game: Phaser.Game;

    constructor(game: Phaser.Game) {
        this.game = game;
    }

    init() {
        // Responsive scaling
        this.game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;

        // Center the game
        this.game.scale.pageAlignHorizontally = true;
        this.game.scale.pageAlignVertically = true;
    }

    preload() {
        const load = this.game.load;
        load.image('preload_bar', 'assets/game-rummy/images/preload_bar.png');
    }

    create() {
        this.game.state.start('PreloadState');
    }
}
