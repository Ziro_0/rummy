import CardData from "./CardData";
import { CardSuit } from "./CardSuit";
import IGameState from "../interfaces/IGameState";

export default class MeldIconPair extends Phaser.Group {
    cardData: CardData;

    _gameState: IGameState;
    _scale: number;

    //=========================================================================
    // constructor
    //=========================================================================
    /** @constructor */
    constructor(gameState: IGameState, x: number, y: number,
    parent: PIXI.DisplayObjectContainer, cardData: CardData) {
        super(gameState.game, parent || gameState.game.world);
        
        this.x = x;
        this.y = y;
        this.cardData = cardData;
        this._gameState = gameState;

        this._initIconScale();
        this._createIcons();
    }

    //=========================================================================
    // private
    //=========================================================================

    //-------------------------------------------------------------------------
    _createIcons() {
        const valueIcon = this._createValueIcon();
        this._createSuitIcon(valueIcon);
    }

    //-------------------------------------------------------------------------
    _createSuitIconFrame(): string {
        return(`meld-icon-${this.cardData.suit}`);
    }

    //-------------------------------------------------------------------------
    _createValueIconFrame(): string {
        const valuePart = this.cardData.value;
        const color = this._getSuitColor();
        return(`meld-icon-${valuePart}-${color}`);
    }

    //-------------------------------------------------------------------------
    _createSuitIcon(valueIcon: Phaser.Image): Phaser.Image {
        const frame = this._createSuitIconFrame();
        const icon = this.game.add.image(
            valueIcon.x + valueIcon.width, 0, 'ui', frame, this);
        icon.scale.set(this._scale);
        return(icon);
    }

    //-------------------------------------------------------------------------
    _createValueIcon(): Phaser.Image {
        const frame = this._createValueIconFrame();
        const icon = this.game.add.image(0, 0, 'ui', frame, this);
        icon.scale.set(this._scale);
        return(icon);
    }

    //-------------------------------------------------------------------------
    _getSuitColor(): string {
        const suit = this.cardData.suit;
        return(
            suit === CardSuit.HEART || suit === CardSuit.DIAMOND ?
            "red" : "black");
    }

    //-------------------------------------------------------------------------
    _initIconScale() {
        const jsonData = this._gameState.metricsJsonData;
        this._scale = jsonData.meldIcons.scale;
    }
}
