import { CardValue } from "./CardValue";
import { CardSuit } from "./CardSuit";

export default class CardData {
    suit: CardSuit;
    value: CardValue;

    //=========================================================================
    // constructor
    //=========================================================================
    /** @constructor */
    constructor(value: CardValue, suit: CardSuit) {
        this.suit = suit;
        this.value = value;
    }

    //=========================================================================
    // public
    //=========================================================================
    
    //-------------------------------------------------------------------------
    clone(): CardData {
        return(new CardData(this.value, this.suit));
    }

    //-------------------------------------------------------------------------
    equals(cardData: CardData): boolean {
        return(
            this === cardData ||
            (this.suit === cardData.suit &&
            this.value === cardData.value));
    }

    //-------------------------------------------------------------------------
    static GetImageFrameName(cardData: CardData): string {
        const PREFIX = 'card_';
        return(`${PREFIX}${cardData.value}${cardData.suit}`);
    }

    //-------------------------------------------------------------------------
    static IndexOf(cardsData: CardData[], targetCardData: CardData): number {
        if (!cardsData || !targetCardData) {
            return(-1);
        }

        for (let index = 0; index < cardsData.length; index++) {
            const sourceCardData = cardsData[index];
            if (targetCardData.equals(sourceCardData)) {
                return(index);
            }
        }

        return(-1);
    }
}
