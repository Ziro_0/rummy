import { CardValue } from "./CardValue";

export default class CardValueRanks {
    static readonly _RANKS_TABLE = [
        CardValue.ACE,
        CardValue.TWO,
        CardValue.THREE,
        CardValue.FOUR,
        CardValue.FIVE,
        CardValue.SIX,
        CardValue.SEVEN,
        CardValue.EIGHT,
        CardValue.NINE,
        CardValue.TEN,
        CardValue.JACK,
        CardValue.QUEEN,
        CardValue.KING
    ];

    //-------------------------------------------------------------------------
    static RankOf(value: CardValue): number {
        return(CardValueRanks._RANKS_TABLE.indexOf(value));
    }

    //-------------------------------------------------------------------------
    static ValueOf(rank: number): CardValue {
        return(CardValueRanks._RANKS_TABLE[rank]);
    }
}
