import CardData from "./CardData";

export default class PlayerData {
    pickedDiscardedCard: CardData;
    
    name: string;
    score: number;
    totalTurnTime: number;

    yCardsPosition: number;

    _cardsData: CardData[];
    _index: number;

    //=========================================================================
    // constructor
    //=========================================================================
    /** @constructor */
    constructor(name: string = '', index: number, jPlayerData?: any) {
        this._cardsData = [];
        this.name = name;
        this.score = 0;
        this.totalTurnTime = 0;
        
        this._index = index;
        this._dataFromJsonObject(jPlayerData);
    }

    //=========================================================================
    // public
    //=========================================================================

    //-------------------------------------------------------------------------
    addCardData(cardData: CardData): number {
        return(this.addCardDataAt(cardData, this._cardsData.length));
    }

    //-------------------------------------------------------------------------
    addCardDataAt(cardData: CardData, index: number): number {
        this._cardsData.splice(index, 0, cardData);
        return(index);
    }

    //-------------------------------------------------------------------------
    get cardsData(): CardData[] {
        return(this._cardsData);
    }
    
    //-------------------------------------------------------------------------
    getCardDataAt(index: number): CardData {
        return(this._cardsData[index]);
    }

    //-------------------------------------------------------------------------
    getCardDataIndexOf(cardData: CardData): number {
        for (let index = 0; index < this._cardsData.length; index++) {
            if (this._cardsData[index] === cardData) {
                return(index);
            }
        }
        return(-1);
    }

    //-------------------------------------------------------------------------
    setCardDataPositions(cardDataIndexes: number[]) {
        const tempCardsData = this._cardsData.concat();

        cardDataIndexes.forEach(function(cardDataIndex, arrIndex) {
            if (cardDataIndex > -1 && cardDataIndex <= this._cardsData.length) {
                this._cardsData[arrIndex] = tempCardsData[cardDataIndex];
            }
        }, this);
    }

    //-------------------------------------------------------------------------
    get numCards(): number {
        return(this._cardsData.length);
    }

    //-------------------------------------------------------------------------
    get index(): number {
        return(this._index);
    }

    //-------------------------------------------------------------------------
    removeAllCardsData() {
        this._cardsData.length = 0;
    }

    //-------------------------------------------------------------------------
    removeCardData(cardData: CardData): boolean {
        const index = this._cardsData.indexOf(cardData);
        return(this.removeCardDataAt(index) !== null);
    }

    //-------------------------------------------------------------------------
    removeCardDataAt(index: number): CardData {
        const cardData: CardData = this._cardsData[index];
        if (cardData) {
            this._cardsData.splice(index, 1);
        }
        return(cardData);
    }

    //=========================================================================
    // private
    //=========================================================================

    //-------------------------------------------------------------------------
    _dataFromJsonObject(jPlayerData: any) {
        this.yCardsPosition = jPlayerData.y;
    }
}
