import CardData from "./CardData";
import MeldIconPair from "./MeldIconPair";
import CardImage from "./CardImage";
import { MeldType } from "./MeldType";
import MatchesDataStraightFlush from "./cardMatchUtils/MatchesDataStraightFlush";
import CardMatchUtils from "./cardMatchUtils/CardMatchUtils";
import IGameState from "../interfaces/IGameState";

export default class MeldImage extends Phaser.Group {

    onDown: Phaser.Signal;

    _gameState: IGameState;
    _contact: Phaser.Graphics
    _meldType: MeldType;
    
    //=========================================================================
    // constructor
    //=========================================================================
    /** @constructor */
    constructor(gameState: IGameState, x: number, y: number,
    cardData: CardData[], meldType: MeldType, parent?: PIXI.DisplayObjectContainer) {
        super(gameState.game, parent);
        
        this._gameState = gameState;
        this._meldType = meldType;
        this.onDown = new Phaser.Signal();

        this.x = x;
        this.y = y;

        this._createContact();
        this.enabled = false;

        this.addIconPairs(cardData);
    }

    //=========================================================================
    // public
    //=========================================================================

    //-------------------------------------------------------------------------
    addIconPairs(cardsData: CardData[]) {
        if (!cardsData) {
            return;
        }

        this._removeContactFromParent();

        let x = this.width;
        cardsData.forEach(function(cardData) {
            const icon = new MeldIconPair(this._gameState, x, 0, this, cardData);
            x += icon.width;
        }, this);
        
        this._sortIconPairs();

        this._resizeContact();
    }

    //-------------------------------------------------------------------------
    get cardsData(): CardData[] {
        const out: CardData[] = [];
        
        this.forEach(function(pair: MeldIconPair) {
            if (pair instanceof MeldIconPair) {
                out.push(pair.cardData);
            }
        });
        
        return(out);
    }

    //-------------------------------------------------------------------------
    deemphasize() {
        this._contact.input.enabled = false;
        this._contact.visible = true;
        this._emphasizeContact(false);
    }

    //-------------------------------------------------------------------------
    destroy() {
        this.onDown.dispose();
        this._gameState = null;
        super.destroy();
    }

    //-------------------------------------------------------------------------
    set enabled(value: boolean) {
        this._contact.input.enabled = value;
        
        if (value) {
            this._emphasizeContact(true);
        }

        this._contact.visible = value;
    }

    //-------------------------------------------------------------------------
    isLayoffPossible(cards: CardImage[] | CardData[]): boolean {
        if (!cards) {
            return(false);
        }

        const cardsData: CardData[] = this._isCardImageArray(cards) ?
            CardImage.GetCardsDataFrom(cards as CardImage[]) :
            cards as CardData[];
        
        let result =
            cardsData.length > 0 &&
            (this._isSameValueLayoffPossible(cardsData) ||
            this._isStraighFlushValueLayoffPossible(cardsData));
        
        return(result);
    }

    //-------------------------------------------------------------------------
    get meldType(): MeldType {
        return(this._meldType);
    }
    
    //=========================================================================
    // private
    //=========================================================================

    //-------------------------------------------------------------------------
    _createContact() {
        this._contact = this._gameState.game.make.graphics();
        this._drawContact();

        this._contact.inputEnabled = true;
        this._contact.input.useHandCursor = true;
        this._contact.events.onInputDown.add(this._onInputDown, this);
    }

    //-------------------------------------------------------------------------
    _drawContact(color?: number, alpha?: number) {
        this._contact.clear();
        this._contact.beginFill(color, alpha);
        this._contact.drawRect(0, 0, 50, 50);
        this._contact.endFill();
    }

    //-------------------------------------------------------------------------
    _emphasizeContact(value: boolean) {
        const FILL_COLOR = value ? 0x00ff00 : 0x404040;
        const FILL_ALPHA = value ? 0.5 : 0.75;
        this._drawContact(FILL_COLOR, FILL_ALPHA);
        this._resizeContact();
    }

    //-------------------------------------------------------------------------
    _getIconByCardData(cardData): MeldIconPair {
        for (let index = 0; index < this.length; index++) {
            const pair: MeldIconPair = this.getChildAt(index) as MeldIconPair;
            if (pair instanceof MeldIconPair) {
                if (pair.cardData === cardData) {
                    return(pair);
                }
            }
        }

        return(null);
    }

    //-------------------------------------------------------------------------
    _isCardImageArray(cards: CardImage[] | CardData[]): boolean {
        for (let index = 0; index < cards.length; index += 1) {
            if (cards[index] instanceof CardImage) {
                return (true);
            }
        }

        return (false);
    }

    //-------------------------------------------------------------------------
    _isSameValueLayoffPossible(cardsData: CardData[]): boolean {
        let mergedCardsData: CardData[] = this.cardsData.concat(cardsData);
        mergedCardsData = CardMatchUtils.SortCardsByDescendingValue(mergedCardsData);
        return(CardMatchUtils.DoCardsHaveSameValue(mergedCardsData, mergedCardsData.length));
    }

    //-------------------------------------------------------------------------
    _isStraighFlushValueLayoffPossible(cardsData: CardData[]): boolean {
        const mergedCardsData: CardData[] = this.cardsData.concat(cardsData);
        return(MatchesDataStraightFlush.ExecMatch(mergedCardsData, mergedCardsData.length));
    }

    //-------------------------------------------------------------------------
    _onInputDown() {
        this.onDown.dispatch(this);
    }

    //-------------------------------------------------------------------------
    _removeContactFromParent() {
        if (this._contact.parent) {
            this._contact.parent.removeChild(this._contact);
        }
    }

    //-------------------------------------------------------------------------
    _resizeContact() {
        this._removeContactFromParent();

        this._contact.width = this.width;
        this._contact.height = this.height;

        this.addChild(this._contact);
    }

    //-------------------------------------------------------------------------
    _sortIconPairs() {
        const cardsData = CardMatchUtils.SortCardsByAscendingValue(this.cardsData);

        this._removeContactFromParent();

        let x = 0;
        cardsData.forEach(function(cardData) {
            const icon: MeldIconPair = this._getIconByCardData(cardData);
            if (icon) {
                this.bringToTop(icon);
                icon.x = x;
                x += icon.width;
            }
        }, this);

        this._resizeContact();
    }
}
