import CardData from "./CardData";
import { CardSuit } from "./CardSuit";
import { CardValue } from "./CardValue";

export default class CardImage extends Phaser.Image {
    handPositionPreMeld: Phaser.Point;
    onInputDown: Phaser.Signal;
    
    _cardData: CardData;
    _isFaceDown: boolean;

    //=========================================================================
    // constructor
    //=========================================================================
    /** @constructor */
    constructor(game: Phaser.Game, x: number, y: number, cardData: CardData,
    group: Phaser.Group, onInputDown?: Phaser.Signal) {
        const frame = CardData.GetImageFrameName(cardData);
        super(game, x, y, 'cards', frame);
        group.add(this);

        this.anchor.set(0.5);

        this.onInputDown = onInputDown;

        this._cardData = cardData;

        this.inputEnabled = true;
        this.input.useHandCursor = true;
        this.events.onInputDown.add(this._onInputDown, this);
    }

    //=========================================================================
    // public
    //=========================================================================
    
    //-------------------------------------------------------------------------
    get cardData(): CardData {
        return(this._cardData);
    }

    //-------------------------------------------------------------------------
    set cardData(value: CardData) {
        if (!value) {
            return;
        }

        this._cardData = value;

        if (!this._isFaceDown) {
            this.frameName = CardData.GetImageFrameName(this._cardData);
        }
    }

    //-------------------------------------------------------------------------
    destroy() {
        this.onInputDown = null;
        this._cardData = null;
        super.destroy();
    }

    //-------------------------------------------------------------------------
    get isEnabled(): boolean {
        return(this.input.enabled);
    }

    //-------------------------------------------------------------------------
    set isEnabled(value: boolean) {
        this.input.enabled = value;
    }

    //-------------------------------------------------------------------------
    set isFaceDown(value: boolean) {
        if (value === this._isFaceDown) {
            return;
        }

        this._isFaceDown = value;
        
        this.frameName = this._isFaceDown ?
            'card_back_red' :
            CardData.GetImageFrameName(this._cardData);
    }

    //-------------------------------------------------------------------------
    static GetCardsDataFrom(images: CardImage[]): CardData[] {
        const data: CardData[] = [];
        
        images.forEach(function(image) {
            data.push(image.cardData);
        });

        return(data);
    }

    //-------------------------------------------------------------------------
    get suit(): CardSuit {
        return(this._cardData.suit);
    }

    //-------------------------------------------------------------------------
    get value(): CardValue {
        return(this._cardData.value);
    }

    //=========================================================================
    // private
    //=========================================================================

    //-------------------------------------------------------------------------
    _onInputDown() {
        if (this.onInputDown) {
            this.onInputDown.dispatch(this);
        }
    }
}
