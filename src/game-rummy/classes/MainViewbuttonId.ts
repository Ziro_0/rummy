export enum MainViewbuttonId {
    CANCEL_MELD = 'cancelMeld',
    DEAL = 'deal',
    FINISH_MELD = 'finishMeld',
    MELD_LAY_OFF = 'meldLayOff',
    SORT = 'sort'
};
