import { Game } from "../game";

export default class TimerBar extends Phaser.Group {
    _frontImage: Phaser.Image;

    //=========================================================================
    // constructor
    //=========================================================================
    /** @constructor */
    constructor(game: Game, x: number, y: number, parent: Phaser.Group) {
        super(game, parent);
        
        this.x = x;
        this.y = y;

        this._initImage('time-bar-borders');
        this._initImage('time-bar-back');
        this._frontImage = this._initImage('time-bar-front');
    }

    //=========================================================================
    // public
    //=========================================================================

    //-------------------------------------------------------------------------
    set value(ratio: number) {
        ratio = Math.max(0, Math.min(1.0, ratio));
        this._frontImage.scale.x = ratio;
    }

    //=========================================================================
    // private
    //=========================================================================

    //-------------------------------------------------------------------------
    _initImage(imageFrame: string): Phaser.Image {
        const image = this.game.add.image(0, 0, 'ui', imageFrame, this);
        image.anchor.set(0.5);
        return(image);
    }
}
