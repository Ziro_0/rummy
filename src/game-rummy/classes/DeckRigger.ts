import IGameState from "../interfaces/IGameState";
import PlayerData from "./PlayerData";
import CardData from "./CardData";
import Deck from "./Deck";
import CardValueRanks from "./CardValueRanks";
import { CardValue } from "./CardValue";
import CardMatchUtils from "./cardMatchUtils/CardMatchUtils";
import MeldImage from "./MeldImage";
import { MeldType } from "./MeldType";
import GameState from '../states/GameState';

export default class DeckRigger {
    _gameState: IGameState;
    _aiCardToDrawNext: CardData;

    //=========================================================================
    // constructor
    //=========================================================================
    /** @constructor */
    constructor(gameState: IGameState) {
        this._gameState = gameState;
    }

    //=========================================================================
    // public
    //=========================================================================
    
    set aiCardToDrawNext(cardData: CardData) {
        this._aiCardToDrawNext = cardData;
    }

    //-------------------------------------------------------------------------
    rigDrawCard(playerIndex: number) {
        const isRiggedInPlayersFavorConfig = this._gameState.game.config.isRiggedInPlayersFavor;
        if (typeof isRiggedInPlayersFavorConfig !== 'boolean') {
            return;
        }

        if (this._gameState.isDealingOpeningHands) {
            return;
        }

        const PLAYER_INDEX_PLAYER = 0;
        const PLAYER_INDEX_AI = 1;
        let numPlayerCards = this._gameState.getPlayerDataAt(PLAYER_INDEX_PLAYER).numCards;
        let numAiCards = this._gameState.getPlayerDataAt(PLAYER_INDEX_AI).numCards;

        if (isRiggedInPlayersFavorConfig === true) {
            if (numAiCards <= numPlayerCards) {
                if (playerIndex === PLAYER_INDEX_PLAYER) {
                    this._pro(playerIndex);
                } else {
                    this._anti(playerIndex);
                }
            } else {
                if (this._gameState.isRiggedInPlayersFavor === true) {
                    if (playerIndex === PLAYER_INDEX_PLAYER) {
                        this._pro(playerIndex);
                    } else {
                        this._anti(playerIndex);
                    }
                } else {
                    if (playerIndex === PLAYER_INDEX_AI) {
                        this._pro(playerIndex);
                    } else {
                        this._anti(playerIndex);
                    }
                }
            }
        } else {
            if (numPlayerCards <= numAiCards) {
                if (playerIndex === PLAYER_INDEX_AI) {
                    this._pro(playerIndex);
                } else {
                    this._anti(playerIndex);
                }
            } else {
                if (this._gameState.isRiggedInPlayersFavor === false) {
                    if (playerIndex === PLAYER_INDEX_AI) {
                        this._pro(playerIndex);
                    } else {
                        this._anti(playerIndex);
                    }
                } else {
                    if (playerIndex === PLAYER_INDEX_PLAYER) {
                        this._pro(playerIndex);
                    } else {
                        this._anti(playerIndex);
                    }
                }
            }
        }

        /*
        if (this._gameState.isRiggedInPlayersFavor === true) {
            if (playerIndex === PLAYER_INDEX_PLAYER) {
                this._pro(playerIndex);
            } else {
                this._anti(playerIndex);
            }
        } else if (this._gameState.isRiggedInPlayersFavor === false) {
            if (playerIndex === PLAYER_INDEX_AI) {
                this._pro(playerIndex);
            } else {
                this._anti(playerIndex);
            }
        }
        */
    }
    
    //=========================================================================
    // private
    //=========================================================================
    
    //-------------------------------------------------------------------------
    _anti(playerIndex: number) {
        this._antiTopDeckCard(playerIndex);
    }

    //-------------------------------------------------------------------------
    _antiTopDeckCard(playerIndex: number) {
        const playerData = this._gameState.getPlayerDataAt(playerIndex);
        if (playerData.numCards === 0) {
            return;
        }
        
        console.log('anti deck top card')

        //1. get cards in deck whose values do not match any cards already in player's hand
        const cardsDataValuesNotInHand = this._getUniqueCardValuesFromDeck(playerData);

        const playerCardsData = playerData.cardsData;
        let nSelectedCardData: CardData = null;
        for (let nIndex = 0; nIndex < cardsDataValuesNotInHand.length; nIndex++) {
            const nCardData = cardsDataValuesNotInHand[nIndex];

            //2. make sure the card doesnt allow the player to lay off any existing melds
            if (this._canThisCardLayOffExistingMeld(nCardData)) {
                continue;
            }

            //3. if the card value is +/-1 from one already in hand must be a diff suit
            const nRank = CardValueRanks.RankOf(nCardData.value);

            for (let pIndex = 0; pIndex < playerCardsData.length; pIndex++) {
                const pCardData = playerCardsData[pIndex];
                const pRank = CardValueRanks.RankOf(pCardData.value);
                const rankDiff = Math.abs(pRank - nRank);
                if (rankDiff > 2) {
                    nSelectedCardData = nCardData;
                } else if (rankDiff <= 2 && nCardData.suit !== pCardData.suit) {
                    nSelectedCardData = nCardData;
                } else {
                    nSelectedCardData = null;
                    break;
                }
            }

            if (nSelectedCardData) {
                break;
            }
        }

        if (nSelectedCardData) {
            this._deck.swap(0, this._deck.cardDataIndexOf(nSelectedCardData));
        }
    }

    //-------------------------------------------------------------------------
    _canThisCardLayOffExistingMeld(cardData: CardData): boolean {
        return(this._gameState.melds.isLayOffPossible([cardData]));
    }

    //-------------------------------------------------------------------------
    get _deck(): Deck {
        return(this._gameState.cardsManager.deck);
    }

    //-------------------------------------------------------------------------
    _getCardDataNextValueButDiffSuit(cardsData: CardData[]) {

    }

    //-------------------------------------------------------------------------
    _getPlayerDataAt(index: number): PlayerData {
        return(this._gameState.getPlayerDataAt(index));
    }

    //-------------------------------------------------------------------------
    _getSameValueMeldCardIndexesFromDeck(meldImage: MeldImage): number[] {
        const cardDataIndexes = this._deck.search(meldImage.cardsData[0].value);
        if (cardDataIndexes.length > 0) {
            return(cardDataIndexes);
        }

        return(null);
    }

    //-------------------------------------------------------------------------
    _getSfValueMeldCardIndexesFromDeck(meldImage: MeldImage): number[] {
        const meldCardsData = CardMatchUtils.SortCardsByAscendingValue(meldImage.cardsData);
        const lowestCardData: CardData = meldCardsData[0];
        const lowestRank = CardValueRanks.RankOf(lowestCardData.value);
        let out: number[] = [];

        if (lowestRank > CardValueRanks.RankOf(CardValue.ACE)) {
            const cardValue = CardValueRanks.ValueOf(lowestRank - 1);
            const tmpCardIndexes = this._deck.search(cardValue, lowestCardData.suit);
            if (tmpCardIndexes.length > 0) {
                out = out.concat(tmpCardIndexes);
            }
        }

        const highestCardData: CardData = meldCardsData[meldCardsData.length - 1];
        const highestRank = CardValueRanks.RankOf(highestCardData.value);
        if (highestRank < CardValueRanks.RankOf(CardValue.KING)) {
            const cardValue = CardValueRanks.ValueOf(highestRank + 1);
            const tmpCardIndexes = this._deck.search(cardValue, highestCardData.suit);
            if (tmpCardIndexes.length > 0) {
                out = out.concat(tmpCardIndexes);
            }
        }

        return(out);
    }

    //-------------------------------------------------------------------------
    _getUniqueCardValuesFromDeck(playerData: PlayerData): CardData[] {
        const testDeck: Deck = new Deck(this._deck.cardsData.concat());

        for (let index = 0; index < playerData.numCards; index++) {
            const playerCardData = playerData.getCardDataAt(index);
            const indexesOf = testDeck.search(playerCardData.value);
            if (indexesOf.length > 0) {
                for (let indexOfIndex = indexesOf.length - 1; indexOfIndex >= 0; indexOfIndex--) {
                    const indexToDeleteAt = indexesOf[indexOfIndex];
                    testDeck.cardsData.splice(indexToDeleteAt, 1);
                }
            }
        }

        return(testDeck.cardsData);
    }

    //-------------------------------------------------------------------------
    _isThereAtLeastOneMeld(): boolean {
        return(this._gameState.melds.numMelds > 0);
    }

    //-------------------------------------------------------------------------
    _pro(playerIndex: number) {
        if (this._aiCardToDrawNext) {
            if (playerIndex === GameState.AI_PLAYER_INDEX) {
                const cardIndex = this._deck.cardDataIndexOf(this._aiCardToDrawNext);
                if (cardIndex > -1) {
                    console.log('forced card to draw next:');
                    console.log(this._deck.getAt(cardIndex));
                    this._deck.swap(0, cardIndex);
                }

                this._aiCardToDrawNext = null;
                return;
            }
        }

        if (this._isThereAtLeastOneMeld()) {
            if (this._proTopDeckMeldCard(playerIndex)) {
                return;
            }
        }

        this._proTopDeckCard(playerIndex);
    }

    //-------------------------------------------------------------------------
    _proTopDeckCard(playerIndex: number): boolean {
        let nSelectedCardDataIndex: number = -1;

        const playerData = this._gameState.getPlayerDataAt(playerIndex);
        if (playerData.numCards === 0) {
            return(false);
        }

        console.log('pro top deck card');

        //1. get lowest value card in hand. if count of cards with this value in hand >= 1, and
        // at least one card of this value is in deck, draw card with that value (suit n/a).
        const sortedCardsAsc = CardMatchUtils.SortCardsByAscendingValue(playerData.cardsData);

        for (let index = 0; index < sortedCardsAsc.length; index++) {
            const baseCardData = sortedCardsAsc[index];
            if (CardMatchUtils.CountCardsByValue(sortedCardsAsc, baseCardData.value) >= 1) {
                const cardIndexes = this._deck.search(baseCardData.value);
                if (cardIndexes.length > 0) {
                    nSelectedCardDataIndex = cardIndexes[0];
                }
            }

            if (nSelectedCardDataIndex > -1) {
                console.log('pro top deck card - found same-value card');
                break;
            }

            if (nSelectedCardDataIndex === -1) {
                //2. start with lowest card value in hand (base card).
                // check deck for card value +/-1 or +/-2 from base card card. suit must match same as
                // base card. if no matches, move to next lowest card, and make that the base card.
                // continue until a valid card is found from the deck, or all cards in hand have
                // been tried
                const nRank = CardValueRanks.RankOf(baseCardData.value);
                const cardValueUp1 = CardValueRanks.ValueOf(nRank + 1);
                const cardValueDown1 = CardValueRanks.ValueOf(nRank - 1);
                const cardValueUp2 = CardValueRanks.ValueOf(nRank + 2);
                const cardValueDown2 = CardValueRanks.ValueOf(nRank - 2);
                const validValues: CardValue[] = [];
                
                if (cardValueUp1) {
                    validValues.push(cardValueUp1);
                }

                if (cardValueDown1) {
                    validValues.push(cardValueDown1);
                }

                if (cardValueUp2) {
                    validValues.push(cardValueUp2);
                }

                if (cardValueDown2) {
                    validValues.push(cardValueDown2);
                }

                Phaser.ArrayUtils.shuffle(validValues);

                for (let vvIndex = 0; vvIndex < validValues.length; vvIndex++) {
                    const cardIndexes = this._deck.search(validValues[vvIndex], baseCardData.suit);
                    if (cardIndexes.length > 0) {
                        nSelectedCardDataIndex = cardIndexes[0];
                    }

                    if (nSelectedCardDataIndex > -1) {
                        console.log('found sf card');
                        break;
                    }
                }
            }
        }

        console.log(`pro top deck card - swap index with ${nSelectedCardDataIndex}`);
        if (nSelectedCardDataIndex === -1) {
            return(false);
        }

        this._deck.swap(0, nSelectedCardDataIndex);
        return(true);
    }

    //-------------------------------------------------------------------------
    _proTopDeckMeldCard(playerIndex: number): boolean {
        console.log('pro top deck meld card')

        let availableCardDataIndexes: number[] = [];

        this._gameState.melds.forEach(function(meldImage: MeldImage, meldIndex: number) {
            if (meldImage.meldType === MeldType.SAME_VALUE) {
                const cardDataIndexes = this._getSameValueMeldCardIndexesFromDeck(meldImage);
                console.log('checking same-value meld')
                if (cardDataIndexes) {
                    availableCardDataIndexes = availableCardDataIndexes.concat(cardDataIndexes);
                }
            } else if (meldImage.meldType === MeldType.STRAIGHT_FLUSH) {
                const cardDataIndexes = this._getSfValueMeldCardIndexesFromDeck(meldImage);
                console.log('checking sf meld')
                if (cardDataIndexes) {
                    availableCardDataIndexes = availableCardDataIndexes.concat(cardDataIndexes);
                }
            }
        }, this);

        const cardIndex = Phaser.ArrayUtils.getRandomItem(availableCardDataIndexes);
        if (cardIndex === null) {
            console.log('no pro top deck meld card available')
            return(false);
        }

        this._deck.swap(0, cardIndex);
        return(true);
    }
}
