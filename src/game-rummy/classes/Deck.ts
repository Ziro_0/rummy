import CardData from "./CardData";
import { CardValue, CardValueMap } from "./CardValue";
import { CardSuit, CardSuitMap } from "./CardSuit";

export default class Deck {
    _cardsData: CardData[];
    _maxSize: number;

    //=========================================================================
    // constructor
    //=========================================================================
    /** @constructor */
    constructor(cardsData?: CardData[]) {
        this._init(cardsData);
    }

    //=========================================================================
    // public
    //=========================================================================
    
    //-------------------------------------------------------------------------
    add(cardData: CardData) {
        if (cardData) {
            this._cardsData.unshift(cardData);
        }
    }

    //-------------------------------------------------------------------------
    cardDataIndexOf(cardData: CardData): number {
        return(this._cardsData.indexOf(cardData));
    }

    //-------------------------------------------------------------------------
    get cardsData(): CardData[] {
        return(this._cardsData);
    }

    //-------------------------------------------------------------------------
    getAt(index: number): CardData {
        return(this._cardsData[index]);
    }

    //-------------------------------------------------------------------------
    getCardDataByCardDataId(cardId: string): CardData {
        const index = this.getIndexOfCardDataById(cardId);
        return(this.getAt(index));
    }

    //-------------------------------------------------------------------------
    getIndexOfCardDataById(cardId: string): number {
        const value: CardValue = CardValueMap[cardId.substring(0, cardId.length - 1)];
        const suit: CardSuit = CardSuitMap[cardId.charAt(cardId.length - 1)];

        for (let index = 0; index < this._cardsData.length; index++) {
            const cardData = this._cardsData[index];
            if (cardData.value === value && cardData.suit === suit) {
                return(index);
            }
        }
        return(-1);
    }

    //-------------------------------------------------------------------------
    get length(): number {
        return(this._cardsData.length);
    }

    //-------------------------------------------------------------------------
    get maxSize(): number {
        return(this._maxSize);
    }

    //-------------------------------------------------------------------------
    remove(): CardData {
        return(this._cardsData.shift());
    }

    //-------------------------------------------------------------------------
    reset() {
        this._init(null);
    }

    //-------------------------------------------------------------------------
    search(value?: CardValue, suit?: CardSuit, startIndex?: number): number[] {
        if (startIndex === undefined) {
            startIndex = 0;
        } else {
            startIndex = Math.max(startIndex, 0);
        }

        const cardDataIndexes: number[] = [];

        for (let index = startIndex; index < this._cardsData.length; index++) {
            const cardData = this._cardsData[index];
            const doesValueMatch: boolean = !value || value === cardData.value;
            const doesSuitMatch: boolean = !suit || suit === cardData.suit;
            if (doesValueMatch && doesSuitMatch) {
                cardDataIndexes.push(index);
            }
        };
        
        return(cardDataIndexes);
    }

    //-------------------------------------------------------------------------
    shuffle() {
        Phaser.ArrayUtils.shuffle(this._cardsData);
    }

    //-------------------------------------------------------------------------
    swap(index1: number, index2: number) {
        if (index1 === index2 ||
        index1 < 0 || index1 >= this.length ||
        index2 < 0 || index2 >= this.length) {
            return;
        }

        const temp = this._cardsData[index1];
        this._cardsData[index1] = this._cardsData[index2];
        this._cardsData[index2] = temp;
    }

    //=========================================================================
    // private
    //=========================================================================
    
    //-------------------------------------------------------------------------
    _init(cardData: CardData[]) {
        if (cardData) {
            this._cardsData = cardData;
        } else {
            this._initCardData();
        }

        this._maxSize = this._cardsData.length;
    }

    //-------------------------------------------------------------------------
    _initCardData() {
        const values: CardValue[] = Object.keys(CardValue).map(key => CardValue[key]);
        const suits: CardSuit[] = Object.keys(CardSuit).map(key => CardSuit[key]);
        
        this._cardsData = [];

        values.forEach(function(value) {
            suits.forEach(function(suit) {
                this._cardsData.push(new CardData(value, suit));
            }, this);
        }, this);
    }
}
