import CardImage from "./CardImage";
import IGameState from "../interfaces/IGameState";
import Deck from "./Deck";

export default class DeckImage extends Phaser.Group {
    onInputDown: Phaser.Signal;
    _gameState: IGameState;

    //=========================================================================
    // constructor
    //=========================================================================
    /** @constructor */
    constructor(gameState: IGameState, x: number, y: number, onInputDown: Phaser.Signal,
    parent?: PIXI.DisplayObjectContainer) {
        super(gameState.game, parent);
        this._gameState = gameState;
        this.x = x;
        this.y = y;
        this.onInputDown = onInputDown;
        this._initDeckImage();
    }

    //=========================================================================
    // public
    //=========================================================================
    
    //-------------------------------------------------------------------------
    getTopCard(): CardImage {
        return(this.getTop() as CardImage);
    }

    //-------------------------------------------------------------------------
    getCardPosition(): Phaser.Point {
        const cardImage = this.getTopCard();
        return(cardImage ?
            new Phaser.Point(this.x + cardImage.x, this.y + cardImage.y) : 
            new Phaser.Point());
    }

    //-------------------------------------------------------------------------
    set stack(value: number) {
        let numCardsVisible = Math.floor(value);
        if (numCardsVisible > this.length) {
            numCardsVisible = this.length;
        } else if (numCardsVisible < 0) {
            numCardsVisible = 0;
        }

        this.children.forEach(function(cardDisplay, index) {
            cardDisplay.visible = numCardsVisible > index;
        });
    }

    //-------------------------------------------------------------------------
    updateStack(deck: Deck) {
        const MAX_CARDS = deck.maxSize;
        const NUM_LOGICAL_STACKS_VISIBLE = Math.min(this.length, deck.length);
        const SLOPE = NUM_LOGICAL_STACKS_VISIBLE / MAX_CARDS;
        this.stack = Math.ceil(SLOPE * deck.length);
    }

    //=========================================================================
    // private
    //=========================================================================

    //-------------------------------------------------------------------------
    _initDeckImage() {
        const jsonData = this._gameState.metricsJsonData;
        const jDeck = jsonData.deck;
        const stackSize: number = jDeck.stackSize;
        const stackOffset: Phaser.Point = new Phaser.Point(
            jDeck.stackOffset.x,
            jDeck.stackOffset.y);

        for (let stack = 0; stack < stackSize; stack++) {
            const x = stackOffset.x * stack;
            const y = stackOffset.y * stack;
            const image = this.game.add.image(x, y, 'cards', 'card_back_red', this);
            image.anchor.set(0.5);
            image.inputEnabled = true;
            image.input.useHandCursor = true;
            image.events.onInputDown.add(this._onInputDown, this);
        }
    }

    //-------------------------------------------------------------------------
    _onInputDown() {
        if (this.onInputDown) {
            this.onInputDown.dispatch(this);
        }
    }
}
