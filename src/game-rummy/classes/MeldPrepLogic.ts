import CardImage from "./CardImage";
import IGameState from "../interfaces/IGameState";
import PlayerData from "./PlayerData";
import MatchesDataStraightFlush from "./cardMatchUtils/MatchesDataStraightFlush";
import MatchesDataThreeKind from "./cardMatchUtils/MatchesDataThreeKind";
import CardData from "./CardData";
import { MeldType } from "./MeldType";
import MeldImage from "./MeldImage";
import Melds from "./Melds";

export default class MeldPrepLogic {
    meldType: MeldType;

    _gameState: IGameState;
    _meldPrepCardImages: CardImage[];
    
    //=========================================================================
    // constructor
    //=========================================================================
    /** @constructor */
    constructor(gameState: IGameState) {
        this._gameState = gameState;
        this._meldPrepCardImages = [];
    }

    //=========================================================================
    // public
    //=========================================================================

    //-------------------------------------------------------------------------
    getCardsDataFromPrep(): CardData[] {
        const out: CardData[] = [];
        
        this._meldPrepCardImages.forEach(function(cardImage) {
            out.push(cardImage.cardData);
        });
        
        return(out);
    }

    //-------------------------------------------------------------------------
    handleMeldPrepMoveCardsComplete() {
        this._updateFinishMeldButton();
        this._updateAvailableMelds();
    }

    //-------------------------------------------------------------------------
    meldPrepCancel(playerIndex: number) {
        this._meldPrepCardRemoveAll(playerIndex);
        this._disableAllMelds();
    }

    //-------------------------------------------------------------------------
    meldPrepCard(playerIndex: number, cardImage: CardImage) {
        const playerData: PlayerData = this._gameState.playersData[playerIndex];
        if (!playerData) {
            console.warn('Invalid player index.');
            return;
        }

        const meldPrepCardIndex = this._meldPrepCardImages.indexOf(cardImage);
        if (meldPrepCardIndex > -1) {
            this._meldPrepCardRemove(playerIndex, cardImage, meldPrepCardIndex);
        } else {
            this._meldPrepCardAdd(playerIndex, cardImage);
        }
    }

    //-------------------------------------------------------------------------
    get meldPrepCardImages(): CardImage[] {
        return(this._meldPrepCardImages);
    }

    //=========================================================================
    // private
    //=========================================================================

    //-------------------------------------------------------------------------
    _disableAllMelds() {
        this._gameState.melds.disable();
    }

    //-------------------------------------------------------------------------
    _getPrepCardsData(): CardData[] {
        const cardsData: CardData[] = [];
        this._meldPrepCardImages.forEach(function(cardImage) {
            cardsData.push(cardImage.cardData);
        });
       return(cardsData);
    }

    //-------------------------------------------------------------------------
    _getValidLayoffMelds(): MeldImage[] {
        return(this._gameState.melds.getValidLayoffMelds(this._meldPrepCardImages));
    }

    //-------------------------------------------------------------------------
    _isMeldPrepValid(): boolean {
        const cardsData = this._getPrepCardsData();
        
        if (MatchesDataStraightFlush.ExecMatch(cardsData)) {
            this.meldType = MeldType.STRAIGHT_FLUSH;
            return(true);
        } else if (MatchesDataThreeKind.ExecMatch(cardsData)) {
            this.meldType = MeldType.SAME_VALUE;
            return(true);
        }
        
        return(false);
    }

    //-------------------------------------------------------------------------
    _meldPrepCardAdd(playerIndex: number, cardImage: CardImage) {
        this._gameState.cardsManager.meldPrepCardAdd(playerIndex, cardImage);
    }

    //-------------------------------------------------------------------------
    _meldPrepCardRemove(playerIndex: number, cardImage: CardImage,
    meldPrepCardIndex: number) {
       this._gameState.cardsManager.meldPrepCardRemove(playerIndex, meldPrepCardIndex);
    }

    //-------------------------------------------------------------------------
    _meldPrepCardRemoveAll(playerIndex: number) {
        this._gameState.cardsManager.meldPrepCardRemoveAll(playerIndex);
    }

    //-------------------------------------------------------------------------
    _onMeldImageInputDown(melds: Melds, meldImage: MeldImage) {
        const cardsData = this._getPrepCardsData();
        this._gameState.melds.add(cardsData, null, meldImage);
    }

    //-------------------------------------------------------------------------
    _repositionMeldPrepCards(playerIndex: number) {
        const nc = this._meldPrepCardImages.length;
        if (nc === 0) {
            return;
        }
        
        const jsonData = this._gameState.metricsJsonData;
        const jMeldPrep = jsonData.meldPrep;
        const ofs = jMeldPrep.cardsOffset;
        const xCenter = this._gameState.game.world.centerX;
        const halfNc = Math.floor(nc / 2);
        let xStart: number;

        if ((nc & 1) === 1) {
            //starting position for meld preps with an odd number of cards
            xStart = xCenter - halfNc * ofs;
        } else {
            //starting position for meld preps with an even number of cards
            xStart = xCenter - Math.floor(ofs / 2) - ofs * (halfNc - 1);
        }

        const y: number = jMeldPrep.yPositionForPlayers[playerIndex];
        let x = xStart;
        for (let index = 0; index < nc; index++) {
            const cardImage: CardImage = this._meldPrepCardImages[index];
            cardImage.x = x;
            cardImage.y = y;
            x += ofs;
        }
    }

    //-------------------------------------------------------------------------
    _updateAvailableMelds() {
        if (this._meldPrepCardImages.length === 0) {
            this._disableAllMelds();
            return;
        }

        const layoffMeldImages: MeldImage[] = this._getValidLayoffMelds();

        this._gameState.melds.forEach(function(meldImage: MeldImage) {
            if (layoffMeldImages.indexOf(meldImage) > -1) {
                meldImage.enabled = true;
            } else {
                meldImage.deemphasize();
            }
        });
    }

    //-------------------------------------------------------------------------
    _updateFinishMeldButton() {
        this._gameState.finishMeldButtonVisible = 
            (this._gameState.isThisPlayersTurn() || this._gameState.canControlAi) &&
                this._isMeldPrepValid();
    }
}
