import { CardValue } from "./CardValue";

export default class CardValuePoints {
    static readonly _POINTS_TABLE = [
        { value: CardValue.ACE, points: 1 },
        { value: CardValue.TWO, points: 2 },
        { value: CardValue.THREE, points: 3 },
        { value: CardValue.FOUR, points: 4 },
        { value: CardValue.FIVE, points: 5 },
        { value: CardValue.SIX, points: 6 },
        { value: CardValue.SEVEN, points: 7 },
        { value: CardValue.EIGHT, points: 8 },
        { value: CardValue.NINE, points: 9 },
        { value: CardValue.TEN, points: 10 },
        { value: CardValue.JACK, points: 10 },
        { value: CardValue.QUEEN, points: 10 },
        { value: CardValue.KING, points: 10 }
    ];

    //-------------------------------------------------------------------------
    static PointsOf(value: CardValue): number {
        for (let index = 0; index < CardValuePoints._POINTS_TABLE.length; index++) {
            const cell = CardValuePoints._POINTS_TABLE[index];
            if (cell.value === value) {
                return(cell.points);
            }
        }

        return(0);
    }
};
