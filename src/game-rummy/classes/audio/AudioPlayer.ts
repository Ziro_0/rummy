import IGameState from "../../interfaces/IGameState";
import { AudioEventKey } from "./AudioEventKey";

export default class AudioPlayer {
    _gameState: IGameState;

    //=========================================================================
    // constructor
    //=========================================================================
    /** @constructor */
    constructor(gameState: IGameState) {
        this._gameState = gameState;
    }

    //=========================================================================
    // public
    //=========================================================================
    
    //-------------------------------------------------------------------------
    dispose() {
        this._gameState.game.sound.removeAll();
    }
    
    //-------------------------------------------------------------------------
    play(soundEventKey: AudioEventKey, stopCallback?: Function,
    stopCallbackContext?: any): boolean {
        const jsonData = this._gameState.audioJsonData;
        const jsonSoundEvents = jsonData.soundEvents;
        const jsonSoundKeysRaw: any = jsonSoundEvents[soundEventKey];
        if (!jsonSoundKeysRaw) {
            console.warn(`AudioPlayer.play. Sound event key ${soundEventKey} not found in json data`);
            return(false);
        }

        let jsonSoundKeys: string[];
        if (Array.isArray(jsonSoundKeysRaw)) {
            jsonSoundKeys = jsonSoundKeysRaw;
        } else {
            jsonSoundKeys = [ jsonSoundKeysRaw ];
        }

        const soundKey = Phaser.ArrayUtils.getRandomItem(jsonSoundKeys);
        const sound = this._gameState.game.sound.play(soundKey);
        if (!sound) {
            console.warn(`AudioPlayer.play. Could not play sound with key ${soundKey}`);
            return(false);
        }

        if (stopCallback) {
            sound.onStop.addOnce(stopCallback, stopCallbackContext);
        }
        return(true);
    }
}
