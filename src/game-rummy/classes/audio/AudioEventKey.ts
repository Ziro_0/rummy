export enum AudioEventKey {
    BUTTON_PRESS = 'buttonPress',
    DEAL_CARD = 'dealCard',
    DISCARD = 'discard',
    END_GAME_DRAW = 'endGameDraw',
    LAY_OFF_MELD = 'layOffMeld',
    NEW_MELD = 'newMeld',
    PLAYER_LOSES = 'playerLoses',
    PLAYER_TIME_OVER = 'playerTimeOver',
    PLAYER_WINS = 'playerWins',
    SHUFFLE_DECK = 'shuffleDeck'
}
