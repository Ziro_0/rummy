import IGameState from "../../interfaces/IGameState";
import PlayerData from "../PlayerData";
import CardImage from "../CardImage";
import CardMatchUtils from "../cardMatchUtils/CardMatchUtils";
import { CardValue } from "../CardValue";
import CardValueRanks from "../CardValueRanks";
import CardData from "../CardData";
import MeldImage from "../MeldImage";
import { MeldType } from "../MeldType";
import { irandomRange } from "../../util/util";
import AiTargetMelds from './AiTargetMelds';
import GameState from '../../states/GameState';
import Deck from '../Deck';

export default class Ai {
    //=========================================================================
    // "static / const"
    //=========================================================================
    static readonly _PRESENT_MELD_DELAY: number = 500;

    onEndTurn: Phaser.Signal;

    meldType: MeldType;

    _targetMeldCards: CardData[];

    _gameState: IGameState;
    
    _targetLayoffMeldImage: MeldImage;

    _playerData: PlayerData;

    _nextMoveTimer: Phaser.Timer;

    _resumeTurnCallback: Function;

    _aiSpeedMin: number;
    _aiSpeedMax: number;
    _playerIndex: number;

    _layOffMeldWithPickedDiscardedCard: boolean;

    //=========================================================================
    // constructor
    //=========================================================================
    /** @constructor */
    constructor(gameState: IGameState, playerIndex: number) {
        this._gameState = gameState;
        this._playerData = this._gameState.getPlayerDataAt(playerIndex);
        this._playerIndex = playerIndex;

        this.onEndTurn = new Phaser.Signal();

        this._aiSpeedMin = this._gameState.game.config.aiSpeedMin;
        this._aiSpeedMax = this._gameState.game.config.aiSpeedMax;
        
        this._targetMeldCards = [];

        this._initNextMoveTimer();
    }

    //=========================================================================
    // public
    //=========================================================================

    //-------------------------------------------------------------------------
    dispose() {
        this.onEndTurn.dispose();
        this._nextMoveTimer.destroy();
    }

    //-------------------------------------------------------------------------
    play() {
        if (this._resumeTurnCallback) {
            const callback = this._resumeTurnCallback;
            this._resumeTurnCallback = null;
            callback.call(this);
        } else {
            this._playerData.pickedDiscardedCard = null;
            this._layOffMeldWithPickedDiscardedCard = false;
            this._startNextMoveDelayTimer(this._checkDiscard);
        }
    }

    //-------------------------------------------------------------------------
    get playerIndex(): number {
        return(this._playerIndex);
    }

    //-------------------------------------------------------------------------
    reset() {
        this._targetMeldCards = [];
        this._targetLayoffMeldImage = null;
        this._resumeTurnCallback = null;
        this._layOffMeldWithPickedDiscardedCard = false;
    }

    //-------------------------------------------------------------------------
    stop() {
        this._nextMoveTimer.stop();
    }

    //=========================================================================
    // private
    //=========================================================================

    //-------------------------------------------------------------------------
    _areCardRanksWithinRange(cardValue1: CardValue, cardValue2: CardValue,
    range: number): boolean {
        const rank1 = CardValueRanks.RankOf(cardValue1);
        const rank2 = CardValueRanks.RankOf(cardValue2);
        return(Math.abs(rank1 - rank2) <= range);
    }

    //-------------------------------------------------------------------------
    _calcAiSpeed(): number {
        return(irandomRange(this._aiSpeedMin, this._aiSpeedMax));
    }

    //-------------------------------------------------------------------------
    _checkDiscard() {
        if (this._shouldPickDiscardedCard()) {
            this._pickDiscardedCard();
        } else {
            this._drawCard();
        }
    }

    //-------------------------------------------------------------------------
    _checkHandForLayoffMelds(): boolean {
        if (this._layOffMeldWithPickedDiscardedCard) {
            this._layOffMeldWithPickedDiscardedCard = false;

            if (this._playerData.pickedDiscardedCard) {
                const cardImages = [ this._gameState.getCardImage(this._playerData.pickedDiscardedCard) ];
                const cardsData = [ this._playerData.pickedDiscardedCard ];

                const meldImages = this._gameState.melds.getValidLayoffMelds(cardImages);
                if (meldImages.length > 0) {
                    this._presentLayoffMeld(cardsData, meldImages[0]);
                    return(true);
                }
            }
        }
        
        for (let cardIndex = 0; cardIndex < this._playerData.numCards; cardIndex++) {
            const playerCardData = this._playerData.getCardDataAt(cardIndex);
            const cardsData = [ playerCardData ];
            const cardImages = [ this._gameState.getCardImage(playerCardData) ];
            const meldImages = this._gameState.melds.getValidLayoffMelds(cardImages);
            if (meldImages.length > 0) {
                this._presentLayoffMeld(cardsData, meldImages[0]);
                return(true);
            }
        }

        return(false);
    }

    //-------------------------------------------------------------------------
    _checkHandForNewMelds(): boolean {
        const sortedCardsDesc = CardMatchUtils.SortCardsByDescendingValue(this._playerData.cardsData);

        //check for cards that can form a same-value meld
        let matchingCardsData = this._checkHandForNewMeldSameValue(sortedCardsDesc);
        if (matchingCardsData) {
            this._presentNewMeld(matchingCardsData, MeldType.SAME_VALUE);
            return(true);
        }

        //check for cards that can form a sf meld
        matchingCardsData = this._checkHandForNewMeldSf(sortedCardsDesc);
        if (matchingCardsData) {
            this._presentNewMeld(matchingCardsData, MeldType.STRAIGHT_FLUSH);
            return(true);
        }

        return(false);
    }

    //-------------------------------------------------------------------------
    _checkHandForNewMeldSameValue(sortedCardsDesc: CardData[]): CardData[] {
        const matchingCardsData: CardData[] = [];
        if (CardMatchUtils.DoCardsHaveSameValue(sortedCardsDesc, 4, matchingCardsData)) {
            return(matchingCardsData);
        } else if (CardMatchUtils.DoCardsHaveSameValue(sortedCardsDesc, 3, matchingCardsData)) {
            return(matchingCardsData);
        }

        return(null);
    }

    //-------------------------------------------------------------------------
    _checkHandForNewMeldSf(sortedCardsDesc: CardData[]): CardData[] {
        const matchingCardsData: CardData[] = [];
        if (CardMatchUtils.AreCardsStraightFlush(sortedCardsDesc, undefined,
        matchingCardsData)) {
            return(matchingCardsData);
        }

        return(null);
    }

    //-------------------------------------------------------------------------
    get _deck(): Deck {
        return(this._gameState.cardsManager.deck);
    }
    
    //-------------------------------------------------------------------------
    _discardCard() {
        if (typeof this._gameState.isRiggedInPlayersFavor === 'boolean') {
            if (this._gameState.isRiggedInPlayersFavor) {
                this._discardCardAnti();
            } else {
                this._discardCardPro();
            }
        } else {
            this._discardCardPro();
        }
    }

    //-------------------------------------------------------------------------
    _discardCardAnti() {
        console.log('ai discard anti');
        const aiTargetMelds = new AiTargetMelds(
            this._gameState,
            this._playerData,
            this._gameState.getPlayerDataAt(GameState.THIS_PLAYER_INDEX),
            this._gameState.cardsManager);  
        
        const cardsToKeepUsing1 = aiTargetMelds.getCardsToKeepUsing1Card();
        // console.log('cards to keep using 1 card');
        // console.log(cardsToKeepUsing1);
        
        let cardToDiscard: CardData;

        if (cardsToKeepUsing1.length > 0) {
            cardToDiscard = Phaser.ArrayUtils.getRandomItem(cardsToKeepUsing1);
        } else {
            const cardsToKeepUsing2 = aiTargetMelds.getCardsToKeepUsing2Cards();
            // console.log('cards to keep using 2 cards');
            // console.log(cardsToKeepUsing2);
            cardToDiscard = Phaser.ArrayUtils.getRandomItem(cardsToKeepUsing2);
        }

        let cardImage: CardImage = this._gameState.getCardImage(cardToDiscard);
        if (!cardImage) {
            // console.log('no cards to discard; discarding first card instead')
            cardImage = this._gameState.getCardImage(this._playerData.cardsData[0]);
        }

        this._gameState.discardCardFromTurnPlayer(cardImage);
    }

    //-------------------------------------------------------------------------
    _discardCardPro() {
        console.log('ai discard pro');
        const aiTargetMelds = new AiTargetMelds(
            this._gameState,
            this._playerData,
            this._gameState.getPlayerDataAt(GameState.THIS_PLAYER_INDEX),
            this._gameState.cardsManager);  
        
        const cardsToKeepUsing2 = aiTargetMelds.getCardsToKeepUsing2Cards();
        // console.log('cards to keep using 2 cards');
        // console.log(cardsToKeepUsing2);

        const cardsToKeepUsing1 = aiTargetMelds.getCardsToKeepUsing1Card();
        // console.log('cards to keep using 1 card');
        // console.log(cardsToKeepUsing1);

        const cardsToKeep = this._mergeUniqueCards(cardsToKeepUsing1, cardsToKeepUsing2);
        // console.log('cards to keep');
        // console.log(cardsToKeep.concat());

        let discardCards = this._removeCardsToKeepFrom(this._playerData.cardsData, cardsToKeep);
        // console.log('initial pool of cards to discard');
        // console.log(discardCards.concat());
        
        let cardImage: CardImage = this._gameState.getCardImage(discardCards[0]);
        if (!cardImage) {
            discardCards = this._removeCardsToKeepFrom(this._playerData.cardsData, cardsToKeepUsing1);
            // console.log('no cards to discard; check cards to keep using 1 card');
            // console.log(discardCards.concat());

            cardImage = this._gameState.getCardImage(discardCards[0]);
            if (!cardImage) {
                // console.log('no cards to discard; check cards that allow player to create a meld');
                const playerMeldCards: CardData[] = [];
                aiTargetMelds.check1HandCardWithPlayersHand(playerMeldCards);
                discardCards = this._removeCardsToKeepFrom(this._playerData.cardsData,
                    playerMeldCards);
                // console.log(discardCards);
                cardImage = this._gameState.getCardImage(discardCards[0]);
                
                if (!cardImage) {
                    // console.log("no cards to discard; discarding first card in ai's hand");
                    cardImage = this._gameState.getCardImage(this._playerData.cardsData[0]);
                }
            }
        }

        this._gameState.discardCardFromTurnPlayer(cardImage);
    }

    //-------------------------------------------------------------------------
    _mergeUniqueCards(cards1: CardData[], cards2: CardData[]): CardData[] {
        const out: CardData[] = cards1.concat();
        
        cards2.forEach((card) => {
            if (out.indexOf(card) === -1) {
                out.push(card);
            }
        });

        return(out);
    }

    //-------------------------------------------------------------------------
    _drawCard() {
        this._resumeTurnCallback = this._onDrewCard;
        this._gameState.drawCard();
    }

    //-------------------------------------------------------------------------
    _endTurn() {
        this.onEndTurn.dispatch(this._gameState.playersData.indexOf(this._playerData));
    }

    //-------------------------------------------------------------------------
    _removeCardsToKeepFrom(aiCards: CardData[], cardsToKeep: CardData[]): CardData[] {
        const out: CardData[] = [];

        aiCards.forEach((aiCard) => {
            const index = cardsToKeep.indexOf(aiCard);
            if (index === -1) {
                out.push(aiCard);
            }
        });

        return(out);
    }

    //-------------------------------------------------------------------------
    _initNextMoveTimer() {
        const AUTO_DESTROY = false;
        this._nextMoveTimer = this._gameState.game.time.create(AUTO_DESTROY);
        this._nextMoveTimer.start();
    }

    //-------------------------------------------------------------------------
    _onAddedDiscardedCard() {
        this._playerData.pickedDiscardedCard =
            this._playerData.cardsData[this._playerData.cardsData.length - 1];
        this._startNextMoveDelayTimer(this._onAddedDiscardedCardDelayTimerComplete);
    }

    //-------------------------------------------------------------------------
    _onAddedDiscardedCardDelayTimerComplete() {
        if (this._checkHandForNewMelds()) {
            return;
        }

        if (this._checkHandForLayoffMelds()) {
            return;
        }

        this._discardCard();
    }

    //-------------------------------------------------------------------------
    _onDrewCard() {
        this._startNextMoveDelayTimer(this._onDrewCardDelayTimerComplete);
    }

    //-------------------------------------------------------------------------
    _onDrewCardDelayTimerComplete() {
        if (this._checkHandForNewMelds()) {
            return;
        }

        if (this._checkHandForLayoffMelds()) {
            return;
        }

        this._discardCard();
    }

    //-------------------------------------------------------------------------
    _onMeldHandledComplete() {
        this._startNextMoveDelayTimer(this._onMeldHandledDelayComplete);
    }

    //-------------------------------------------------------------------------
    _onMeldHandledDelayComplete() {
        if (!this._checkHandForNewMelds() && !this._checkHandForLayoffMelds()) {
            this._discardCard();
        }
    }
    
    //-------------------------------------------------------------------------
    _onPresentMeldCardsComplete() {
        this._startNextMoveDelayTimer(this._onPresentMeldPrepTimerComplete,
            Ai._PRESENT_MELD_DELAY);
    }

    //-------------------------------------------------------------------------
    _onPresentMeldPrepTimerComplete() {
        this._resumeTurnCallback = this._onMeldHandledComplete;
        this._gameState.handleMeldFinished(this._targetLayoffMeldImage);
        this._targetLayoffMeldImage = null;
    }

    //-------------------------------------------------------------------------
    _pickDiscardedCard() {
        this._resumeTurnCallback = this._onAddedDiscardedCard;
        this._gameState.addDiscardedCardToHand();
    }

    //-------------------------------------------------------------------------
    _presentLayoffMeld(cardsData: CardData[], meldImage: MeldImage) {
        this._resumeTurnCallback = this._onPresentMeldCardsComplete;
        this._targetLayoffMeldImage = meldImage;
        this._gameState.presentAiMeldPrepCards(cardsData, meldImage);
    }

    //-------------------------------------------------------------------------
    _presentNewMeld(cardsData: CardData[], meldType: MeldType) {
        this._resumeTurnCallback = this._onPresentMeldCardsComplete;
        this.meldType = meldType;
        this._gameState.presentAiMeldPrepCards(cardsData, null, meldType);
    }

    //-------------------------------------------------------------------------
    _shouldPickDiscardedCard(): boolean {
        const discardedCard = this._gameState.cardsManager.topDiscardCard;
        if (!discardedCard) {
            return(false); //sanity check
        }

        const isRiggedInPlayersFavor = this._gameState.isRiggedInPlayersFavor;

        //pick the discarded card if it can lay off an existing meld
        const discardedCardImages = [ this._gameState.getCardImage(discardedCard) ];
        const meldImages = this._gameState.melds.getValidLayoffMelds(discardedCardImages);
        if (meldImages.length > 0) {
            this._layOffMeldWithPickedDiscardedCard = true;
            
            // if rigging is in player's favor, ai should not pick the discarded card if the ai,
            // in an attempt to lay off an existing meld, is ai has one card in hand. this would
            // allow ai to lay off the discarded, and discard that one card for the win
            // (ai sholdn't win if rigging is in player's favor)
            if (isRiggedInPlayersFavor) {
                if (this._playerData.cardsData.length === 1) {
                    return(false);
                }
            }

            return(true);
        }

        //count number of cards in hand that have the same value the discarded
        // card. If there's at least one, the discarded card shoule be added to
        // the hand.
        const numOfMatchCards = CardMatchUtils.CountCardsByValue(this._playerData.cardsData,
            discardedCard.value);
        if (numOfMatchCards > 0) {
            // if rigging is in player's favor, ai should not pick the discarded card if the
            // ai already has two cards in hand to form a new meld, and the hand contains
            // three cards total. ai could create the meld, and discard the last card for the win
            // (ai sholdn't win if rigging is in player's favor)
            if (isRiggedInPlayersFavor) {
                if (this._playerData.cardsData.length === 3 && numOfMatchCards === 2) {
                    return(false);
                }
            } else {
                return(true);
            }
        }

        //if any card in the hand has a rank that is +/-1 or +/-2 of the discarded
        // card's rank, and the suits match, the discarded card shoule be added to
        // the hand.
        for (let index = 0; index < this._playerData.numCards; index++) {
            const playerCardData = this._playerData.getCardDataAt(index);
            if (discardedCard.suit !== playerCardData.suit) {
                continue;
            }

            if (isRiggedInPlayersFavor) {
                return(false);
            }

            //if the discarded card and the ai's currently checked card (playerCardData)
            // can create a sf meld using a third card from the deck, then pick this discarded
            // card. otherwise, dont pick it
            const sfCards =
                CardMatchUtils.SortCardsByAscendingValue([playerCardData, discardedCard]);
                    
            if (this._areCardRanksWithinRange(discardedCard.value, playerCardData.value, 1)) {
                //check if third card with rank of +/-1 exists in the deck
                const sfCard = sfCards[0];
                const nRank = CardValueRanks.RankOf(sfCard.value);
                const cardValueDown = CardValueRanks.ValueOf(nRank - 1);
                let cardIndexes = this._deck.search(cardValueDown, sfCard.suit);
                if (cardIndexes.length === 0) {
                    const cardValueUp = CardValueRanks.ValueOf(nRank + 1);
                    cardIndexes = this._deck.search(cardValueUp, sfCard.suit);
                }

                if (cardIndexes.length > 0) {
                    //put this third card at end of deck so player doesn't possibly draw it next turn
                    //ai will draw this card next turn
                    const cardIndex = cardIndexes[0];
                    this._deck.swap(cardIndex, this._deck.length - 1);
                    this._gameState.cardsManager.aiCardToDrawNext = this._deck.getAt(this._deck.length - 1);
                    return(true);
                }
            }
            
            if (this._areCardRanksWithinRange(discardedCard.value, playerCardData.value, 2)) {
                //since the two cards differ in rank by two, check if third card's rank is
                //in between these two cards
                const sfCard = sfCards[0];
                const nRank = CardValueRanks.RankOf(sfCard.value);
                const cardValueUp = CardValueRanks.ValueOf(nRank + 1);
                const cardIndexes = this._deck.search(cardValueUp, sfCard.suit);

                if (cardIndexes.length > 0) {
                    //put this third card at end of deck so player doesn't possibly draw it next turn
                    //ai will draw this card next turn
                    const cardIndex = cardIndexes[0];
                    this._deck.swap(cardIndex, this._deck.length - 1);
                    this._gameState.cardsManager.aiCardToDrawNext = this._deck.getAt(this._deck.length - 1);
                    return(true);
                }
            }
        }

        return(false);
    }

    //-------------------------------------------------------------------------
    _startNextMoveDelayTimer(callbackFunction: Function, speed?: number) {
        if (speed === undefined) {
            speed = this._calcAiSpeed();
        }

        this._nextMoveTimer.add(speed || speed, callbackFunction, this);
    }
}
