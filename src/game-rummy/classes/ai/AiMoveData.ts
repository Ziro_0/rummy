import { AiMove } from "./AiMove";

export default class AiMoveData {
    aiMove: AiMove;
    cardSlotIndex: number;

    //=========================================================================
    // constructor
    //=========================================================================
    /** @constructor */
    constructor(aiMove: AiMove, cardSlotIndex: number = -1) {
        this.aiMove = aiMove;
        this.cardSlotIndex = cardSlotIndex;
    }
}
