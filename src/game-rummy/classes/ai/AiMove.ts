export enum AiMove {
    CHECK_FOR_MELD_LAYOFFS = 'checkForMeldLayOffs',
    CHECK_FOR_NEW_MELDS = 'checkForNewMelds',
    DISCARD = 'discard',
    DRAW = 'draw',
    PICK_DISCARD = 'pickDiscard'
}
