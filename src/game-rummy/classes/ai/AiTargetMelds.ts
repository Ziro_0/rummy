import IGameState from '../../interfaces/IGameState';
import CardData from '../CardData';
import CardImage from '../CardImage';
import CardMatchUtils from '../cardMatchUtils/CardMatchUtils';
import CardsManager from '../CardsManager';
import PlayerData from '../PlayerData';

export default class AiTargetMelds {
    gameState: IGameState;
    aiPlayerData: PlayerData;
    playerData: PlayerData;
    cardsManager: CardsManager;

    constructor(gameState: IGameState, aiPlayerData: PlayerData, playerData: PlayerData,
    cardsManager: CardsManager) {
        this.gameState = gameState;
        this.aiPlayerData = aiPlayerData;
        this.playerData = playerData;
        this.cardsManager = cardsManager;
    }

    //=========================================================================
    // public
    //=========================================================================

    //-------------------------------------------------------------------------
    check1HandCardWithPlayersHand(targetCards_out: CardData[]): boolean {
        const aiPlayerCards = this.aiPlayerData.cardsData;
        const playerCards = this.gameState.getPlayerDataAt(this.gameState.thisPlayerIndex).cardsData;
        
        for (let index1 = 0; index1 < aiPlayerCards.length; index1 += 1) {
            const aiPlayerCard = aiPlayerCards[index1];
            
            for (let index2_1 = 0; index2_1 < playerCards.length - 1; index2_1 += 1) {
                const playerCard1 = playerCards[index2_1];
                
                for (let index2_2 = index2_1 + 1; index2_2 < playerCards.length; index2_2 += 1) {
                    const playerCard2 = playerCards[index2_2];
                    const cardsToCheck = [aiPlayerCard, playerCard1, playerCard2];
                    if (this._checkCardsForSameValue(cardsToCheck) ||
                    this._checkCardsForSf(cardsToCheck)) {
                        this._addUniqueCard(targetCards_out, aiPlayerCard);
                    }
                }
            }
        }

        return(targetCards_out.length > 0);
    }

    //-------------------------------------------------------------------------
    getCardsToKeepUsing1Card(targetCards_in_out: CardData[] = null): CardData[] {
        const targetCards: CardData[] = targetCards_in_out ? targetCards_in_out : [];
        this._check1HandCards(targetCards);
        return(targetCards);
    }

    //-------------------------------------------------------------------------
    getCardsToKeepUsing2Cards(targetCards_in_out: CardData[] = null): CardData[] {
        const targetCards: CardData[] = targetCards_in_out ? targetCards_in_out : [];
        this._check2HandCards(targetCards);
        return(targetCards);
    }

    //=========================================================================
    // private
    //=========================================================================

    //-------------------------------------------------------------------------
    _addUniqueCard(targetCards_in_out: CardData[], cardsToAdd: CardData | CardData[]) {
        const _cardsToAdd: CardData[] = Array.isArray(cardsToAdd) ? cardsToAdd : [cardsToAdd];

        _cardsToAdd.forEach((cardToAdd) => {
            if (targetCards_in_out && cardToAdd && targetCards_in_out.indexOf(cardToAdd) === -1) {
                targetCards_in_out.push(cardToAdd);
            }
        });
    }

    //-------------------------------------------------------------------------
    _check1HandCards(targetCards_out: CardData[]): boolean {
        this.check1HandCardWithPlayersHand(targetCards_out);
        this._check1HandCardWithOwnHand(targetCards_out);
        return(targetCards_out.length > 0);
    }

    //-------------------------------------------------------------------------
    _check1HandCardWithOwnHand(targetCards_out: CardData[]): boolean {
        const aiPlayerCards = this.aiPlayerData.cardsData;
        for (let index1 = 0; index1 < aiPlayerCards.length - 1; index1 += 1) {
            const card1 = aiPlayerCards[index1];
            
            for (let index2 = index1 + 1; index2 < aiPlayerCards.length; index2 += 1) {
                const card2 = aiPlayerCards[index2];
                const cards = [card1, card2];
                if (this._checkCardsForSameValue(cards) || this._checkCardsForSf(cards)) {
                    this._addUniqueCard(targetCards_out, cards);
                }
            }
        }

        return(targetCards_out.length > 0);
    }

    //-------------------------------------------------------------------------
    _check2HandCards(targetCards_out: CardData[]): boolean {
        const playerCards = this.aiPlayerData.cardsData;
        const NUM_CARDS_IN_HAND = playerCards.length;
        
        if (NUM_CARDS_IN_HAND < 2) {
            return(false);
        }
 
        for (let index1 = 0; index1 < NUM_CARDS_IN_HAND - 1; index1 += 1) {
            const card1 = playerCards[index1];
            for (let index2 = index1 + 1; index2 < NUM_CARDS_IN_HAND; index2 += 1) {
                const card2 = playerCards[index2];
                
                const cards: CardData[] = [card1, card2];
                let atLeastOnePassed = this._check2HandCardsWithDiscardedCard(cards);
                atLeastOnePassed = this._check2HandCardsWithDeck(cards) || atLeastOnePassed;
                atLeastOnePassed = this._check2HandCardsWithExistingMelds(cards) || atLeastOnePassed;
                atLeastOnePassed = this._check2HandCardsWithPlayersHand(cards) || atLeastOnePassed;
                
                if (atLeastOnePassed) {
                    this._addUniqueCard(targetCards_out, cards);
                }
            }
        }

        return(targetCards_out.length > 0);
    }

    //-------------------------------------------------------------------------
    _check2HandCardsWithDeck(cards: CardData[]): boolean {
        const cardsToCheck: CardData[] = cards.concat();
        const deckCards = this.cardsManager.deck.cardsData;
        for (let index = 0; index < deckCards.length; index += 1) {
            const deckCard = deckCards[index];
            cardsToCheck.push(deckCard);
            
            if (this._checkCardsForSameValue(cardsToCheck) || this._checkCardsForSf(cardsToCheck)) {
                return(true);
            }

            cardsToCheck.pop();
        }

        return(false);
    }

    //-------------------------------------------------------------------------
    _check2HandCardsWithDiscardedCard(cards: CardData[]): boolean {
        const cardsToCheck: CardData[] = cards.concat();
        cardsToCheck.push(this.cardsManager.topDiscardCard);
        return(
            this._checkCardsForSameValue(cardsToCheck) ||
            this._checkCardsForSf(cardsToCheck));
    }

    //-------------------------------------------------------------------------
    _check2HandCardsWithExistingMelds(handCards: CardData[]): boolean {
        const cardImagesToCkeck: CardImage[] = [];
        handCards.forEach((handCard) => {
            cardImagesToCkeck.push(this.gameState.getCardImage(handCard));
        }, this);

        return(this.gameState.melds.getValidLayoffMelds(cardImagesToCkeck).length > 0);
    }

    //-------------------------------------------------------------------------
    _check2HandCardsWithPlayersHand(handCards: CardData[]): boolean {
        const cardsToCheck: CardData[] = handCards.concat();
        const playersCards = this.playerData.cardsData;

        for (let index = 0; index < playersCards.length; index += 1) {
            const playerCard = playersCards[index];
            cardsToCheck.push(playerCard);
            
            if (this._checkCardsForSameValue(cardsToCheck) || this._checkCardsForSf(cardsToCheck)) {
                return(true);
            }

            cardsToCheck.pop();
        }

        return(false);
    }   

    //-------------------------------------------------------------------------
    _checkCardsForSameValue(cards: CardData[]): boolean {
        const cardsSortDes: CardData[] = CardMatchUtils.SortCardsByDescendingValue(cards);
        return(CardMatchUtils.DoCardsHaveSameValue(cardsSortDes, cardsSortDes.length));
    }

    //-------------------------------------------------------------------------
    _checkCardsForSf(cards: CardData[]): boolean {
        const cardsSortDes: CardData[] = CardMatchUtils.SortCardsByDescendingValue(cards);
        return(CardMatchUtils.AreCardsStraightFlush(cardsSortDes, cardsSortDes.length));
    }

}

