import IGameState from "../interfaces/IGameState";
import Button from "./Button";

export default class HelpPagesView extends Phaser.Group {
    static readonly _MAX_PAGES = 4;

    onClose: Phaser.Signal;

    _gameState: IGameState;

    _backButton: Button;
    _closeButton: Button;
    _nextButton: Button;

    _pageImageContainer: Phaser.Group;
    _currentPageIndex: number;

    //=========================================================================
    // constructor
    //=========================================================================
    /** @constructor */
    constructor(gameState: IGameState, parent: PIXI.DisplayObjectContainer) {
        super(gameState.game, parent);
        
        this.onClose = new Phaser.Signal();

        this._gameState = gameState;
        
        this._pageImageContainer = this._gameState.game.add.group(this);

        this._initButtons();

        this._showPage(0);
    }

    //=========================================================================
    // public
    //=========================================================================
    
    //-------------------------------------------------------------------------
    destroy() {
        this.onClose.dispose();
        super.destroy(true, false);
    }
    
    //=========================================================================
    // private
    //=========================================================================
    
    //-------------------------------------------------------------------------
    _initButton(baseKey: string, onPressedCallback: Function): Button {
        const jMetrics = this._gameState.metricsJsonData;
        const jHelpPagesButtons = jMetrics.helpPagesButtons;
        const jButtonData = jHelpPagesButtons[baseKey];

        const button = new Button(
            this._gameState,
            jButtonData.x, jButtonData.y,
            'ui',
            onPressedCallback,
            this,
            baseKey);

        this.add(button);
        return(button);
    }

    //-------------------------------------------------------------------------
    _initButtons() {
        this._backButton = this._initButton('game_button_back', this._onBackButtonPressed);
        this._closeButton = this._initButton('game_button_close', this._onCloseButtonPressed);
        this._nextButton = this._initButton('game_button_next', this._onNextButtonPressed);
    }

    //-------------------------------------------------------------------------
    _nextPage() {
        const pageIndex = this._currentPageIndex + 1;
        if (pageIndex < HelpPagesView._MAX_PAGES) {
            this._showPage(pageIndex);
        }
    }

    //-------------------------------------------------------------------------
    _onBackButtonPressed() {
        this._prevPage();
    }

    //-------------------------------------------------------------------------
    _onCloseButtonPressed() {
        this.onClose.dispatch(this);
    }

    //-------------------------------------------------------------------------
    _onNextButtonPressed() {
        this._nextPage();
    }

    //-------------------------------------------------------------------------
    _prevPage() {
        const pageIndex = this._currentPageIndex - 1;
        if (pageIndex >= 0) {
            this._showPage(pageIndex);
        }
    }

    //-------------------------------------------------------------------------
    _showPage(pageIndex) {
        this._currentPageIndex = pageIndex;
        this._updateImage();
        this._updateButtons();
    }

    //-------------------------------------------------------------------------
    _updateButtons() {
        this._backButton.visible = this._currentPageIndex > 0;
        this._nextButton.visible = this._currentPageIndex != HelpPagesView._MAX_PAGES - 1;
    }

    //-------------------------------------------------------------------------
    _updateImage() {
        const image = this._gameState.game.make.image(0, 0,
            `help-page-${this._currentPageIndex}`);

        if (image) {
            this._pageImageContainer.removeAll(true, true);
            this._pageImageContainer.add(image);
        }
    }
}
