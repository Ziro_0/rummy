import IGameState from "../interfaces/IGameState";
import { AudioEventKey } from "./audio/AudioEventKey";

export default class Button extends Phaser.Button {
    callback: Function;
    callbackContext: any;

    _gameState: IGameState;
    
    _normFrame: string;
    _overFrame: string;
    _downFrame: string;
    _disabledFrame: string;

    //=========================================================================
    // constructor
    //=========================================================================
    /** @constructor */
    constructor(gameState: IGameState, x: number, y: number, key: string,
    callback: Function, callbackContext: any, framePrefix: string) {
        const normFrame = `${framePrefix}_norm`;
        const overFrame = `${framePrefix}_over`;
        const downFrame = `${framePrefix}_down`;

        super(gameState.game, x, y, key, null, null, overFrame, normFrame,
            downFrame);

        this._gameState = gameState;

        this.callback = callback;
        this.callbackContext = callbackContext;

        this.onInputUp.add(this._onPressed, this);

        this.anchor.set(0.5);

        this._normFrame = normFrame;
        this._overFrame = overFrame;
        this._downFrame = downFrame;
        this._disabledFrame = `${framePrefix}_disabled`;
    }

    //=========================================================================
    // public
    //=========================================================================

    //-------------------------------------------------------------------------
    destroy() {
        this._gameState = null;
        this.callback = null;
        this.callbackContext = null;

        this.clearFrames();
        super.destroy();
    }

    //-------------------------------------------------------------------------
    get enabled(): boolean {
        return(this.input.enabled);
    }

    //-------------------------------------------------------------------------
    set enabled(value: boolean) {
        this.input.enabled = value;

        if (!value) {
            this.frameName = this._disabledFrame;
            this.freezeFrames = true;
            return;
        }

        this.freezeFrames = false;

        if (this.input.pointerOver) {
            this.frameName = this._overFrame;
        } else {
            this.frameName = this._normFrame;
        }
    }

    //=========================================================================
    // private
    //=========================================================================

    //-------------------------------------------------------------------------
    _onPressed(button: Phaser.Button, pointer: Phaser.Pointer) {
        this._gameState.playSound(AudioEventKey.BUTTON_PRESS);

        if (this.callback) {
            this.callback.call(this.callbackContext, button, pointer);
        }
    }
}
