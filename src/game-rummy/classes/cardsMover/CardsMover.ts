import IGameState from "../../interfaces/IGameState";
import CardImage from "../CardImage";
import CardMoveData from "./CardMoveData";
import CardData from "../CardData";
import PlayerData from "../PlayerData";
import Deck from "../Deck";
import DeckImage from "../DeckImage";
import { CardMoveTarget } from "./CardMoveTarget";
import { LocalToGlobal } from "./CoordinateSpace";
import MeldImage from "../MeldImage";
import { AudioEventKey } from "../audio/AudioEventKey";
import DeckRigger from "../DeckRigger";

export default class CardsMover {
    static readonly MOVE_SPEED_ADD_TO_HAND_MS = 100;
    static readonly MOVE_SPEED_MELD_PREP_MS = 100;
    static readonly MOVE_SPEED_DISCARD_MS = 100;
    static readonly MOVE_SPEED_MELD_COMPLETE_MS = 250;
    static readonly MOVE_SPEED_RECYCLE_MS = 50;
    static readonly MOVE_SPEED_SORT_MS = 75;

    onComplete: Phaser.Signal;

    _cardImagesByCardData: Map<CardData, CardImage>;

    _discardedCardsData: CardData[];
    _moveDataQueue: CardMoveData[];
    _meldPrepYPositionForPlayers: number[];

    _gameState: IGameState;
    
    _deck: Deck;
    _deckImage: DeckImage;
    _deckRigger: DeckRigger;

    _currentTween: Phaser.Tween;

    _cardsInHandOffset: number;
    _meldPrepCardsOffset: number;

    //=========================================================================
    // constructor
    //=========================================================================
    /** @constructor */
    constructor(gameState: IGameState, cardImagesByCardData: Map<CardData, CardImage>,
    deck: Deck, deckImage: DeckImage, discardedCardsData: CardData[],
    deckRigger: DeckRigger) {
        this._gameState = gameState;
        this._cardImagesByCardData = cardImagesByCardData;
        this._deck = deck;
        this._deckImage = deckImage;
        this._discardedCardsData = discardedCardsData;
        this._deckRigger = deckRigger;

        this.onComplete = new Phaser.Signal();

        const jsonData = this._gameState.metricsJsonData;
        this._cardsInHandOffset = jsonData.cardsInHandOffset;
        
        const jMeldPrep = jsonData.meldPrep;
        this._meldPrepCardsOffset = jMeldPrep.cardsOffset;
        this._meldPrepYPositionForPlayers = jMeldPrep.yPositionForPlayers;

        this._moveDataQueue = [];
    }

    //=========================================================================
    // public
    //=========================================================================

    //-------------------------------------------------------------------------
    dispose() {
        this.onComplete.dispose();
        this._gameState = null;
        this._moveDataQueue.length = 0;
        this._cardImagesByCardData = null;
        this._deck = null;
        this._deckImage = null;
        this._removeTween();
    }

    //-------------------------------------------------------------------------
    enqueue(source: CardMoveTarget, target: CardMoveTarget, playerIndex: number = -1,
    cardSlotIndex: number = -1, meldImage: MeldImage = null, playSound: boolean = true,
    targetCardSlotIndexex: number[] = null, useRigger: boolean = false) {
        if (source === CardMoveTarget.DECK) {
            if (target === CardMoveTarget.DISCARD) {
                this._enqueueFromDeckToDiscard(playerIndex, useRigger, playSound);
            } else if (target === CardMoveTarget.HAND) {
                this._enqueueFromDeckToHand(playerIndex, playSound, useRigger);
            }
        } else if (source === CardMoveTarget.DISCARD) {
            if (target === CardMoveTarget.DECK) {
                this._enqueueFromDiscardToDeck();
            } else if (target === CardMoveTarget.HAND) {
                this._enqueueFromDiscardToHand(playerIndex, playSound);
            }
        } else if (source === CardMoveTarget.HAND) {
            if (target === CardMoveTarget.DISCARD) {
                this._enqueueFromHandToDiscard(playerIndex, cardSlotIndex, playSound);
            } else if (target === CardMoveTarget.MELD) {
                this._enqueueFromHandToMeld(playerIndex, cardSlotIndex);
            } else if (target === CardMoveTarget.HAND) {
                this._enqueueSortCardsInHands(playerIndex, cardSlotIndex, targetCardSlotIndexex);
            }
            
        } else if (source === CardMoveTarget.MELD) {
            if (target === CardMoveTarget.HAND) {
                this._enqueueFromMeldToHand(playerIndex, cardSlotIndex);
            } else if (target === CardMoveTarget.MELD_IMAGE) {
                this._enqueueFromMeldToMeldImage(playerIndex, meldImage);
            }
        }
    }

    //-------------------------------------------------------------------------
    play() {
        this._moveNextCard();
    }

    //=========================================================================
    // private
    //=========================================================================

    //-------------------------------------------------------------------------
    _calcCardPositions(numCards: number, offset: number, yCardsPosition: number,
    out?: Phaser.Point[]): Phaser.Point[] {
        if (out) {
            out.length = 0;
        } else {
            out = [];
        }
        
        const nc_div_2 = Math.floor(numCards / 2);
        const xCenter = this._gameState.game.world.centerX;
        let xStart: number;

        if ((numCards & 1) === 1) {
            //starting position for hands with an odd number of cards
            xStart = xCenter - nc_div_2 * offset;
        } else {
            //starting position for hands with an even number of cards
            xStart = (xCenter - offset / 2) - Math.floor((numCards - 1) / 2) * offset;
        }

        let x = xStart;
        for (let index = 0; index < numCards; index++) {
            out.push(new Phaser.Point(x, yCardsPosition));
            x += offset;
        }

        return(out);
    }

    //-------------------------------------------------------------------------
    _calcMeldCardPositions(playerData: PlayerData, out?: Phaser.Point[]): Phaser.Point[] {
        return(this._calcCardPositions(
            this._gameState.meldPrepCardImages.length,
            this._meldPrepCardsOffset,
            this._meldPrepYPositionForPlayers[playerData.index],
            out));
    }

    //-------------------------------------------------------------------------
    _calcPlayerCardPositions(playerData: PlayerData, out?: Phaser.Point[]): Phaser.Point[] {
        return(this._calcCardPositions(
            playerData.numCards,
            this._cardsInHandOffset,
            playerData.yCardsPosition,
            out));
    }

    //-------------------------------------------------------------------------
    _complete(cardImage: CardImage, data: CardMoveData) {
        let discardedCardImage: CardImage;

        if (data.target === CardMoveTarget.DISCARD) {
            discardedCardImage = cardImage;
        }

        this.onComplete.dispatch(this, discardedCardImage);
    }

    //-------------------------------------------------------------------------
    _completeMeldVisuals(meldImage: MeldImage) {
        this._destroyMeldCardImages();
        meldImage.visible = true;
    }

    //-------------------------------------------------------------------------
    _destroyCardImage(cardImage: CardImage): CardImage {
        if (!cardImage) {
            return(null);
        }

        this._cardImagesByCardData.delete(cardImage.cardData);
        cardImage.destroy();
        return(null);
    }

    //-------------------------------------------------------------------------
    _destroyMeldCardImages() {
        this._gameState.meldPrepCardImages.forEach(function(cardImage) {
            this._destroyCardImage(cardImage);
        }, this);

        this._gameState.meldPrepCardImages.length = 0;
    }

    //-------------------------------------------------------------------------
    _enqueueFromDeckToDiscard(playerIndex: number, useRigger: boolean, playSound: boolean) {
        this._moveDataQueue.push(
            new CardMoveData(
                CardMoveTarget.DECK,
                CardMoveTarget.DISCARD,
                playerIndex,
                null, null, null,
                playSound,
                null,
                useRigger));
    }

    //-------------------------------------------------------------------------
    _enqueueFromDeckToHand(playerIndex: number, playSound: boolean,
    useRigger: boolean) {
        this._moveDataQueue.push(
            new CardMoveData(
                CardMoveTarget.DECK,
                CardMoveTarget.HAND,
                playerIndex,
                -1,
                this._isFaceDown(playerIndex),
                null,
                playSound,
                null,
                useRigger));
    }

    //-------------------------------------------------------------------------
    _enqueueFromDiscardToDeck() {
        this._moveDataQueue.push(
            new CardMoveData(
                CardMoveTarget.DISCARD,
                CardMoveTarget.DECK));
    }

    //-------------------------------------------------------------------------
    _enqueueFromDiscardToHand(playerIndex: number, playSound: boolean) {
        this._moveDataQueue.push(
            new CardMoveData(
                CardMoveTarget.DISCARD,
                CardMoveTarget.HAND,
                playerIndex,
                -1,
                this._isFaceDown(playerIndex),
                null,
                playSound));
    }

    //-------------------------------------------------------------------------
    _enqueueFromHandToMeld(playerIndex: number, playerCardDataIndex: number) {
        const IS_FACE_DOWN = false;
        this._moveDataQueue.push(
            new CardMoveData(
                CardMoveTarget.HAND,
                CardMoveTarget.MELD,
                playerIndex,
                playerCardDataIndex,
                IS_FACE_DOWN));
    }

    //-------------------------------------------------------------------------
    _enqueueFromHandToDiscard(playerIndex: number, playerCardDataIndex: number,
    playSound: boolean) {
        const IS_FACE_DOWN = false;
        this._moveDataQueue.push(
            new CardMoveData(
                CardMoveTarget.HAND,
                CardMoveTarget.DISCARD,
                playerIndex,
                playerCardDataIndex,
                IS_FACE_DOWN,
                null,
                playSound));
    }

    //-------------------------------------------------------------------------
    _enqueueFromMeldToHand(playerIndex: number, meldPrepCardIndex: number) {
        const IS_FACE_DOWN = false;
        this._moveDataQueue.push(
            new CardMoveData(
                CardMoveTarget.MELD,
                CardMoveTarget.HAND,
                playerIndex,
                meldPrepCardIndex,
                IS_FACE_DOWN));
    }

    //-------------------------------------------------------------------------
    _enqueueFromMeldToMeldImage(playerIndex: number, meldImage: MeldImage) {
        this._moveDataQueue.push(
            new CardMoveData(
                CardMoveTarget.MELD,
                CardMoveTarget.MELD_IMAGE,
                playerIndex,
                -1,
                false,
                meldImage));
    }

    //-------------------------------------------------------------------------
    _enqueueSortCardsInHands(playerIndex: number, cardSlotIndex: number,
    targetCardSlotIndexex: number[]) {
        this._moveDataQueue.push(
            new CardMoveData(
                CardMoveTarget.HAND,
                CardMoveTarget.HAND,
                playerIndex,
                cardSlotIndex,
                null,
                null,
                null,
                targetCardSlotIndexex));
    }

    //-------------------------------------------------------------------------
    _handleCardImageMovedToDeck(cardImage: CardImage) {
        this._deck.add(cardImage.cardData);
        this._updateDeckImage();
        this._destroyCardImage(cardImage);
    }

    //-------------------------------------------------------------------------
    _isFaceDown(playerIndex: number): boolean {
        if (this._gameState.canSeeOtherPlayersCards) {
            return(false);
        }

        if (this._gameState.isTutorialOpen()) {
            return(playerIndex !== 0);
        }

        return(playerIndex !== this._gameState.thisPlayerIndex);
    }

    //-------------------------------------------------------------------------
    _moveCard(data: CardMoveData) {
        this._moveTargetCard(data);
    }

    //-------------------------------------------------------------------------
    _moveExistingCards(positions: Phaser.Point[], cardImages: CardImage[],
    targetCardDataIndex: number, moveSpeed: number) {
        for (let index = 0; index < positions.length; index++) {
            if (index === targetCardDataIndex) {
                //dont move the new card that was just added to the hand
                continue;
            }

            const cardImage = cardImages[index];
            const properties = {
                x: positions[index].x,
                y: positions[index].y
            };
    
            this._gameState.game.tweens.create(cardImage)
                .to(properties, moveSpeed)
                .start();
        }
    }

    //-------------------------------------------------------------------------
    _moveExistingCardsInMeld(playerData: PlayerData, targetCardIndex: number = -1) {
        const existingCardPositions = this._calcMeldCardPositions(playerData);
        this._moveExistingCards(
            existingCardPositions,
            this._gameState.meldPrepCardImages,
            targetCardIndex,
            CardsMover.MOVE_SPEED_MELD_PREP_MS);
    }

    //-------------------------------------------------------------------------
    _moveExistingCardsInHand(playerData: PlayerData, positions: Phaser.Point[],
    moveSpeed: number, targetCardDataIndex: number = -1) {
        const cardImages: CardImage[] = [];

        for (let index = 0; index < playerData.numCards; index++) {
            const cardData = playerData.getCardDataAt(index);
            cardImages.push(this._cardImagesByCardData.get(cardData));
        }

        this._moveExistingCards(positions, cardImages, targetCardDataIndex, moveSpeed);
    }

    //-------------------------------------------------------------------------
    _moveMeldCardsToMeldImage(data: CardMoveData) {
        const meldImagePosition = new Phaser.Point(data.meldImage.x, data.meldImage.y);
        const meldImageLocalBounds = data.meldImage.getLocalBounds();

        for (let meldCardIndex = 0;
        meldCardIndex < this._gameState.meldPrepCardImages.length;
        meldCardIndex++) {
            const meldCardImage = this._gameState.meldPrepCardImages[meldCardIndex];

            const meldImageGlobalPoint = LocalToGlobal(
                this._gameState.game,
                data.meldImage,
                meldImagePosition);
            
            meldImageGlobalPoint.x += meldImageLocalBounds.centerX;
            meldImageGlobalPoint.y += meldImageLocalBounds.centerY;
            

            const tweenProps = {
                x: meldImageGlobalPoint.x,
                y: meldImageGlobalPoint.y
            };

            const tween = this._gameState.game.tweens.create(meldCardImage)
                .to(tweenProps, CardsMover.MOVE_SPEED_MELD_COMPLETE_MS)
                .start();
                
            if (meldCardIndex === 0) {
                this._currentTween = tween;
                this._currentTween.onComplete.addOnce(this._onTweenMoveCardComplete,
                    this, 0, data);
            }
        }
    }

    //-------------------------------------------------------------------------
    _moveNextCard(): boolean {
        const data = this._moveDataQueue.shift();
        if (!data) {
            return(false);
        }

        this._moveCard(data);
        return(true);
    }

    //-------------------------------------------------------------------------
    _moveNextCardOrComplete(cardImage?: CardImage, data?: CardMoveData): number {
        if (!this._moveNextCard()) {
            this._complete(cardImage, data);
        }
        return(0);
    }

    //-------------------------------------------------------------------------
    _moveTargetAllCardsFromMeldToHand(data: CardMoveData) {
        const playerData = this._gameState.getPlayerDataAt(data.playerIndex);
        const targetHandPositions = this._calcPlayerCardPositions(playerData);

        for (let targetCardIndex = 0; targetCardIndex < targetHandPositions.length;
        targetCardIndex++) {
            const cardData = playerData.getCardDataAt(targetCardIndex);
            const cardImage = this._cardImagesByCardData.get(cardData);
            const targetHandPosition = targetHandPositions[targetCardIndex];
            const props = {
                x: targetHandPosition.x,
                y: targetHandPosition.y
            };
            
            this._currentTween = this._gameState.game.tweens.create(cardImage)
                .to(props, CardsMover.MOVE_SPEED_MELD_PREP_MS)
                .start();
                
            if (targetCardIndex === 0) {
                this._currentTween.onComplete.addOnce(this._onTweenMoveCardComplete,
                    this, 0, data);
            }
        }

        this._gameState.meldPrepCardImages.length = 0;
    }

    //-------------------------------------------------------------------------
    _moveTargetAllCardsFromMeldToMeldImage(data: CardMoveData) {
        this._removeMeldPrepCardDataFromPlayer(data.playerIndex);
        this._moveMeldCardsToMeldImage(data);
        
        const IS_SOLO_TWEEN: boolean = false;
        this._updatePlayerCardPositions(data, IS_SOLO_TWEEN,
            CardsMover.MOVE_SPEED_MELD_COMPLETE_MS);
    }

    //-------------------------------------------------------------------------
    _moveTargetCard(data: CardMoveData) {
        if (data.source === CardMoveTarget.DECK) {
            if (data.target === CardMoveTarget.HAND) {
                this._moveTargetCardFromDeckToHand(data);
            } else if (data.target === CardMoveTarget.DISCARD) {
                this._moveTargetCardFromDeckToDiscard(data);
            }
        } else if (data.source === CardMoveTarget.DISCARD) {
            if (data.target === CardMoveTarget.HAND) {
                this._moveTargetCardFromDiscardToHand(data);
            } else if (data.target === CardMoveTarget.DECK) {
                this._moveTargetCardFromDiscardToDeck(data);
            }
        } else if (data.source === CardMoveTarget.HAND) {
            if (data.target === CardMoveTarget.DISCARD) {
                this._moveTargetCardFromHandToDiscard(data);
            } if (data.target === CardMoveTarget.MELD) {
                this._moveTargetCardFromHandToMeld(data);
            } if (data.target === CardMoveTarget.HAND) {
                this._moveTargetSortCardsInHand(data);
            }
        } else if (data.source === CardMoveTarget.MELD) {
            if (data.target === CardMoveTarget.HAND) {
                if (data.cardSlotIndex === -1) {
                    this._moveTargetAllCardsFromMeldToHand(data);
                } else {
                    this._moveTargetCardFromMeldToHand(data);
                }
            } else if (data.target === CardMoveTarget.MELD_IMAGE) {
                this._moveTargetAllCardsFromMeldToMeldImage(data);
            }
        }
    }

    //-------------------------------------------------------------------------
    _moveTargetCardFromDeckToDiscard(data: CardMoveData) {
        if (data.useRigger) {
            if (typeof this._gameState.isRiggedInPlayersFavor === "boolean") {
                this._deckRigger.rigDrawCard(data.playerIndex);
                this._gameState.setNextRiggedInPlayersFavorIndex();
            }
        }
        
        const cardData = this._deck.remove();
        if (!cardData) {
            console.warn("deck empty");
            return(this._moveNextCardOrComplete(null, data));
        }

        const sourceCardPosition = this._deckImage.getCardPosition();
        const cardImage = new CardImage(
            this._gameState.game,
            sourceCardPosition.x,
            sourceCardPosition.y,
            cardData,
            this._gameState.deckAndDiscardGroup);
        
        cardImage.isFaceDown = false;

        this._cardImagesByCardData.set(cardData, cardImage);
        
        const targetCardPosition = this._gameState.discardPilePosition;
        const props = {
            x: targetCardPosition.x,
            y: targetCardPosition.y
        };

        this._currentTween = this._gameState.game.tweens.create(cardImage)
            .to(props, CardsMover.MOVE_SPEED_DISCARD_MS)
            .start();
        this._currentTween.onComplete.addOnce(this._onTweenMoveCardComplete, this, 0, data);

        if (data.playSound) {
            this._gameState.playSound(AudioEventKey.DISCARD);
        }
        
        this._updateDeckImage();
    }

    //-------------------------------------------------------------------------
    _moveTargetCardFromDeckToHand(data: CardMoveData) {
        if (data.useRigger) {
            if (typeof this._gameState.isRiggedInPlayersFavor === "boolean") {
                this._deckRigger.rigDrawCard(data.playerIndex);
                this._gameState.setNextRiggedInPlayersFavorIndex();
            }
        }
        
        const cardData = this._deck.remove();
        if (!cardData) {
            console.warn("deck empty");
            return(this._moveNextCardOrComplete(null, data));
        }

        const sourceCardPosition = this._deckImage.getCardPosition();
        const cardImage = new CardImage(
            this._gameState.game,
            sourceCardPosition.x,
            sourceCardPosition.y,
            cardData,
            this._gameState.cardsGroup,
            this._gameState.onCardImageInputDownSgn);

        cardImage.isFaceDown = data.isFaceDown;
        
        this._cardImagesByCardData.set(cardData, cardImage);
        
        const playerData = this._gameState.getPlayerDataAt(data.playerIndex);
        playerData.addCardData(cardData);

        const IS_SOLO_TWEEN = true;
        this._updatePlayerCardPositions(data, IS_SOLO_TWEEN, CardsMover.MOVE_SPEED_ADD_TO_HAND_MS);
        this._updateDeckImage();

        if (data.playSound) {
            this._gameState.playSound(AudioEventKey.DEAL_CARD);
        }
    }

    //-------------------------------------------------------------------------
    _moveTargetCardFromDiscardToDeck(data: CardMoveData) {
        const cardImage = this._popDiscardedCard();
        if (!cardImage) {
            console.warn('no discarded card available')
            return(!this._moveNextCardOrComplete(null, data));
        }

        cardImage.isFaceDown = true;

        const targetCardPosition = this._deckImage.getCardPosition();
        const props = {
            x: targetCardPosition.x,
            y: targetCardPosition.y
        };

        this._currentTween = this._gameState.game.tweens.create(cardImage)
            .to(props, CardsMover.MOVE_SPEED_RECYCLE_MS)
            .start();
        this._currentTween.onComplete.addOnce(this._onTweenMoveCardComplete, this, 0, data);
    }

    //-------------------------------------------------------------------------
    _moveTargetCardFromDiscardToHand(data: CardMoveData) {
        const cardImage = this._popDiscardedCard();
        if (!cardImage) {
            console.warn('no discarded card available')
            return(!this._moveNextCardOrComplete(null, data));
        }

        cardImage.isFaceDown = data.isFaceDown;

        const playerData = this._gameState.getPlayerDataAt(data.playerIndex);
        playerData.addCardData(cardImage.cardData);

        this._gameState.cardsGroup.add(cardImage);

        const targetCardPosition = this._gameState.discardPilePosition;
        const props = {
            x: targetCardPosition.x,
            y: targetCardPosition.y
        };
        
        const MOVE_SPEED = CardsMover.MOVE_SPEED_DISCARD_MS;
        this._currentTween = this._gameState.game.tweens.create(cardImage)
            .to(props, MOVE_SPEED)
            .start();
        this._currentTween.onComplete.addOnce(this._onTweenMoveCardComplete, this, 0, data);
        
        const existingCardPositions = this._calcPlayerCardPositions(playerData);
        this._moveExistingCardsInHand(playerData, existingCardPositions, MOVE_SPEED);

        if (data.playSound) {
            this._gameState.playSound(AudioEventKey.DEAL_CARD);
        }
    }

    //-------------------------------------------------------------------------
    _moveTargetCardFromHandToDiscard(data: CardMoveData) {
        const playerData = this._gameState.getPlayerDataAt(data.playerIndex);
        const cardData = playerData.removeCardDataAt(data.cardSlotIndex);
        
        const cardImage = this._cardImagesByCardData.get(cardData);
        cardImage.isFaceDown = data.isFaceDown;

        const targetCardPosition = this._gameState.discardPilePosition;
        const props = {
            x: targetCardPosition.x,
            y: targetCardPosition.y
        };

        const MOVE_SPEED = CardsMover.MOVE_SPEED_DISCARD_MS;
        this._currentTween = this._gameState.game.tweens.create(cardImage)
            .to(props, MOVE_SPEED)
            .start();
        this._currentTween.onComplete.addOnce(this._onTweenMoveCardComplete, this, 0, data);
            
        const existingCardPositions = this._calcPlayerCardPositions(playerData);
        this._moveExistingCardsInHand(playerData, existingCardPositions, MOVE_SPEED);

        if (data.playSound) {
            this._gameState.playSound(AudioEventKey.DISCARD);
        }
    }

    //-------------------------------------------------------------------------
    _moveTargetCardFromHandToMeld(data: CardMoveData) {
        const playerData = this._gameState.getPlayerDataAt(data.playerIndex);
        const cardData = playerData.getCardDataAt(data.cardSlotIndex);
        const cardImage = this._cardImagesByCardData.get(cardData);
        cardImage.isFaceDown = false;

        this._gameState.meldPrepCardImages.push(cardImage);

        const targetCardPositions = this._calcMeldCardPositions(playerData);
        const targetCardIndex = this._gameState.meldPrepCardImages.length - 1;
        const targetCardPosition = targetCardPositions[targetCardIndex];
        const props = {
            x: targetCardPosition.x,
            y: targetCardPosition.y
        };
        
        this._currentTween = this._gameState.game.tweens.create(cardImage)
            .to(props, CardsMover.MOVE_SPEED_MELD_PREP_MS)
            .start();
        this._currentTween.onComplete.addOnce(this._onTweenMoveCardComplete, this, 0, data);

        this._moveExistingCardsInMeld(playerData, targetCardIndex);
    }

    //-------------------------------------------------------------------------
    _moveTargetCardFromMeldToHand(data: CardMoveData) {
        const cardImage = this._gameState.meldPrepCardImages[data.cardSlotIndex];
        this._gameState.meldPrepCardImages.splice(data.cardSlotIndex, 1);

        const playerData = this._gameState.getPlayerDataAt(data.playerIndex);
        const targetHandPositions = this._calcPlayerCardPositions(playerData);
        const targetCardIndex = playerData.getCardDataIndexOf(cardImage.cardData);
        const targetHandPosition = targetHandPositions[targetCardIndex];
        const props = {
            x: targetHandPosition.x,
            y: targetHandPosition.y
        };
        
        this._currentTween = this._gameState.game.tweens.create(cardImage)
            .to(props, CardsMover.MOVE_SPEED_MELD_PREP_MS)
            .start();
        this._currentTween.onComplete.addOnce(this._onTweenMoveCardComplete, this, 0, data);

        this._moveExistingCardsInMeld(playerData);
    }

    //-------------------------------------------------------------------------
    _moveTargetSortCardsInHand(data: CardMoveData) {
        const sortedCardDataIndexes = data.targetCardSlotIndexes;
        const playerData = this._gameState.getPlayerDataAt(data.playerIndex);

        const targetHandPositions = this._calcPlayerCardPositions(playerData);

        for (let targetIndex = 0; targetIndex < targetHandPositions.length; targetIndex++) {
            const sortedIndexIndex = sortedCardDataIndexes[targetIndex];
            
            const targetHandPosition = targetHandPositions[targetIndex];
            const props = {
                x: targetHandPosition.x,
                y: targetHandPosition.y
            };
            
            const cardData = playerData.getCardDataAt(sortedIndexIndex);
            const cardImage = this._cardImagesByCardData.get(cardData);
            
            this._gameState.cardsGroup.remove(cardImage, false, true);
            this._gameState.cardsGroup.addAt(cardImage, targetIndex, true);

            const tween = this._gameState.game.tweens.create(cardImage)
                .to(props, CardsMover.MOVE_SPEED_SORT_MS)
                .start();
                
            if (targetIndex === 0) {
                this._currentTween = tween;
                this._currentTween.onComplete.addOnce(this._onTweenMoveCardComplete,
                    this, 0, data);
            }
        }

        playerData.setCardDataPositions(sortedCardDataIndexes);
    }

    //-------------------------------------------------------------------------
    _onTweenMoveCardComplete(cardImage: CardImage, tween: Phaser.Tween, data: CardMoveData) {
        if (data.target === CardMoveTarget.DECK) {
            this._handleCardImageMovedToDeck(cardImage);
        } else if (data.target === CardMoveTarget.DISCARD) {
            this._pushDiscardedCard(cardImage);
        } else if (data.target === CardMoveTarget.MELD_IMAGE) {
            this._completeMeldVisuals(data.meldImage);
        } else if (data.target === CardMoveTarget.HAND) {
            cardImage.onInputDown = this._gameState.onCardImageInputDownSgn;
        }

        this._currentTween = null;
        
        if (!this._moveNextCard()) {
            this._complete(cardImage, data);
        }
    }

    //-------------------------------------------------------------------------
    _popDiscardedCard(): CardImage {
        const cardData = this._discardedCardsData.pop();
        if (!cardData) {
            console.warn('no discarded card available');
            return(null);
        }

        const cardImage = this._cardImagesByCardData.get(cardData);
        cardImage.onInputDown = null;

        const topDiscardedCardCata =
            this._discardedCardsData[this._discardedCardsData.length - 1];
        if (topDiscardedCardCata) {
            const position = this._gameState.discardPilePosition;
            const nextDiscardedCardImage = new CardImage(
                this._gameState.game,
                position.x,
                position.y,
                topDiscardedCardCata,
                this._gameState.deckAndDiscardGroup,
                this._gameState.onDiscardCardImageInputDownSgn);
            
            this._cardImagesByCardData.set(topDiscardedCardCata,
                nextDiscardedCardImage);
        }

        return(cardImage);
    }

    //-------------------------------------------------------------------------
    _pushDiscardedCard(discardedCardImage: CardImage) {
        if (discardedCardImage) {
            this._discardedCardsData.push(discardedCardImage.cardData);
            this._gameState.deckAndDiscardGroup.add(discardedCardImage);
            discardedCardImage.onInputDown = this._gameState.onDiscardCardImageInputDownSgn;
        }

        const previousDiscardedCardCata =
            this._discardedCardsData[this._discardedCardsData.length - 2];
        const previousDiscardedCardImage = this._cardImagesByCardData.get(previousDiscardedCardCata);
        if (previousDiscardedCardImage) {
            this._destroyCardImage(previousDiscardedCardImage);
        }
    }

    //-------------------------------------------------------------------------
    _removeMeldPrepCardDataFromPlayer(playerIndex: number) {
        const playerData = this._gameState.getPlayerDataAt(playerIndex);
        this._gameState.meldPrepCardImages.forEach(function(cardImage) {
            playerData.removeCardData(cardImage.cardData);
        });
    }
    
    //-------------------------------------------------------------------------
    _removeTween() {
        if (this._currentTween) {
            this._currentTween.stop();
        }
    }

    //-------------------------------------------------------------------------
    _updateDeckImage() {
        this._deckImage.updateStack(this._deck);
    }

    //-------------------------------------------------------------------------
    _updatePlayerCardPositions(data: CardMoveData, isSoloTween: boolean, moveSpeed: number) {
        const playerData = this._gameState.getPlayerDataAt(data.playerIndex);
        const targetHandPositions = this._calcPlayerCardPositions(playerData);

        for (let targetCardIndex = 0; targetCardIndex < targetHandPositions.length;
        targetCardIndex++) {
            const cardData = playerData.getCardDataAt(targetCardIndex);
            const cardImage = this._cardImagesByCardData.get(cardData);
            const targetHandPosition = targetHandPositions[targetCardIndex];
            const props = {
                x: targetHandPosition.x,
                y: targetHandPosition.y
            };
            
            const tween = this._gameState.game.tweens.create(cardImage)
                .to(props, moveSpeed)
                .start();

            if (isSoloTween) {
                if (targetCardIndex === 0) {
                    this._currentTween = tween;
                    this._currentTween.onComplete.addOnce(
                        this._onTweenMoveCardComplete, this, 0, data);
                }
            }
        }    
    }
}
