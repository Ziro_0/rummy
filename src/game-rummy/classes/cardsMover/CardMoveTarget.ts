export enum CardMoveTarget {
    HAND,
    MELD,
    DISCARD,
    DECK,
    MELD_IMAGE
};
