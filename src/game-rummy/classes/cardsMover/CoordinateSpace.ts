//-----------------------------------------------------------------------------

export function GlobalToLocal(game: Phaser.Game,
source: PIXI.DisplayObjectContainer, globalPosition: Phaser.Point): Phaser.Point {
    const parent = source;
    const parentLocalPosition = new Phaser.Point(parent.x, parent.y);
    const parentGlocalPosition = _getGlobalPosition(parent, parentLocalPosition,
        game.world);
    const out = globalPosition.clone();
    out.subtract(parentGlocalPosition.x, parentGlocalPosition.y);
    return(out);
}

//-----------------------------------------------------------------------------
export function LocalToGlobal(game: Phaser.Game,
source: PIXI.DisplayObjectContainer, localPosition: Phaser.Point): Phaser.Point {
    const out = _getGlobalPosition(source, localPosition, game.world);
    return(out);
}

//-----------------------------------------------------------------------------
function _getGlobalPosition(source: PIXI.DisplayObjectContainer,
localPosition: Phaser.Point, world: PIXI.DisplayObjectContainer): Phaser.Point {
    const out = localPosition.clone();

    let parent = source.parent;
    while (parent && parent !== world) {
        out.x += parent.x;
        out.y += parent.y;
        parent = parent.parent;
    }

    return(out);
}
