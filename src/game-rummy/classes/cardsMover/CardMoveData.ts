import { CardMoveTarget } from "./CardMoveTarget";
import MeldImage from "../MeldImage";

export default class CardMoveData {
    targetCardSlotIndexes: number[];

    source: CardMoveTarget;
    target: CardMoveTarget;
    
    meldImage: MeldImage;

    playerIndex: number;
    cardSlotIndex: number;

    isFaceDown: boolean;
    playSound: boolean;
    useRigger: boolean;

    //=========================================================================
    // constructor
    //=========================================================================
    /** @constructor */
    constructor(source: CardMoveTarget, target: CardMoveTarget, playerIndex: number = -1,
    cardSlotIndex: number = -1, isFaceDown: boolean = false, meldImage: MeldImage = null,
    playSound: boolean = true, targetCardSlotIndexes: number[] = null,
    useRigger: boolean = false) {
        this.source = source;
        this.target = target;
        this.playerIndex = playerIndex;
        this.cardSlotIndex = cardSlotIndex;
        this.isFaceDown = isFaceDown;
        this.meldImage = meldImage;
        this.playSound = playSound;
        this.targetCardSlotIndexes = targetCardSlotIndexes;
        this.useRigger = useRigger;
    }
}
