export enum JsonKey {
    AUDIO = 'audio',
    METRICS = 'metrics',
    TUTORIAL_STEPS = 'tutorialSteps'
};
