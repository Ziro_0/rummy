import DeckImage from "./DeckImage";
import Deck from "./Deck";
import PlayerData from "./PlayerData";
import CardImage from "./CardImage";
import CardData from "./CardData";
import IGameState from "../interfaces/IGameState";
import CardsMover from "./cardsMover/CardsMover";
import { CardMoveTarget } from "./cardsMover/CardMoveTarget";
import MeldImage from "./MeldImage";
import CardMatchUtils from "./cardMatchUtils/CardMatchUtils";
import DeckRigger from "./DeckRigger";
import { CardValueMap, CardValue } from './CardValue';
import { CardSuitMap, CardSuit } from './CardSuit';

export default class CardsManager {
    onCardsMoveComplete: Phaser.Signal;

    _gameState: IGameState;

    _cardImagesByCardData: Map<CardData, CardImage>;

    /** first index = player index */
    _playersCardImages: CardImage[][];
    
    _discardedCardsData: CardData[];

    _deck: Deck;
    _deckImage: DeckImage;
    _deckRigger: DeckRigger;

    _cardsMover: CardsMover;

    _cardsInHandOffset: number;
    
    //=========================================================================
    // constructor
    //=========================================================================
    /** @constructor */
    constructor(gameState: IGameState) {
        this._gameState = gameState;
        
        this.onCardsMoveComplete = new Phaser.Signal();

        this._cardImagesByCardData = new Map();

        const jsonData = this._gameState.metricsJsonData;
        this._initDeck(jsonData);
        
        this._playersCardImages = [
            [],
            []
        ];

        this._discardedCardsData = [];

        this._cardsInHandOffset = jsonData.cardsInHandOffset;
        
        this._initCardsMover();
    }

    //=========================================================================
    // public
    //=========================================================================

    //-------------------------------------------------------------------------
    addDiscardedCardToHand(playerIndex: number) {
        this._cardsMover.enqueue(
            CardMoveTarget.DISCARD,
            CardMoveTarget.HAND,
            playerIndex);

        this._cardsMover.play();
    }

    //-------------------------------------------------------------------------
    set aiCardToDrawNext(cardData: CardData) {
        this._deckRigger.aiCardToDrawNext = cardData;
    }

    //-------------------------------------------------------------------------
    applyDebugDeckCardIds(cardIds: string[]) {
        if (!cardIds || !cardIds.length) {
            return;
        }

        let targetDeckIndex = 0;
        for (let index = 0; index < cardIds.length; index++) {
            const cardId = cardIds[index];
            const cardIdIndex = this._deck.getIndexOfCardDataById(cardId);
            if (cardIdIndex === -1) {
                continue;
            }

            this._deck.swap(targetDeckIndex, cardIdIndex);
            targetDeckIndex++;
        }
    }

    //-------------------------------------------------------------------------
    dealOpeningCards(useRigger?: boolean) {
        const NUM_OF_PLAYERS = this._gameState.playersData.length;
        const NUM_CARDS_TO_DEAL_PER_PLAYER = this._gameState.game.config.numCardsToDeal;
        
        if (typeof useRigger !== 'boolean') {
            useRigger = this._gameState.isRiggedInPlayersFavor !== null;
        }

        for (let round = 0; round < NUM_CARDS_TO_DEAL_PER_PLAYER; round++) {
            for (let playerIndex = 0; playerIndex < NUM_OF_PLAYERS; playerIndex++) {
                this.drawCard(playerIndex, false, false, useRigger);
            }
        }

        this._cardsMover.play();
    }

    //-------------------------------------------------------------------------
    get deck(): Deck {
        return(this._deck);
    }
    
    //-------------------------------------------------------------------------
    discardCardFromPlayer(playerIndex: number, cardImage: CardImage,
    autoPlay: boolean = false) {
        const playerData = this._gameState.getPlayerDataAt(playerIndex);
        const playerCardDataIndex: number =
            playerData.getCardDataIndexOf(cardImage.cardData);
        const PLAY_SOUND = true;
        
        this._cardsMover.enqueue(
            CardMoveTarget.HAND,
            CardMoveTarget.DISCARD,
            playerIndex,
            playerCardDataIndex,
            null,
            PLAY_SOUND);

        if (autoPlay) {
            this.moveCards();
        }
    }

    //-------------------------------------------------------------------------
    discardCardFromDeck(useRigger?: boolean) {
        if (this.isDeckEmpty()) {
            //sanity check
            console.warn('deck empty');
            return;
        }

        const playerIndex = this._gameState.turnPlayerIndex || 0;
        const PLAY_SOUND = true;

        if (typeof useRigger !== 'boolean') {
            useRigger = this._gameState.isRiggedInPlayersFavor !== null;
        }
        
        this._cardsMover.enqueue(
            CardMoveTarget.DECK,
            CardMoveTarget.DISCARD,
            playerIndex,
            null, null, PLAY_SOUND, null,
            useRigger);

        this._cardsMover.play();
    }

    //-------------------------------------------------------------------------
    get discardCards(): CardData[] {
        return(this._discardedCardsData.concat());
    }

    //-------------------------------------------------------------------------
    dispose() {
        this._playersCardImages = [
            [],
            []
        ];

        this._discardedCardsData = [];
        
        this.onCardsMoveComplete.dispose();
        this._cardsMover.dispose();

        this._gameState = null;
    }

    //-------------------------------------------------------------------------
    drawCard(playerIndex: number, autoPlay: boolean = false, playSound: boolean = true,
    useRigger: boolean) {

        this._cardsMover.enqueue(
            CardMoveTarget.DECK,
            CardMoveTarget.HAND,
            playerIndex,
            -1, null, playSound, null,
            useRigger);

        if (autoPlay) {
            this.moveCards();
        }
    }

    //-------------------------------------------------------------------------
    getCardImage(cardData: CardData): CardImage {
        return(this._cardImagesByCardData.get(cardData));
    }

    //-------------------------------------------------------------------------
    isDeckEmpty(): boolean {
        return(this._deck.length === 0);
    }

    //-------------------------------------------------------------------------
    meldPrepCardAdd(playerIndex: number, cardImage: CardImage) {
        const playerData = this._gameState.getPlayerDataAt(playerIndex);
        const playerCardDataIndex: number =
            playerData.getCardDataIndexOf(cardImage.cardData);

        this._cardsMover.enqueue(
            CardMoveTarget.HAND,
            CardMoveTarget.MELD,
            playerIndex,
            playerCardDataIndex);

        this.moveCards();
    }

    //-------------------------------------------------------------------------
    meldPrepCardRemove(playerIndex: number, meldPrepCardIndex: number) {
        this._cardsMover.enqueue(
            CardMoveTarget.MELD,
            CardMoveTarget.HAND,
            playerIndex,
            meldPrepCardIndex);

        this.moveCards();
    }

    //-------------------------------------------------------------------------
    meldPrepCardRemoveAll(playerIndex: number) {
        this._cardsMover.enqueue(
            CardMoveTarget.MELD,
            CardMoveTarget.HAND,
            playerIndex,
            -1);
    
        this.moveCards();
    }
    
    //-------------------------------------------------------------------------
    moveCards() {
        this._cardsMover.play();
    }

    //-------------------------------------------------------------------------
    moveMeldPrepCardsToMeld(targetMeldImage: MeldImage, playerIndex?: number) {
        this._cardsMover.enqueue(
            CardMoveTarget.MELD,
            CardMoveTarget.MELD_IMAGE,
            typeof playerIndex === 'number' ? playerIndex : this._gameState.turnPlayerIndex,
            -1,
            targetMeldImage);
    
        this.moveCards();
    }

    //-------------------------------------------------------------------------
    putAllCardsBackIntoDeck() {
        //no movement animations on any of these

        //recover discarded cards
        this._discardedCardsData.forEach(function(cardData) {
            this._destroyCardImageByData(cardData);
            this._deck.add(cardData);
        }, this);

        this._discardedCardsData.length = 0;

        //recover put all players' cards
        for (let playerIndex = 0; playerIndex < this._gameState.playersData.length; playerIndex++) {
            const playerData = this._gameState.getPlayerDataAt(playerIndex);
            for (let cardIndex = 0; cardIndex < playerData.numCards; cardIndex++) {
                const cardData = playerData.getCardDataAt(cardIndex);
                this._destroyCardImageByData(cardData);
                this._deck.add(cardData);
            }

            playerData.cardsData.length = 0;

            this._playersCardImages[playerIndex].length = 0;
        }

        //recover cards from melds
        this._gameState.melds.forEach(function(meldImage) {
            const cardsData: CardData[] = meldImage.cardsData;
            cardsData.forEach(function(cardData) {
                this._destroyCardImageByData(cardData);
                this._deck.add(cardData);
            }, this);
        }, this);

        this._gameState.melds.clear();


        this._deckImage.updateStack(this._deck);
    }

    //-------------------------------------------------------------------------
    recycleDiscardedCards() {
        for (let index = 0; index < this._discardedCardsData.length; index++) {
            this._cardsMover.enqueue(
                CardMoveTarget.DISCARD,
                CardMoveTarget.DECK);
        }

        this.moveCards();
    }

    //-------------------------------------------------------------------------
    searchCardImageById(cardId: string): CardImage {
        if (!cardId) {
            //sanity check
            return(null);
        }

        const value: CardValue = CardValueMap[cardId.substring(0, cardId.length - 1)];
        const suit: CardSuit = CardSuitMap[cardId.charAt(cardId.length - 1)];
        let targetCardImage: CardImage = null;

        this._cardImagesByCardData.forEach(
            function(cardImage, cardData) {
                if (cardData.suit === suit && cardData.value === value) {
                    targetCardImage = cardImage;
                    return;
                }
            });

        return(targetCardImage);
    }

    //-------------------------------------------------------------------------
    sortCards(playerIndex: number) {
        const playerData = this._gameState.getPlayerDataAt(playerIndex);
        const origCardsData = playerData.cardsData;
        const sortedCardsData = CardMatchUtils.SortCardsByAscendingValue(origCardsData);
        
        const targetCardIndexes: number[] = [];
        for (let index = 0; index < sortedCardsData.length; index++) {
            const sortedCardData = sortedCardsData[index];
            targetCardIndexes.push(CardData.IndexOf(origCardsData, sortedCardData));
        }

        this._cardsMover.enqueue(
            CardMoveTarget.HAND,
            CardMoveTarget.HAND,
            playerIndex, -1, null, null, targetCardIndexes);

        this.moveCards();
    }

    //-------------------------------------------------------------------------
    shuffleDeck() {
        this._deck.shuffle();
    }

    //-------------------------------------------------------------------------
    get topDiscardCard(): CardData {
        return(this._discardedCardsData[this._discardedCardsData.length - 1]);
    }

    //=========================================================================
    // private
    //=========================================================================

    //-------------------------------------------------------------------------
    _addCardToPlayer(playerData: PlayerData, cardData: CardData, isFaceDown: boolean) {
        playerData.addCardData(cardData);

        const cardImage = new CardImage(this._gameState.game, 0, playerData.yCardsPosition,
            cardData, this._gameState.cardsGroup, this._gameState.onCardImageInputDownSgn);
        cardImage.isFaceDown = isFaceDown;
        this._playersCardImages[playerData.index].push(cardImage);

        this._repositionPlayerCards(playerData);
    }

    //-------------------------------------------------------------------------
    _calcPlayerCardPositions(playerData: PlayerData, out?: Phaser.Point[]): Phaser.Point[] {
        if (out) {
            out.length = 0;
        } else {
            out = [];
        }
        
        const nc = playerData.numCards;
        const ofs = this._cardsInHandOffset;
        const xCenter = this._gameState.game.world.centerX;
        let xStart: number;

        if ((nc & 1) === 1) {
            //starting position for hands with an odd number of cards
            xStart = xCenter - nc / 2 * ofs;
        } else {
            //starting position for hands with an even number of cards
            xStart = xCenter - nc / 2 - ofs * (nc / 2 - 1);
        }

        let x = xStart;
        for (let index = 0; index < nc; index++) {
            out.push(new Phaser.Point(x, playerData.yCardsPosition));
            x += ofs;
        }

        return(out);
    }

    //-------------------------------------------------------------------------
    _destroyCardImage(cardImage: CardImage): CardImage {
        if (!cardImage) {
            return(null);
        }

        this._cardImagesByCardData.delete(cardImage.cardData);
        cardImage.destroy();
        return(null);
    }

    //-------------------------------------------------------------------------
    _destroyCardImageByData(cardData: CardData): CardImage {
        const cardImage = this._cardImagesByCardData.get(cardData);
        return(this._destroyCardImage(cardImage));
    }

    //-------------------------------------------------------------------------
    _initCardsMover() {
        this._cardsMover = new CardsMover(
            this._gameState,
            this._cardImagesByCardData,
            this._deck,
            this._deckImage,
            this._discardedCardsData,
            this._deckRigger);

        this._cardsMover.onComplete.add(this._onCardsMoverComplete, this);
    }

    //-------------------------------------------------------------------------
    _initDeck(jsonData) {
        this._deck = new Deck();
        this._initDeckImage(jsonData);
        this._deckRigger = new DeckRigger(this._gameState);
    }

    //-------------------------------------------------------------------------
    _initDeckImage(jsonData) {
        const jDeck = jsonData.deck;
        this._deckImage = new DeckImage(
            this._gameState,
            jDeck.position.x,
            jDeck.position.y,
            this._gameState.onDeckInputDownSgn,
            this._gameState.deckAndDiscardGroup);

        this._deckImage.updateStack(this._deck);
    }

    //-------------------------------------------------------------------------
    _onCardsMoverComplete(cardsMover: CardsMover, discardedCardImage: CardImage) {
        this.onCardsMoveComplete.dispatch(this);
    }

    //-------------------------------------------------------------------------
    _onTweenCardMoveStart(cardImage: CardImage, tween: Phaser.Tween) {
        const customTweenData: any = tween["customData"];
        if (!customTweenData) {
            return;
        }

        cardImage.visible = true;
        cardImage.isFaceDown = customTweenData.isFaceDown;
    }

    //-------------------------------------------------------------------------
    _repositionPlayerCards(playerData: PlayerData) {
        const nc = playerData.numCards;
        const ofs = this._cardsInHandOffset;
        const xCenter = this._gameState.game.world.centerX;
        let xStart: number;

        if ((nc & 1) === 1) {
            //starting position for hands with an odd number of cards
            xStart = xCenter - nc / 2 * ofs;
        } else {
            //starting position for hands with an even number of cards
            xStart = xCenter - nc / 2 - ofs * (nc / 2 - 1);
        }

        const playersCardImages = this._playersCardImages[playerData.index];
        let x = xStart;
        for (let index = 0; index < nc; index++) {
            playersCardImages[index].x = x;
            x += ofs;
        }
    }

    //-------------------------------------------------------------------------
    /*
    _setDiscardedCard(discardedCardImage: CardImage) {
        if (!discardedCardImage) {
            return;
        }

        this._discardedCardsData.push(discardedCardImage.cardData);

        if (this._discardCardImage) {
            this._discardCardImage.cardData = discardedCardImage.cardData;
            this.destroyCard(discardedCardImage);
        } else {
            this._discardCardImage = discardedCardImage;
        }
    }
    */

    /*
    //-------------------------------------------------------------------------
    _updateDeckImage() {
        const MAX_CARDS = 52;
        const SLOPE = this._deckImage.length / MAX_CARDS;
        this._deckImage.stack = Math.ceil(SLOPE * this._deck.remaining);
    }
    */
}
