import IGameState from '../../interfaces/IGameState';
import TutorialStepHandler from './TutorialStepHandler';
import TutorialMessageHandler from './TutorialMessageHandler';
import TutorialDealOpeningCardsHandler from './TutorialDealOpeningCardsHandler';
import TutorialSetDeckCardsHandler from './TutorialSetDeckCardsHandler';
import TutorialDiscardCardFromDeckHandler from './TutorialDiscardCardFromDeckHandler';
import TutorialWaitHandler from './TutorialWaitHandler';
import TutorialArrowHandler from './TutorialArrowHandler';
import TutorialPressHandler from './TutorialPressHandler';
import TutorialDrawCardHandler from './TutorialDrawCardHandler';
import TutorialHiliteCardHandler from './TutorialHiliteCardHandler';
import TutorialDiscardCardFromPlayerHandler from './TutorialDiscardCardFromPlayerHandler';
import TutorialPickDiscardedCardHandler from './TutorialPickDiscardedCardHandler';
import TutorialPresentMeldPrepdHandler from './TutorialPresentMeldPrepdHandler';
import TutorialCreateMeldHandler from './TutorialCreateMeldHandler';
import TutorialButtonHiliteHandler from './TutorialButtonHiliteHandler';
import { MainViewbuttonId } from '../MainViewbuttonId';
import TutorialButtonVisibleHander from './TutorialButtonVisibleHander';
import TutorialLayOffMeldHandler from './TutorialLayOffMeldHandler';
import TutorialMeldImageHiliteHandler from './TutorialMeldImageHiliteHandler';
import TutorialEndOfGameProcHandler from './TutorialEndOfGameProcHandler';
import TutorialPlaySoundHandler from './TutorialPlaySoundHandler';

export default class TutorialView extends Phaser.Group {
    onClose: Phaser.Signal;

    _jStepsData: any[];
    
    _gameState: IGameState;
    
    _stepHandler: TutorialStepHandler;
    _arrowHandler: TutorialArrowHandler;

    _arrowImage: Phaser.Group;

    _stepIndex: number;

    //=========================================================================
    // constructor
    //=========================================================================
    /** @constructor */
    constructor(gameState: IGameState) {
        super(gameState.game);
        
        this._gameState = gameState;

        this.onClose = new Phaser.Signal();

        this._begin();
    }

    //=========================================================================
    // public
    //=========================================================================

    //-------------------------------------------------------------------------
    get arrowHandler(): TutorialArrowHandler {
        return(this._arrowHandler);
    }

    //-------------------------------------------------------------------------
    set arrowHandler(value: TutorialArrowHandler) {
        const handler = this._arrowHandler;
        this._arrowHandler = value;
        if (handler) {
            handler.dispose();
        }
    }

    //-------------------------------------------------------------------------
    complete() {
        this.onClose.dispatch(this);
    }

    //-------------------------------------------------------------------------
    destroy() {
        this.arrowHandler = null;
        this._disposeStepHandler();
        this._gameState = null;

        TutorialButtonHiliteHandler.Unregister();
        TutorialHiliteCardHandler.UnregisterAll();
        
        super.destroy();
    }

    //-------------------------------------------------------------------------
    handleDeckInputDown() {
        if (!(this._stepHandler instanceof TutorialPressHandler)) {
            return;
        }

        const pressHandler = this._stepHandler as TutorialPressHandler;
        pressHandler.begin();
    }

    //-------------------------------------------------------------------------
    onButtonPressed(id: MainViewbuttonId) {
        if (this._stepHandler instanceof TutorialPressHandler) {
            this._stepHandler.handleButtonPressed(id);
        }
    }

    //=========================================================================
    // private
    //=========================================================================

    //-------------------------------------------------------------------------
    _begin() {
        this._startStep(0);
    }

    //-------------------------------------------------------------------------
    _disposeStepHandler() {
        if (this._stepHandler) {
            this._stepHandler.dispose();
            this._stepHandler = null;
        }
    }

    //-------------------------------------------------------------------------
    _handleButtonVisible(value: any) {
        this._stepHandler = new TutorialButtonVisibleHander(this._gameState,
            value.id, value.visible);
    }

    //-------------------------------------------------------------------------
    _handleCreateMeld(value: any) {
        this._stepHandler = new TutorialCreateMeldHandler(this._gameState,
            value.meldType, value.playerIndex);
    }

    //-------------------------------------------------------------------------
    _handleDealOpeningCards() {
        this._stepHandler = new TutorialDealOpeningCardsHandler(this._gameState, this);
    }

    //-------------------------------------------------------------------------
    _handleDiscardCardFromDeck() {
        this._stepHandler = new TutorialDiscardCardFromDeckHandler(this._gameState, this);
    }

    //-------------------------------------------------------------------------
    _handleDiscardCardFromPlayer(value: any) {
        const playerIndex: number = value.playerIndex;
        const cardId: string = value.cardId;
        this._stepHandler = new TutorialDiscardCardFromPlayerHandler(this._gameState,
            playerIndex, cardId);
    }

    //-------------------------------------------------------------------------
    _handleDrawCard(targetPlayerIndex: number) {
        this._stepHandler = new TutorialDrawCardHandler(this._gameState, this,
            targetPlayerIndex);
    }

    //-------------------------------------------------------------------------
    _handleEndOfGameProc(value) {
        this._stepHandler = new TutorialEndOfGameProcHandler(this._gameState,
            value.losingPlayerIndex);
    }

    //-------------------------------------------------------------------------
    _handleHideArrow() {
        this._stepHandler = new TutorialArrowHandler(this._gameState, this);
    }

    //-------------------------------------------------------------------------
    _handleHiliteButton(value: any) {
        if (!value) {
            console.warn('Required "value" object missing from hilite button step.');
            return;
        }

        let buttonId: MainViewbuttonId;
        let color: number | string;
        let hiliteRate: number;

        if (typeof value === 'string') {
            buttonId = <MainViewbuttonId>value;
        } else {
            buttonId = value.buttonId;
            color = value.color;
            hiliteRate = value.speed;
        }
        
        if (!buttonId) {
            console.warn(
                'Required "buttonId" property missing from "value" object of hilite button step.');
            return;
        }

        TutorialButtonHiliteHandler.Register(this._gameState, buttonId, color, hiliteRate);
    }
    
    //-------------------------------------------------------------------------
    _handleHiliteCards(value: any) {
        if (!value) {
            console.warn('Required "value" object missing from hilite step.');
            return;
        }

        let cardIds: string | string[];
        let color: number | string;
        let handlerId: string;
        let hiliteRate: number;

        if (typeof value === 'string' || Array.isArray(value)) {
            cardIds = value;
        } else {
            handlerId = value.id;
            cardIds = value.cardIds;
            color = value.color;
            hiliteRate = value.speed;
        }
        
        if (!cardIds) {
            console.warn(
                'Required "cardIds" property missing from "value" object of hilite step.');
            return;
        }

        if (!!handlerId) {
            handlerId = TutorialHiliteCardHandler.NextHandlerId;
        }

        TutorialHiliteCardHandler.Register(this._gameState, handlerId, cardIds, color,
            hiliteRate);
    }

    //-------------------------------------------------------------------------
    _handleHiliteMeldImage(value: any) {
        if (!value) {
            console.warn('Required "value" object missing from hilite meld image step.');
            return;
        }

        let meldIndex: number;
        let color: number | string;
        let hiliteRate: number;

        if (typeof value === 'number') {
            meldIndex = value;
        } else {
            meldIndex = value.index;
            color = value.color;
            hiliteRate = value.speed;
        }
        
        if (meldIndex === undefined) {
            console.warn(
                'Required "meldIndex" property missing from "value" object of hilite meld image step.');
            return;
        }

        TutorialMeldImageHiliteHandler.Register(this._gameState, meldIndex, color,
            hiliteRate);
    }

    //-------------------------------------------------------------------------
    _handleLayOffMeld(value: any) {
        this._stepHandler = new TutorialLayOffMeldHandler(this._gameState,
            value.playerIndex, value.meldIndex, value.cardId);
    }
    
    //-------------------------------------------------------------------------
    _handlePress(value: any) {
        this._stepHandler = new TutorialPressHandler(this._gameState, this, value);
    }

    //-------------------------------------------------------------------------
    _handleMessage(data: object) {
        this._stepHandler = new TutorialMessageHandler(this._gameState, this, data);
    }

    //-------------------------------------------------------------------------
    _handlePickDiscardedCard(value: any) {
        this._stepHandler = new TutorialPickDiscardedCardHandler(this._gameState, value);
    }

    //-------------------------------------------------------------------------
    _handlePlaySound(value: any) {
        this._stepHandler = new TutorialPlaySoundHandler(this._gameState, value);
    }

    //-------------------------------------------------------------------------
    _handlePresentMeldPrep(value: any) {
        this._stepHandler = new TutorialPresentMeldPrepdHandler(this._gameState, value);
    }

    //-------------------------------------------------------------------------
    _handleSetDeckCards(cardIds: string[]) {
        this._stepHandler = new TutorialSetDeckCardsHandler(this._gameState, this, cardIds);
    }

    //-------------------------------------------------------------------------
    _handleShowArrow(value: any) {
        this._stepHandler = new TutorialArrowHandler(this._gameState, this, value);
    }

    //-------------------------------------------------------------------------
    _handleUnHiliteAllCards() {
        TutorialHiliteCardHandler.UnregisterAll();
    }

    //-------------------------------------------------------------------------
    _handleUnHliteButton(value: any) {
        TutorialButtonHiliteHandler.Unregister();
    }

    //-------------------------------------------------------------------------
    _handleUnHiliteCards(value: any) {
        if (typeof value === 'string' || Array.isArray(value)) {
            TutorialHiliteCardHandler.UnhiliteCards(value);
            return;
        }

        const handlerId: string = value.id;
        if (!!handlerId) {
            const cardIds = value.cardId || value.cardIds;
            if (typeof cardIds === 'string' || Array.isArray(cardIds)) {
                TutorialHiliteCardHandler.UnhiliteCards(cardIds);
            } else {
                TutorialHiliteCardHandler.Unregister(handlerId);
            }
        } else {
            console.warn(
                'Required "id" property missing from "value" object of unhilite step.');
        }
    }

    //-------------------------------------------------------------------------
    _handleUnHiliteMeldImage(value: any) {
        TutorialMeldImageHiliteHandler.Unregister();
    }

    //-------------------------------------------------------------------------
    _handleWait(duration: number) {
        this._stepHandler = new TutorialWaitHandler(this._gameState, this, duration);
    }

    //-------------------------------------------------------------------------
    _nextStep() {
        const nextStepIndex = this._stepIndex + 1;
        if (nextStepIndex >= this._jStepsData.length) {
            this.complete();
        } else {
            this._startStep(nextStepIndex);
        }
    }

    //-------------------------------------------------------------------------
    _onStepHandlerComplete(stepHandler: TutorialStepHandler,
    shouldNotDisposeStepHandler: boolean) {
        if (shouldNotDisposeStepHandler) {
            this._stepHandler = null;
        } else {
            this._disposeStepHandler();
        }

        this._nextStep();
    }

    //-------------------------------------------------------------------------
    _startStep(stepIndex: number) {
        this._stepIndex = stepIndex;

        this._jStepsData = this._gameState.tutorialStepsJsonData;
        const jStepData = this._jStepsData[this._stepIndex];
        if (!jStepData) {
            //sanity check
            console.warn(`No JSON step data found for index ${stepIndex} in JSON data file.`);
            this.complete();
            return;
        }

        this._disposeStepHandler();
        
        const key: string = jStepData.key;

        switch (key) {
            case 'button-visible':
                this._handleButtonVisible(jStepData.value);
                break;

            case 'create-meld':
                this._handleCreateMeld(jStepData.value);
                break;

            case 'deal-opening-cards':
                this._handleDealOpeningCards();
                break;

            case 'discard-card-from-deck':
                this._handleDiscardCardFromDeck();
                break;

            case 'discard-card-from-player':
                this._handleDiscardCardFromPlayer(jStepData.value);
                break;

            case 'draw-card':
                this._handleDrawCard(jStepData.value);
                break;

            case 'end-of-game-proc':
                this._handleEndOfGameProc(jStepData.value);
                break;

            case 'hide-arrow':
                this._handleHideArrow();
                break;

            case 'hilite-button':
                this._handleHiliteButton(jStepData.value);
                break;

            case 'hilite-card':
            case 'hilite-cards':
                this._handleHiliteCards(jStepData.value);
                break;

            case 'hilite-meld-image':
                this._handleHiliteMeldImage(jStepData.value);
                break;

            case 'lay-off-meld':
                this._handleLayOffMeld(jStepData.value);
                break;

            case 'message':
            case 'messages':
                this._handleMessage(jStepData.value);
                break;

            case 'pick-discarded-card':
                this._handlePickDiscardedCard(jStepData.value);
                break;

            case 'play-sound':
                this._handlePlaySound(jStepData.value);
                break;

            case 'present-meld-prep':
                this._handlePresentMeldPrep(jStepData.value);
                break;

            case 'press':
                this._handlePress(jStepData.value);
                break;

            case 'set-deck-cards':
                this._handleSetDeckCards(jStepData.value);
                break;

            case 'show-arrow':
                this._handleShowArrow(jStepData.value);
                break;

            case 'unhilite-all-cards':
                this._handleUnHiliteAllCards();
                break;

            case 'unhilite-button':
                this._handleUnHliteButton(jStepData.value);
                break;

            case 'unhilite-card':
            case 'unhilite-cards':
                this._handleUnHiliteCards(jStepData.value);
                break;

            case 'unhilite-meld-image':
                this._handleUnHiliteMeldImage(jStepData.value);
                break;

            case 'wait':
                this._handleWait(jStepData.value);
                break;

            default:
                console.warn(`Unknown step key: "${key}".`);
                break;
        }

        if (this._stepHandler) {
            this._stepHandler.onComplete.addOnce(this._onStepHandlerComplete, this);
            this._stepHandler.begin();
        } else {
            this._nextStep();
        }
    }
}
