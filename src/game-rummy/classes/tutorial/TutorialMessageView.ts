import Button from '../Button';
import IGameState from '../../interfaces/IGameState';

export default class TutorialMessageView extends Phaser.Group {
    static readonly ACTION_NEXT: string = 'next';
    static readonly ACTION_SKIP: string = 'skip';
    
    static readonly _INPUT_PROORITY_ID_BACK: number = 100;
    static readonly _INPUT_PROORITY_ID_NEXT_BUTTON: number = 110;
    static readonly _INPUT_PROORITY_ID_SKIP_BUTTON: number = 120;

    onClose: Phaser.Signal;

    _gameState: IGameState;
    _nextButton: Button;
    _skipButton: Button;

    _bitmapText: Phaser.BitmapText;

    //=========================================================================
    // constructor
    //=========================================================================
    /** @constructor */
    constructor(gameState: IGameState, container?: Phaser.Group) {
        super(gameState.game);
        
        this._gameState = gameState;
        this.onClose = new Phaser.Signal();

        this._createDimBack();
        this._createBackImage();
        this._createText();
        this._initButtons();

        if (container) {
            container.add(this);
        }
    }

    //=========================================================================
    // public
    //=========================================================================

    //-------------------------------------------------------------------------
    destroy() {
        this._bitmapText.destroy();
        this._bitmapText = null;

        this._nextButton.destroy();
        this._skipButton.destroy();
        this._gameState = null;
        super.destroy();
    }

    //-------------------------------------------------------------------------
    set text(value: string) {
        if (this._bitmapText) {
            this._bitmapText.text = value || '';
        }
    }

    //-------------------------------------------------------------------------
    update() {
        if (!this._gameState.game.config.debugEnterKeyToSkipMessages) {
            return;
        }

        if (this._gameState.game.input.keyboard.isDown(Phaser.KeyCode.ENTER)) {
            this._next();
        }
    }

    //=========================================================================
    // private
    //=========================================================================

    //-------------------------------------------------------------------------
    _createBackImage() {
        const image = this.game.make.image(0, 0, 'ui', 'message-text-panel');
        image.inputEnabled = true;
        image.input.priorityID = TutorialMessageView._INPUT_PROORITY_ID_BACK;
        this.addChild(image);
    }

    //-------------------------------------------------------------------------
    _createDimBack() {
        const game = this._gameState.game;
        const graphics: Phaser.Graphics = game.add.graphics(0, 0, this);
        graphics.beginFill(0x000000, 0.5);
        graphics.drawRect(0, 0, game.world.width, game.world.height);
        graphics.endFill();
    }

    //-------------------------------------------------------------------------
    _createText() {
        const jsonData = this._gameState.metricsJsonData.tutorialView;
        const languageKey = this._gameState.game.config.tutorialLanguage;
        const jMessageText = jsonData.message_text[languageKey];
        
        if (!jMessageText) {
            console.error(`No language data found for key "${languageKey}"`);
            return;
        }

        this._bitmapText = this._gameState.game.add.bitmapText(
            jMessageText.x,
            jMessageText.y,
            jMessageText.font,
            '',
            jMessageText.size,
            this);
            
        this._bitmapText.maxWidth = jMessageText.width;
    }

    //-------------------------------------------------------------------------
    _initButton(position: any, namePrefix: string, onPressedCallback,
    inputPriorityId?: number): Button {
        const button = new Button(
            this._gameState,
            position.x, position.y,
            'ui',
            onPressedCallback,
            this,
            namePrefix);

        if (typeof inputPriorityId === 'number') {
            button.input.priorityID = inputPriorityId;
        }

        this.add(button);
        return(button);
    }

    //-------------------------------------------------------------------------
    _initButtons() {
        const jsonData = this._gameState.metricsJsonData.tutorialView;
        const jMessagePanel = jsonData.message_panel;

        this._nextButton = this._initButton(
            jMessagePanel.next_button_position,
            'game_button_next',
            this._onNextButtonPressed,
            TutorialMessageView._INPUT_PROORITY_ID_NEXT_BUTTON);

        this._skipButton = this._initButton(
            jMessagePanel.skip_button_position,
            'game_button_skip',
            this._onSkipButtonPressed,
            TutorialMessageView._INPUT_PROORITY_ID_SKIP_BUTTON);
    }

    //-------------------------------------------------------------------------
    _next() {
        this.onClose.dispatch(this, TutorialMessageView.ACTION_NEXT);
    }

    //-------------------------------------------------------------------------
    _onNextButtonPressed() {
        this._next();
    }

    //-------------------------------------------------------------------------
    _onSkipButtonPressed() {
        this.onClose.dispatch(this, TutorialMessageView.ACTION_SKIP);
    }
}
