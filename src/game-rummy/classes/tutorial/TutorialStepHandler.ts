import IGameState from '../../interfaces/IGameState';
import TutorialView from './TutorialView';

export default abstract class TutorialStepHandler {
    tutorialView: TutorialView;
    onComplete: Phaser.Signal;

    /** @protected */
    _gameState: IGameState;
    
    //=========================================================================
    // constructor
    //=========================================================================
    /** @constructor */
    constructor(gameState: IGameState, tutorialView: TutorialView) {
        this.tutorialView = tutorialView;
        this._gameState = gameState;
        this.onComplete = new Phaser.Signal();
    }

    //=========================================================================
    // public
    //=========================================================================

    //-------------------------------------------------------------------------
    /**
     * Designed to be overridden by sub-classes.
     */
    abstract begin(): void;

    //-------------------------------------------------------------------------
    dispose(): void {
        this.onComplete.dispose();
        this.tutorialView = null;
        this._gameState = null;
    }

    //=========================================================================
    // protected
    //=========================================================================
    
    //-------------------------------------------------------------------------
    /**
     * @protected
     */
    _complete(shouldNotDisposeStepHandler: boolean = false): void {
        this.onComplete.dispatch(this, shouldNotDisposeStepHandler);
    }
}
