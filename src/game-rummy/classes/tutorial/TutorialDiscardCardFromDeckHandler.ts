import TutorialStepHandler from './TutorialStepHandler';
import IGameState from '../../interfaces/IGameState';
import TutorialView from './TutorialView';

export default class TutorialDiscardCardFromDeckHandler extends TutorialStepHandler {
    //=========================================================================
    // constructor
    //=========================================================================
    /** @constructor */
    constructor(gameState: IGameState, tutorialView: TutorialView) {
        super(gameState, tutorialView);
    }

    //=========================================================================
    // public
    //=========================================================================

    //-------------------------------------------------------------------------
    begin(): void {
        const cardsManager = this._gameState.cardsManager;
        cardsManager.onCardsMoveComplete.addOnce(
            this._onCardsManagerDiscardCardComplete, this);
        
        const USE_RIGGER = false;
        cardsManager.discardCardFromDeck(USE_RIGGER);
    }

    //=========================================================================
    // private
    //=========================================================================

    //-------------------------------------------------------------------------
    _onCardsManagerDiscardCardComplete(): void {
        this._complete();
    }
}
