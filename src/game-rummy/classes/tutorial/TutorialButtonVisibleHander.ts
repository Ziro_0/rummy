import TutorialStepHandler from './TutorialStepHandler';
import IGameState from '../../interfaces/IGameState';
import { MainViewbuttonId } from '../MainViewbuttonId';

export default class TutorialButtonVisibleHander extends TutorialStepHandler {
    _buttonId: MainViewbuttonId;
    _visible: boolean;

    //=========================================================================
    // constructor
    //=========================================================================
    /** @constructor */
    constructor(gameState: IGameState, buttonId: MainViewbuttonId, visible: boolean) {
        super(gameState, null);
        this._buttonId = buttonId;
        this._visible = visible;
    }

    //=========================================================================
    // public
    //=========================================================================

    //-------------------------------------------------------------------------
    begin(): void {
        const button = this._gameState.getMainViewButton(this._buttonId);
        if (button) {
            button.visible = this._visible;
        }
        
        this._complete();
    }

    //=========================================================================
    // private
    //=========================================================================
}
