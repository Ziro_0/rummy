import TutorialStepHandler from './TutorialStepHandler';
import IGameState from '../../interfaces/IGameState';
import TutorialView from './TutorialView';
import TutorialMessageView from './TutorialMessageView';

export default class TutorialMessageHandler extends TutorialStepHandler {
    _messages: string[];

    _messageView: TutorialMessageView;
        
    _messagesIndex: number;

    //=========================================================================
    // constructor
    //=========================================================================
    /** @constructor */
    constructor(gameState: IGameState, tutorialView: TutorialView,
    data: object) {
        super(gameState, tutorialView);
        this._messages = this._resolveLanguageMessage(data);
        this._createMessageView();
    }

    //=========================================================================
    // public
    //=========================================================================

    //-------------------------------------------------------------------------
    begin(): void {
        this._showMessage(0);
    }

    //-------------------------------------------------------------------------
    dispose(): void {
        this._messageView.destroy();
        super.dispose();
    }

    //=========================================================================
    // private
    //=========================================================================

    //-------------------------------------------------------------------------
    _createMessageView(): void {
        this._messageView = new TutorialMessageView(this._gameState, this.tutorialView);
        this._messageView.onClose.add(this._onMessageViewClosed, this);
        this._messageView.visible = false;

        const jsonData = this._gameState.metricsJsonData.tutorialView;
        const jMessagePanel = jsonData.message_panel;
        const jPosition = jMessagePanel.position;

        this._messageView.x = jPosition.x;
        this._messageView.y = jPosition.y;
    }

    //-------------------------------------------------------------------------
    _onMessageViewClosed(view: TutorialMessageView, action: string): void {
        switch (action) {
            case TutorialMessageView.ACTION_NEXT:
                this._showNextMessage();
                break;

            case TutorialMessageView.ACTION_SKIP:
            default:
                this.tutorialView.complete();
                break;
        }
    }

    //-------------------------------------------------------------------------
    _presentMessageView(message: string): void {
        this._messageView.visible = true;
        this._messageView.text = message;
    }

    //-------------------------------------------------------------------------
    _resolveLanguageMessage(data: object): string[] {
        const languageKey = this._gameState.game.config.tutorialLanguage;
        const languageData = data[languageKey];
        const messages: string[] = typeof languageData === 'string' ? [ languageData ] : languageData;
        return(messages);
    }

    //-------------------------------------------------------------------------
    _showMessage(messageIndex: number): void {
        this._messagesIndex = messageIndex;
        const message = this._messages[this._messagesIndex];
        this._presentMessageView(message);
    }

    //-------------------------------------------------------------------------
    _showNextMessage(): void {
        const nextMessageIndex = this._messagesIndex + 1;
        
        if (nextMessageIndex >= this._messages.length) {
            this._complete();
        } else {
            this._showMessage(nextMessageIndex);
        }
    }
}
