import TutorialStepHandler from './TutorialStepHandler';
import IGameState from '../../interfaces/IGameState';

export default class TutorialEndOfGameProcHandler extends TutorialStepHandler {
    _losingPlayerIndex: number;

    //=========================================================================
    // constructor
    //=========================================================================
    /** @constructor */
    constructor(gameState: IGameState, losingPlayerIndex: number) {
        super(gameState, null);
        this._losingPlayerIndex = losingPlayerIndex;
    }

    //=========================================================================
    // public
    //=========================================================================

    //-------------------------------------------------------------------------
    begin(): void {
        this._gameState.endOfRoundProc(this._losingPlayerIndex);
        this._complete();
    }

    //=========================================================================
    // private
    //=========================================================================
}
