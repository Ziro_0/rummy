import TutorialStepHandler from './TutorialStepHandler';
import IGameState from '../../interfaces/IGameState';
import TutorialView from './TutorialView';

export default class TutorialDealOpeningCardsHandler extends TutorialStepHandler {
    //=========================================================================
    // constructor
    //=========================================================================
    /** @constructor */
    constructor(gameState: IGameState, tutorialView: TutorialView) {
        super(gameState, tutorialView);
    }

    //=========================================================================
    // public
    //=========================================================================

    //-------------------------------------------------------------------------
    begin(): void {
        const cardsManager = this._gameState.cardsManager;
        cardsManager.onCardsMoveComplete.addOnce(
            this._onCardsManagerOpeningDealComplete, this);
        
        const USE_RIGGER = false;
        cardsManager.dealOpeningCards(USE_RIGGER);
    }

    //=========================================================================
    // private
    //=========================================================================

    //-------------------------------------------------------------------------
    _onCardsManagerOpeningDealComplete(): void {
        this._complete();
    }
}
