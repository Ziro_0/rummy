import TutorialStepHandler from './TutorialStepHandler';
import IGameState from '../../interfaces/IGameState';
import Button from '../Button';
import { MainViewbuttonId } from '../MainViewbuttonId';

export default class TutorialButtonHiliteHandler extends TutorialStepHandler {
    static readonly DEFAULT_COLOR = 0x00ff00;
    static readonly DEFAULT_HILITE_RATE = 250;

    static readonly _TINT_OFF_COLOR = 0xffffff;

    static _registeredHandler: TutorialButtonHiliteHandler;

    _button: Button;
    _buttonId: MainViewbuttonId;

    _tintTimer: Phaser.Timer;

    _color: number;
    _speed: number;
    _isTinted: boolean;

    //=========================================================================
    // constructor
    //=========================================================================
    /** @constructor */
    constructor(gameState: IGameState, buttonId: MainViewbuttonId,
    color: number | string, speed: number) {
        super(gameState, null);
        this._buttonId = buttonId;
        this._color = this._resolveColor(color);
        this._speed = speed;
        this._isTinted = false;
    }

    //=========================================================================
    // public
    //=========================================================================

    //-------------------------------------------------------------------------
    begin(): void {
        this._button = this._getButton();
        
        let SHOULD_NOT_DISPOSE: boolean = true;
        if (this._button) {
            this._button.visible = true;
            this._startHiliteTimer();
        } else {
            SHOULD_NOT_DISPOSE = false;
        }

        this._complete(SHOULD_NOT_DISPOSE);
    }

    //-------------------------------------------------------------------------
    dispose() {
        if (this._tintTimer) {
            this._tintTimer.destroy();
        }

        this._applyColorToButton(TutorialButtonHiliteHandler._TINT_OFF_COLOR);
        super.dispose();
    }

    //-------------------------------------------------------------------------
    static Register(gameState: IGameState, buttonId: MainViewbuttonId,
    color: number | string = TutorialButtonHiliteHandler.DEFAULT_COLOR,
    speed: number = TutorialButtonHiliteHandler.DEFAULT_HILITE_RATE) {
        if (TutorialButtonHiliteHandler._registeredHandler) {
            return; //sanity check; can only hilite one button at a time
        }

        const handler = new TutorialButtonHiliteHandler(gameState, buttonId,
            color, speed);
        TutorialButtonHiliteHandler._registeredHandler = handler;
        handler.begin();
    }
    
    //-------------------------------------------------------------------------
    static Unregister() {
        const handler: TutorialButtonHiliteHandler =
            TutorialButtonHiliteHandler._registeredHandler;
        
        if (!handler) {
            //sanity check; no handler registered with this card id to remove
            return;
        }

        TutorialButtonHiliteHandler._registeredHandler = null;
        handler.dispose();
    }

    //=========================================================================
    // private
    //=========================================================================

    //-------------------------------------------------------------------------
    _applyColorToButton(color: number) {
        this._button.tint = color;
    }

    //-------------------------------------------------------------------------
    _getButton(): Button {
        return(this._gameState.getMainViewButton(this._buttonId));
    }
    
    //-------------------------------------------------------------------------
    _onTimerTint() {
        this._isTinted = !this._isTinted;

        this._applyColorToButton(this._isTinted ?
            this._color :
            TutorialButtonHiliteHandler._TINT_OFF_COLOR);
    }

    //-------------------------------------------------------------------------
    _resolveColor(color: number | string): number {
        if (typeof color === 'string') {
            const pColor = Phaser.Color.hexToColor(<string>color);
            return(pColor.color);
        }

        return(<number>color);
    }

    //-------------------------------------------------------------------------
    _startHiliteTimer() {
        this._tintTimer = this._gameState.game.time.create(false);
        this._tintTimer.loop(this._speed, this._onTimerTint, this);
        this._tintTimer.start();
    }
}
