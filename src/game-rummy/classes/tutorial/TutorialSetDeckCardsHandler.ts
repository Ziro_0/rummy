import TutorialStepHandler from './TutorialStepHandler';
import IGameState from '../../interfaces/IGameState';
import TutorialView from './TutorialView';

export default class TutorialSetDeckCardsHandler extends TutorialStepHandler {
    cardIds: string[];

    //=========================================================================
    // constructor
    //=========================================================================
    /** @constructor */
    constructor(gameState: IGameState, tutorialView: TutorialView, cardIds: string[]) {
        super(gameState, tutorialView);
        this.cardIds = cardIds;
    }

    //=========================================================================
    // public
    //=========================================================================

    //-------------------------------------------------------------------------
    begin(): void {
        const cardsManager = this._gameState.cardsManager;
        cardsManager.applyDebugDeckCardIds(this.cardIds);
        this._complete();
    }
}
