import TutorialStepHandler from './TutorialStepHandler';
import IGameState from '../../interfaces/IGameState';

export default class TutorialPresentMeldPrepdHandler extends TutorialStepHandler {
    _cardIds: string[];
    _targetPlayerIndex: number;

    //=========================================================================
    // constructor
    //=========================================================================
    /** @constructor */
    constructor(gameState: IGameState, value: any) {
        super(gameState, null);
        this._targetPlayerIndex = value.playerIndex;
        this._cardIds = value.cardIds;
        this._cardIds = this._cardIds.concat();

        const cardsManager = this._gameState.cardsManager;
        cardsManager.onCardsMoveComplete.add(this._onCardsManagerMoveComplete, this);
    }

    //=========================================================================
    // public
    //=========================================================================

    //-------------------------------------------------------------------------
    begin(): void {
        this._prepNextCard();
    }

    //-------------------------------------------------------------------------
    dispose() {
        const cardsManager = this._gameState.cardsManager;
        cardsManager.onCardsMoveComplete.remove(this._onCardsManagerMoveComplete, this);
        super.dispose();
    }

    //=========================================================================
    // private
    //=========================================================================

    //-------------------------------------------------------------------------
    _onCardsManagerMoveComplete() {
        this._prepNextCard();
    }

    //-------------------------------------------------------------------------
    _prepNextCard() {
        const cardId = this._cardIds.shift();
        if (!cardId) {
            this._complete();
            return;
        }

        const cardsManager = this._gameState.cardsManager;
        const cardImage = cardsManager.searchCardImageById(cardId);
        if (!cardImage) {
            this._prepNextCard();
            return;
        }

        cardsManager.meldPrepCardAdd(this._targetPlayerIndex, cardImage);
    }
}
