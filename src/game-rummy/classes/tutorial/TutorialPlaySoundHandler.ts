import TutorialStepHandler from './TutorialStepHandler';
import IGameState from '../../interfaces/IGameState';
import { AudioEventKey } from '../audio/AudioEventKey';

export default class TutorialPlaySoundHandler extends TutorialStepHandler {
    _audioEventKey: AudioEventKey;

    //=========================================================================
    // constructor
    //=========================================================================
    /** @constructor */
    constructor(gameState: IGameState, audioEventKey: AudioEventKey) {
        super(gameState, null);
        this._audioEventKey = audioEventKey;
    }

    //=========================================================================
    // public
    //=========================================================================

    //-------------------------------------------------------------------------
    begin(): void {
        this._gameState.playSound(this._audioEventKey);
        this._complete();
    }

    //=========================================================================
    // private
    //=========================================================================
}
