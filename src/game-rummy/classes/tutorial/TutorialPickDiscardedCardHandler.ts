import TutorialStepHandler from './TutorialStepHandler';
import IGameState from '../../interfaces/IGameState';

export default class TutorialPickDiscardedCardHandler extends TutorialStepHandler {
    _targetPlayerIndex: number;
    _cardId: string;

    //=========================================================================
    // constructor
    //=========================================================================
    /** @constructor */
    constructor(gameState: IGameState, value: any) {
        super(gameState, null);
        this._targetPlayerIndex = value.playerIndex;
        this._cardId = value.cardId;
    }

    //=========================================================================
    // public
    //=========================================================================

    //-------------------------------------------------------------------------
    begin(): void {
        const cardsManager = this._gameState.cardsManager;
        cardsManager.onCardsMoveComplete.addOnce(
            this._onCardsManagerAddDiscardedCardToHandComplete, this);
        cardsManager.addDiscardedCardToHand(this._targetPlayerIndex);
    }

    //=========================================================================
    // private
    //=========================================================================

    //-------------------------------------------------------------------------
    _onCardsManagerAddDiscardedCardToHandComplete() {
        this._complete();
    }
}
