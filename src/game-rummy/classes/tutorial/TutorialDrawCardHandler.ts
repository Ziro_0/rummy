import TutorialStepHandler from './TutorialStepHandler';
import IGameState from '../../interfaces/IGameState';
import TutorialView from './TutorialView';

export default class TutorialDrawCardHandler extends TutorialStepHandler {
    _targetPlayerIndex: number;

    //=========================================================================
    // constructor
    //=========================================================================
    /** @constructor */
    constructor(gameState: IGameState, tutorialView: TutorialView,
    targetPlayerIndex: number) {
        super(gameState, tutorialView);
        this._targetPlayerIndex = targetPlayerIndex;
    }

    //=========================================================================
    // public
    //=========================================================================

    //-------------------------------------------------------------------------
    begin(): void {
        this._drawCard();
    }

    //=========================================================================
    // private
    //=========================================================================

    //-------------------------------------------------------------------------
    _drawCard() {
        const AUTO_PLAY = true;
        const PLAY_SOUND = true;
        const USE_RIGGER = false;
        const cardsManager = this._gameState.cardsManager;
        cardsManager.onCardsMoveComplete.addOnce(
            this._onCardsManagerDrawCardCompleteComplete, this);
        cardsManager.drawCard(this._targetPlayerIndex, AUTO_PLAY, PLAY_SOUND, USE_RIGGER);
     }

    //-------------------------------------------------------------------------
    _onCardsManagerDrawCardCompleteComplete() {
        this._complete();
    }
}
