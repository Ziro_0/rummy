import TutorialStepHandler from './TutorialStepHandler';
import IGameState from '../../interfaces/IGameState';
import CardData from '../CardData';
import MeldImage from '../MeldImage';
import { AudioEventKey } from '../audio/AudioEventKey';
import CardsManager from '../CardsManager';

export default class TutorialLayOffMeldHandler extends TutorialStepHandler {
    _playerIndex: number;
    _meldIndex: number;
    _cardId: string;
 
    //=========================================================================
    // constructor
    //=========================================================================
    /** @constructor */
    constructor(gameState: IGameState, playerIndex: number, meldIndex: number,
    cardId: string) {
        super(gameState, null);
        this._playerIndex = playerIndex;
        this._meldIndex = meldIndex;
        this._cardId = cardId;
    }

    //=========================================================================
    // public
    //=========================================================================

    //-------------------------------------------------------------------------
    begin(): void {
        const meldImage = this._gameState.getMeldImage(this._meldIndex);
        if (!meldImage) {
            this._complete();
            return;
        }

        const cardsData: CardData[] = this._getMeldPrepcardsData();

        const cardsManager = this._gameState.cardsManager;
        cardsManager.onCardsMoveComplete.addOnce(
            this._onCardsManagerMoveCardsComplete, this, 0, cardsData, meldImage);
        cardsManager.moveMeldPrepCardsToMeld(meldImage, this._playerIndex);

        this._gameState.playSound(AudioEventKey.LAY_OFF_MELD);
    }

    //=========================================================================
    // private
    //=========================================================================

    //-------------------------------------------------------------------------
    _getMeldPrepcardsData(): CardData[] {
        const cardsData: CardData[] = [];
        this._gameState.meldPrepCardImages.forEach((cardImage) => {
            cardsData.push(cardImage.cardData);
        });

        return(cardsData);
    }

    //-------------------------------------------------------------------------
    _onCardsManagerMoveCardsComplete(cardsManager: CardsManager,
    cardsData: CardData[], existingMeldImage: MeldImage) {
        if (cardsData && existingMeldImage) {
            this._gameState.addMeld(cardsData, null, existingMeldImage);
        }

        this._complete();
    }
}
