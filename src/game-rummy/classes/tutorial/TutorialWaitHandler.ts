import TutorialStepHandler from './TutorialStepHandler';
import IGameState from '../../interfaces/IGameState';
import TutorialView from './TutorialView';

export default class TutorialWaitHandler extends TutorialStepHandler {
    _timer: Phaser.Timer;
    _duration: number;

    //=========================================================================
    // constructor
    //=========================================================================
    /** @constructor */
    constructor(gameState: IGameState, tutorialView: TutorialView, duration: number) {
        super(gameState, tutorialView);
        this._duration = this._gameState.game.config.debugTutorialNoWaits ? 0 : duration;
    }

    //=========================================================================
    // public
    //=========================================================================

    //-------------------------------------------------------------------------
    begin(): void {
        const AUTO_DESTROY = true;
        this._timer = this._gameState.game.time.create(AUTO_DESTROY);
        this._timer.add(this._duration, this._onTimerComplete, this);
        this._timer.start();
    }

    //-------------------------------------------------------------------------
    dispose(): void {
        this._timer.destroy();
        super.dispose();
    }
    
    //=========================================================================
    // private
    //=========================================================================

    //-------------------------------------------------------------------------
    _onTimerComplete(): void {
        this._complete();
    }
}
