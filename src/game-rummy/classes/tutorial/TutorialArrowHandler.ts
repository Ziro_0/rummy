import TutorialStepHandler from './TutorialStepHandler';
import IGameState from '../../interfaces/IGameState';
import TutorialView from './TutorialView';

export default class TutorialArrowHandler extends TutorialStepHandler {
    _imageContainer: Phaser.Group;
    _image: Phaser.Image;
    _moveTween: Phaser.Tween;

    _value: any;

    //=========================================================================
    // constructor
    //=========================================================================
    /** @constructor */
    constructor(gameState: IGameState, tutorialView: TutorialView, value?: any) {
        super(gameState, tutorialView);
        this._value = value;
    }

    //=========================================================================
    // public
    //=========================================================================

    //-------------------------------------------------------------------------
    begin(): void {
        if (this._value) {
            this._initImage();
            this._initTween();
        }

        if (this._imageContainer) {
            this._moveTween.start();
            this.tutorialView.arrowHandler = this;
            
            const SHOULD_NOT_DISPOSE_STEP_HANDLER = true;
            this._complete(SHOULD_NOT_DISPOSE_STEP_HANDLER);
        } else {
            this.tutorialView.arrowHandler = null;
            this._complete();
        }
    }

    //-------------------------------------------------------------------------
    dispose() {
        if (this._imageContainer) {
            this._imageContainer.destroy();
        }
        
        if (this._moveTween) {
            this._moveTween.stop();
        }
        
        super.dispose();
    }

    //=========================================================================
    // private
    //=========================================================================

    //-------------------------------------------------------------------------
    _initImage() {
        this._imageContainer = this._gameState.game.add.group(this.tutorialView);
        this._imageContainer.x = this._value.x || 0;
        this._imageContainer.y = this._value.y || 0;
        this._imageContainer.angle = this._value.angle || 0;

        this._image = this._gameState.game.add.image(
            0, 0,
            'ui',
            'tutorial-arrow',
            this._imageContainer);
        this._image.anchor.set(0.5);
    }

    //-------------------------------------------------------------------------
    _initTween(): void {
        const TWEEN_DISTANCE = 30;
        const TWEEN_DURATION = 500;
        const AUTO_START = false;
        const REPEAT = -1;
        const SHOULD_YOYO = true;

        this._moveTween = this._gameState.game.add.tween(this._image);
        this._moveTween.to(
            {
                x: -TWEEN_DISTANCE,
            },
            TWEEN_DURATION,
            Phaser.Easing.Cubic.InOut,
            AUTO_START,
            0,
            REPEAT,
            SHOULD_YOYO);
    }
}
