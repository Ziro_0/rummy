import TutorialStepHandler from './TutorialStepHandler';
import IGameState from '../../interfaces/IGameState';

export default class TutorialDiscardCardFromPlayerHandler extends TutorialStepHandler {
    _playerIndex: number;
    _cardId: string;

    //=========================================================================
    // constructor
    //=========================================================================
    /** @constructor */
    constructor(gameState: IGameState, playerIndex: number, cardId: string) {
        super(gameState, null);
        this._playerIndex = playerIndex;
        this._cardId = cardId;
    }

    //=========================================================================
    // public
    //=========================================================================

    //-------------------------------------------------------------------------
    begin(): void {
        const cardsManager = this._gameState.cardsManager;
        const cardImage = cardsManager.searchCardImageById(this._cardId);
        if (!cardImage) {
            this._complete();
            return;
        }

        cardsManager.onCardsMoveComplete.addOnce(
            this._onCardsManagerDiscardCardComplete, this);
        const AUTO_PLAY = true;
        cardsManager.discardCardFromPlayer(this._playerIndex, cardImage, AUTO_PLAY);
    }

    //=========================================================================
    // private
    //=========================================================================

    //-------------------------------------------------------------------------
    _onCardsManagerDiscardCardComplete(): void {
        this._complete();
    }
}
