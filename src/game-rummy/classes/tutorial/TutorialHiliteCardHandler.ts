import TutorialStepHandler from './TutorialStepHandler';
import IGameState from '../../interfaces/IGameState';
import CardImage from '../CardImage';

export default class TutorialHiliteCardHandler extends TutorialStepHandler {
    static readonly DEFAULT_COLOR = 0xffc040;
    static readonly DEFAULT_HILITE_RATE = 250;

    static readonly _TINT_OFF_COLOR = 0xffffff;

    static _nextHandlerId = 0;

    /**
     * key is the handler id
     * @type {object.<string, TutorialHiliteCardHandler>}
     */
    static _registeredHandlers: object = {};

    _cardImages: CardImage[];
    _cardIds: string[];

    _tintTimer: Phaser.Timer;

    _id: string;
    _color: number;
    _speed: number;
    _isTinted: boolean;

    //=========================================================================
    // constructor
    //=========================================================================
    /** @constructor */
    constructor(gameState: IGameState, handlerId: string, cardIds: string | string[],
    color: number | string, speed: number) {
        super(gameState, null);
        this._id = handlerId;
        this._cardIds = typeof cardIds === 'string' ? [ cardIds ] : cardIds;
        this._color = this._resolveColor(color);
        this._speed = speed;
        this._isTinted = false;
    }

    //=========================================================================
    // public
    //=========================================================================

    //-------------------------------------------------------------------------
    begin(): void {
        this._cardImages = this._getCardImages();
        
        let SHOULD_NOT_DISPOSE: boolean = true;
        if (this._cardImages) {
            this._startHiliteTimer();
        } else {
            SHOULD_NOT_DISPOSE = false;
        }

        this._complete(SHOULD_NOT_DISPOSE);
    }

    //-------------------------------------------------------------------------
    dispose() {
        if (this._tintTimer) {
            this._tintTimer.destroy();
        }

        this._applyColorToCards(TutorialHiliteCardHandler._TINT_OFF_COLOR);

        super.dispose();
    }

    //-------------------------------------------------------------------------
    static get NextHandlerId(): string {
        TutorialHiliteCardHandler._nextHandlerId += 1;
        return(`handler-${TutorialHiliteCardHandler._nextHandlerId}`);
    }

    //-------------------------------------------------------------------------
    static Register(gameState: IGameState, handlerId: string,
    cardIds: string | string[],
    color: number | string = TutorialHiliteCardHandler.DEFAULT_COLOR,
    speed: number = TutorialHiliteCardHandler.DEFAULT_HILITE_RATE) {
        if (handlerId in TutorialHiliteCardHandler._registeredHandlers) {
            return; //sanity check; handler already registered with this card id
        }

        const handler = new TutorialHiliteCardHandler(gameState, handlerId,
            cardIds, color, speed);
        TutorialHiliteCardHandler._registeredHandlers[handlerId] = handler;
        handler.begin();
    }

    //-------------------------------------------------------------------------
    static UnhiliteCards(cardIds: string | string[]) {
        const _cardIds: string[] = typeof cardIds === 'string' ? [ cardIds ] : cardIds;
        const handlerIds = Object.keys(TutorialHiliteCardHandler._registeredHandlers);

        handlerIds.forEach(function(handlerId) {
            const handler:TutorialHiliteCardHandler =
                TutorialHiliteCardHandler._registeredHandlers[handlerId];

            _cardIds.forEach((cardId) => {
                handler._removeCard(cardId);
            });
        })
    }

    //-------------------------------------------------------------------------
    static Unregister(handlerId: string) {
        const handler: TutorialHiliteCardHandler =
            TutorialHiliteCardHandler._registeredHandlers[handlerId];
        
        if (!handler) {
            //sanity check; no handler registered with this card id to remove
            return;
        }

        delete TutorialHiliteCardHandler._registeredHandlers[handlerId];
        handler.dispose();
    }
    
    //-------------------------------------------------------------------------
    static UnregisterAll() {
        const handlerIds = Object.keys(TutorialHiliteCardHandler._registeredHandlers);
        handlerIds.forEach(function(handlerId) {
            TutorialHiliteCardHandler.Unregister(handlerId);
        });
    }

    //=========================================================================
    // private
    //=========================================================================

    //-------------------------------------------------------------------------
    _applyColorToCard(cardImage: CardImage, color: number) {
        cardImage.tint = color;
    }

    //-------------------------------------------------------------------------
    _applyColorToCards(color: number) {
        this._cardImages.forEach(function(cardImage) {
            this._applyColorToCard(cardImage, color);
        }, this);
    }

    //-------------------------------------------------------------------------
    _getCardImages(): CardImage[] {
        const cardImages: CardImage[] = [];

        this._cardIds.forEach(function(cardId) {
            if (!cardId) {
                console.warn(`hilite target with unspecified card id.`);
                return;
            }

            const cardImage =
                this._gameState.cardsManager.searchCardImageById(cardId);
            
            if (cardImage) {
                cardImages.push(cardImage);
            } else {
                console.warn(`hilite target with cardId "${cardId}" not found.`);
            }
        }, this);

        return(cardImages.length ? cardImages : null);
    }
    
    //-------------------------------------------------------------------------
    _removeCard(cardId: string) {
        let index = this._cardIds.indexOf(cardId);
        if (index === -1) {
            return;
        }

        const cardImage = this._gameState.cardsManager.searchCardImageById(cardId);
        if (!cardImage) {
            return;
        }

        this._cardIds.splice(index, 1);

        this._applyColorToCard(cardImage, TutorialHiliteCardHandler._TINT_OFF_COLOR);
        
        index = this._cardImages.indexOf(cardImage);
        if (index > -1) {
            this._cardImages.splice(index, 1);
        }

        if (this._cardImages.length === 0) {
            this.dispose();
        }
    }

    //-------------------------------------------------------------------------
    _onTimerTint() {
        this._isTinted = !this._isTinted;

        this._applyColorToCards(this._isTinted ?
            this._color :
            TutorialHiliteCardHandler._TINT_OFF_COLOR);
    }

    //-------------------------------------------------------------------------
    _resolveColor(color: number | string): number {
        if (typeof color === 'string') {
            const pColor = Phaser.Color.hexToColor(<string>color);
            return(pColor.color);
        }

        return(<number>color);
    }

    //-------------------------------------------------------------------------
    _startHiliteTimer() {
        this._tintTimer = this._gameState.game.time.create(false);
        this._tintTimer.loop(this._speed, this._onTimerTint, this);
        this._tintTimer.start();
    }
}
