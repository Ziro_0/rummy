import TutorialStepHandler from './TutorialStepHandler';
import IGameState from '../../interfaces/IGameState';
import { MeldType } from '../MeldType';
import CardData from '../CardData';
import MeldImage from '../MeldImage';
import { AudioEventKey } from '../audio/AudioEventKey';

export default class TutorialCreateMeldHandler extends TutorialStepHandler {
    _meldType: MeldType;
    _playerIndex: number;

    //=========================================================================
    // constructor
    //=========================================================================
    /** @constructor */
    constructor(gameState: IGameState, meldType: MeldType, playerIndex: number) {
        super(gameState, null);
        this._meldType = meldType;
        this._playerIndex = playerIndex;
    }

    //=========================================================================
    // public
    //=========================================================================

    //-------------------------------------------------------------------------
    begin(): void {
        const cardsData = this._getMeldPrepcardsData();
        const targetMeldImage: MeldImage = this._gameState.addMeld(cardsData,
            this._meldType);

        const cardsManager = this._gameState.cardsManager;
        cardsManager.onCardsMoveComplete.addOnce(
            this._onCardsManagerMoveCardsComplete, this);
        cardsManager.moveMeldPrepCardsToMeld(targetMeldImage, this._playerIndex);

        this._gameState.playSound(AudioEventKey.NEW_MELD);
    }

    //=========================================================================
    // private
    //=========================================================================

    //-------------------------------------------------------------------------
    _getMeldPrepcardsData(): CardData[] {
        const cardsData: CardData[] = [];
        this._gameState.meldPrepCardImages.forEach((cardImage) => {
            cardsData.push(cardImage.cardData);
        });

        return(cardsData);
    }

    //-------------------------------------------------------------------------
    _onCardsManagerMoveCardsComplete() {
        this._complete();
    }
}
