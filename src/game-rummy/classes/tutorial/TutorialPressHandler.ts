import TutorialStepHandler from './TutorialStepHandler';
import IGameState from '../../interfaces/IGameState';
import TutorialView from './TutorialView';
import CardImage from '../CardImage';
import { CardValue, CardValueMap } from '../CardValue';
import { CardSuit, CardSuitMap } from '../CardSuit';
import { MainViewbuttonId } from '../MainViewbuttonId';
import Melds from '../Melds';
import MeldImage from '../MeldImage';

export default class TutorialPressHandler extends TutorialStepHandler {
    _targetMeldImage: MeldImage;

    _target: string;
    _id: string;
    _meldImageIndex: number;

    //=========================================================================
    // constructor
    //=========================================================================
    /** @constructor */
    constructor(gameState: IGameState, tutorialView: TutorialView, value: any) {
        super(gameState, tutorialView);
        this._target = value.target;
        this._id = value.id;
        this._meldImageIndex = value.meldIndex;
    }

    //=========================================================================
    // public
    //=========================================================================

    //-------------------------------------------------------------------------
    begin(): void {
        switch (this._target) {
            case 'button':
                // no-op, but shouldn't complete until button is pressed
                break;

            case 'card':
                this._gameState.onCardImageInputDownSgn.add(
                    this._onCardImageInputDown, this);
                this._gameState.onDiscardCardImageInputDownSgn.add(
                    this._onCardImageInputDown, this);
                break;

            case 'deck':
                this._gameState.onDeckInputDownSgn.addOnce(
                    this._onDeckInputDown, this);
                break;

            case 'meld-image':
                this._targetMeldImage = this._gameState.getMeldImage(this._meldImageIndex);
                if (!this._targetMeldImage) {
                    this._complete();
                }

                this._targetMeldImage.enabled = true;
                this._gameState.melds.onMeldImageInputDown.addOnce(
                    this._onMeldImageInputDown, this);

                break;

            default:
                this._complete();
                break;
        }
    }

    //-------------------------------------------------------------------------
    dispose(): void {
        this._gameState.onDeckInputDownSgn.remove(this._onDeckInputDown, this);
        this._gameState.onCardImageInputDownSgn.remove(this._onCardImageInputDown, this);
        this._gameState.onDiscardCardImageInputDownSgn.remove(
            this._onCardImageInputDown, this);
        this._gameState.onDiscardCardImageInputDownSgn.remove(
            this._onMeldImageInputDown, this);

        super.dispose();
    }

    //-------------------------------------------------------------------------
    handleButtonPressed(id: MainViewbuttonId) {
        if (this._target === 'button' && <MainViewbuttonId>this._id === id) {
            this._complete();
        }
    }
    
    //=========================================================================
    // private
    //=========================================================================

    //-------------------------------------------------------------------------
    _onCardImageInputDown(cardImage: CardImage) {
        const id = this._id;
        const value: CardValue = CardValueMap[id.substring(0, id.length - 1)];
        const suit: CardSuit = CardSuitMap[id.charAt(id.length - 1)];
        
        const cardData = cardImage.cardData;
        if (cardData.value === value && cardData.suit === suit) {
            this._complete();
        }
    }

    //-------------------------------------------------------------------------
    _onDeckInputDown() {
        this._complete();
    }

    //-------------------------------------------------------------------------
    _onMeldImageInputDown(melds: Melds, meldImage: MeldImage) {
        if (this._targetMeldImage === meldImage) {
            this._targetMeldImage.enabled = false;
            this._complete();
        }
    }
}
