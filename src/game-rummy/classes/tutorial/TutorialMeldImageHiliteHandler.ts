import TutorialStepHandler from './TutorialStepHandler';
import IGameState from '../../interfaces/IGameState';
import MeldImage from '../MeldImage';
import MeldIconPair from '../MeldIconPair';

export default class TutorialMeldImageHiliteHandler extends TutorialStepHandler {
    static readonly DEFAULT_COLOR = 0x00ff00;
    static readonly DEFAULT_HILITE_RATE = 250;

    static readonly _TINT_OFF_COLOR = 0xffffff;

    static _registeredHandler: TutorialMeldImageHiliteHandler;

    _meldImage: MeldImage;

    _tintTimer: Phaser.Timer;

    _meldIndex: number;
    _color: number;
    _speed: number;
    _isTinted: boolean;

    //=========================================================================
    // constructor
    //=========================================================================
    /** @constructor */
    constructor(gameState: IGameState, meldIndex: number,
    color: number | string, speed: number) {
        super(gameState, null);
        this._meldIndex = meldIndex;
        this._color = this._resolveColor(color);
        this._speed = speed;
        this._isTinted = false;
    }

    //=========================================================================
    // public
    //=========================================================================

    //-------------------------------------------------------------------------
    begin(): void {
        this._meldImage = this._gameState.getMeldImage(this._meldIndex);
        
        let SHOULD_NOT_DISPOSE: boolean = true;
        if (this._meldImage) {
            this._startHiliteTimer();
        } else {
            SHOULD_NOT_DISPOSE = false;
        }

        this._complete(SHOULD_NOT_DISPOSE);
    }

    //-------------------------------------------------------------------------
    dispose() {
        if (this._tintTimer) {
            this._tintTimer.destroy();
        }

        this._applyColorToButton(TutorialMeldImageHiliteHandler._TINT_OFF_COLOR);
        super.dispose();
    }

    //-------------------------------------------------------------------------
    static Register(gameState: IGameState, meldImage: number,
    color: number | string = TutorialMeldImageHiliteHandler.DEFAULT_COLOR,
    speed: number = TutorialMeldImageHiliteHandler.DEFAULT_HILITE_RATE) {
        if (TutorialMeldImageHiliteHandler._registeredHandler) {
            return; //sanity check; can only hilite one meld at a time
        }

        const handler = new TutorialMeldImageHiliteHandler(gameState, meldImage,
            color, speed);
        TutorialMeldImageHiliteHandler._registeredHandler = handler;
        handler.begin();
    }
    
    //-------------------------------------------------------------------------
    static Unregister() {
        const handler: TutorialMeldImageHiliteHandler =
            TutorialMeldImageHiliteHandler._registeredHandler;
        
        if (!handler) {
            //sanity check; no handler registered with this id to remove
            return;
        }

        TutorialMeldImageHiliteHandler._registeredHandler = null;
        handler.dispose();
    }

    //=========================================================================
    // private
    //=========================================================================

    //-------------------------------------------------------------------------
    _applyColorToButton(color: number) {
        const meldImage = this._meldImage;

        for (let index = 0; index < meldImage.length; index++) {
            const pair: MeldIconPair = meldImage.getChildAt(index) as MeldIconPair;
            if (pair instanceof MeldIconPair) {
                pair.forEach(function(pairIconImage: Phaser.Image) {
                    pairIconImage.tint = color;
                });
            }
        }
    }

    //-------------------------------------------------------------------------
    _onTimerTint() {
        this._isTinted = !this._isTinted;

        this._applyColorToButton(this._isTinted ?
            this._color :
            TutorialMeldImageHiliteHandler._TINT_OFF_COLOR);
    }

    //-------------------------------------------------------------------------
    _resolveColor(color: number | string): number {
        if (typeof color === 'string') {
            const pColor = Phaser.Color.hexToColor(<string>color);
            return(pColor.color);
        }

        return(<number>color);
    }

    //-------------------------------------------------------------------------
    _startHiliteTimer() {
        this._tintTimer = this._gameState.game.time.create(false);
        this._tintTimer.loop(this._speed, this._onTimerTint, this);
        this._tintTimer.start();
    }
}
