export enum CardValue {
    ACE = 'a',
    TWO = '2',
    THREE = '3',
    FOUR = '4',
    FIVE = '5',
    SIX = '6',
    SEVEN = '7',
    EIGHT = '8',
    NINE = '9',
    TEN = '10',
    JACK = 'j',
    QUEEN = 'q',
    KING = 'k',
};

export const CardValueMap = {
    'a': CardValue.ACE,
    '2': CardValue.TWO,
    '3': CardValue.THREE,
    '4': CardValue.FOUR,
    '5': CardValue.FIVE,
    '6': CardValue.SIX,
    '7': CardValue.SEVEN,
    '8': CardValue.EIGHT,
    '9': CardValue.NINE,
    '10': CardValue.TEN,
    'j': CardValue.JACK,
    'q': CardValue.QUEEN,
    'k': CardValue.KING
};
