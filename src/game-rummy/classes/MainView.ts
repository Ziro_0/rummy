import Button from "./Button";
import Melds from "./Melds";
import { Game } from "../game";
import CardData from "./CardData";
import { MeldType } from "./MeldType";
import MeldImage from "./MeldImage";
import IGameState from "../interfaces/IGameState";
import TimerBar from "./TimerBar";
import TutorialView from './tutorial/TutorialView';
import { MainViewbuttonId } from './MainViewbuttonId';

export default class MainView {
    _gameState: IGameState;

    cancelMeldButtonPressed: Phaser.Signal;
    meldLayOffButtonPressed: Phaser.Signal;
    dealButtonPressed: Phaser.Signal;
    finishMeldButtonPressed: Phaser.Signal;
    sortButtonPressed: Phaser.Signal;
    tutorialViewClosed: Phaser.Signal;

    playersNameText: Phaser.BitmapText[];
    playersScoreText: Phaser.BitmapText[];

    _melds: Melds;

    _tutorialView: TutorialView;

    _dealButton: Button;
    _sortButton: Button;
    _meldLayOfftButton: Button;
    _cancelMeldButton: Button;
    _finishMeldButton: Button;

    _buttonsGroup: Phaser.Group;
    _timeBarsGroup: Phaser.Group;
    _cardsGroup: Phaser.Group;
    _deckAndDiscardGroup: Phaser.Group;

    _discardPilePosition: Phaser.Point;
    _meldMargin: Phaser.Point;

    _meldIconsScale: number;
    _yStartPosition: number;

    //=========================================================================
    // constructor
    //=========================================================================
    /** @constructor */
    constructor(gameState: IGameState) {
        this._gameState = gameState;

        this.cancelMeldButtonPressed = new Phaser.Signal();
        this.meldLayOffButtonPressed = new Phaser.Signal();
        this.dealButtonPressed = new Phaser.Signal();
        this.finishMeldButtonPressed = new Phaser.Signal();
        this.sortButtonPressed = new Phaser.Signal();
        this.tutorialViewClosed = new Phaser.Signal();

        this._initBackground();

        const game: Game = this._gameState.game;
        this._buttonsGroup = game.add.group();
        this._timeBarsGroup = game.add.group();
        this._deckAndDiscardGroup = game.add.group();
        this._cardsGroup = game.add.group();
        
        this._initButtons();
        this._initTimeBars();
        this._initPositions();
        this._initNameTexts();

        this._melds = new Melds(this._gameState);

        this._setDisplayObjectsVisibile([
            this._sortButton,
            this._cancelMeldButton,
            this._meldLayOfftButton
        ], false);
    }

    //=========================================================================
    // public
    //=========================================================================

    //-------------------------------------------------------------------------
    addMeld(cardsData: CardData[], type: MeldType, targetMeld?: MeldImage): MeldImage {
        return(this._melds.add(cardsData, type, targetMeld));
    }

    //-------------------------------------------------------------------------
    set cancelMeldButtonVisible(value: boolean) {
        this._cancelMeldButton.visible = value;
    }

    //-------------------------------------------------------------------------
    get cardsGroup(): Phaser.Group {
        return(this._cardsGroup);
    }

    //-------------------------------------------------------------------------
    closeTutorialView() {
        this._destroyTutorialView();
    }

    //-------------------------------------------------------------------------
    set dealButtonVisible(value: boolean) {
        this._dealButton.visible = value;
    }

    //-------------------------------------------------------------------------
    get deckAndDiscardGroup(): Phaser.Group {
        return(this._deckAndDiscardGroup);
    }

    //-------------------------------------------------------------------------
    get discardPilePosition(): Phaser.Point {
        return(this._discardPilePosition);
    }

    //-------------------------------------------------------------------------
    dispose() {
        this.cancelMeldButtonPressed.dispose();
        this.meldLayOffButtonPressed.dispose();
        this.dealButtonPressed.dispose();
        this.sortButtonPressed.dispose();
        this.tutorialViewClosed.dispose();
        this._melds.dispose();
        this._destroyTutorialView();
    }

    //-------------------------------------------------------------------------
    set finishMeldButtonVisible(value: boolean) {
        this._finishMeldButton.visible = value;
    }

    //-------------------------------------------------------------------------
    getButton(buttonId: MainViewbuttonId): Button {
        switch (buttonId) {
            case MainViewbuttonId.CANCEL_MELD:
                return(this._cancelMeldButton);

            case MainViewbuttonId.DEAL:
                return(this._dealButton);

            case MainViewbuttonId.FINISH_MELD:
                return(this._finishMeldButton);

            case MainViewbuttonId.MELD_LAY_OFF:
                return(this._meldLayOfftButton);

            case MainViewbuttonId.SORT:
                return(this._sortButton);

            default:
                return(null);
        }
    }
    
    //-------------------------------------------------------------------------
    getTimerBar(playerIndex: number): TimerBar {
        return(this._timeBarsGroup.getAt(playerIndex) as TimerBar);
    }

    //-------------------------------------------------------------------------
    set meldLayOfftButtonVisible(value: boolean) {
        this._meldLayOfftButton.visible = value;
    }
    
    //-------------------------------------------------------------------------
    get melds(): Melds {
        return(this._melds);
    }

    //-------------------------------------------------------------------------
    openTutorial() {
        if (!this._tutorialView) {
            this._tutorialView = new TutorialView(this._gameState);
            this._tutorialView.onClose.addOnce(this._tutorialViewOnClose, this);
        }
    }

    //-------------------------------------------------------------------------
    set sortButtonVisible(value: boolean) {
        this._sortButton.visible = value;
    }

    //-------------------------------------------------------------------------
    get tutorialView(): TutorialView {
        return(this._tutorialView);
    }
    
    //=========================================================================
    // private
    //=========================================================================

    //-------------------------------------------------------------------------
    _destroyTutorialView() {
        if (this._tutorialView) {
            this._tutorialView.destroy();
            this._tutorialView = null;
        }
    }

    //-------------------------------------------------------------------------
    _tutorialViewOnClose() {
        this.tutorialViewClosed.dispatch(this);
    }

    //-------------------------------------------------------------------------
    _initBackground() {
        const world = this._gameState.game.world;
        const backgroundImage = this._gameState.game.add.image(0, 0, 'ui', 'background');
        backgroundImage.width = world.width;
        if (backgroundImage.height < world.height) {
            backgroundImage.height = world.height;
            backgroundImage.scale.x = backgroundImage.scale.y;
        } else {
            backgroundImage.scale.y = backgroundImage.scale.x;
        }
    }

    //-------------------------------------------------------------------------
    _initButton(position: any, namePrefix: string, onPressedCallback): Button {
        const button = new Button(
            this._gameState,
            position.x, position.y,
            'ui',
            onPressedCallback,
            this,
            namePrefix);

        this._buttonsGroup.add(button);
        return(button);
    }

    //-------------------------------------------------------------------------
    _initButtons() {
        const jsonData = this._gameState.metricsJsonData;
        this._dealButton = this._initButton(
            jsonData.dealButtonPosition,
            'game_button_deal',
            this._onDealButtonPressed);

        this._sortButton = this._initButton(
            jsonData.sortHandButtonPosition,
            'game_button_sort_hand',
            this._onSortButtonPressed);

        this._meldLayOfftButton = this._initButton(
            jsonData.meldLayOffButtonPosition,
            'game_button_meld_lay_off',
            this._onMeldLayOffButtonPressed);

        this._cancelMeldButton = this._initButton(
            jsonData.cancelMeldButtonPosition,
            'game_button_cancel_meld',
            this._onCancelMeldButtonPressed);

        this._finishMeldButton = this._initButton(
            jsonData.finishMeldButtonPosition,
            'game_button_finish_meld',
            this._onFinishMeldButtonPressed);
    }

    //-------------------------------------------------------------------------
    _initDiscardPilePosition() {
        const jsonData = this._gameState.metricsJsonData;
        const discardPilePosition = jsonData.discardPilePosition;
        this._discardPilePosition = new Phaser.Point(
            discardPilePosition.x,
            discardPilePosition.y);
    }

    //-------------------------------------------------------------------------
    _initMeldIconPositions() {
        const jsonData = this._gameState.metricsJsonData;
        const meldIcons = jsonData.meldIcons;
        this._meldIconsScale = meldIcons.scale;
        this._yStartPosition = meldIcons.yStartPosition;
        this._meldMargin = new Phaser.Point(
            meldIcons.meldMargin.x,
            meldIcons.meldMargin.y);
    }

    //-------------------------------------------------------------------------
    _initNameTexts() {
        const jsonData = this._gameState.metricsJsonData;
        const playersData: any[] = jsonData.playersData;

        this.playersNameText = [
            this._gameState.game.add.bitmapText(
                playersData[0].namePosition.x,
                playersData[0].namePosition.y,
                'modenine'),

            this._gameState.game.add.bitmapText(
                playersData[1].namePosition.x,
                playersData[1].namePosition.y,
                'modenine')
        ];

        this.playersScoreText = [
            this._gameState.game.add.bitmapText(
                playersData[0].scorePosition.x,
                playersData[0].scorePosition.y,
                'modenine'),

            this._gameState.game.add.bitmapText(
                playersData[1].scorePosition.x,
                playersData[1].scorePosition.y,
                'modenine')
        ];
    }

    //-------------------------------------------------------------------------
    _initPositions() {
        this._initDiscardPilePosition();
        this._initMeldIconPositions();
    }

    //-------------------------------------------------------------------------
    _initTimeBars() {
        const jsonData = this._gameState.metricsJsonData;
        const jPlayersData: any[] = jsonData.playersData;
        
        jPlayersData.forEach(function(jPlayerData: any) {
            const yTimeBar: number = jPlayerData.yTimeBar;
            const timerBar = new TimerBar(this._gameState.game,
                this._gameState.game.world.centerX, yTimeBar, this._timeBarsGroup);
            timerBar.visible = false;
        }, this);
    }

    //-------------------------------------------------------------------------
    _onCancelMeldButtonPressed() {
        this.cancelMeldButtonPressed.dispatch(this);
    }

    //-------------------------------------------------------------------------
    _onDealButtonPressed() {
        this.dealButtonPressed.dispatch(this);
    }

    //-------------------------------------------------------------------------
    _onFinishMeldButtonPressed() {
        this.finishMeldButtonPressed.dispatch(this);
    }

    //-------------------------------------------------------------------------
    _onMeldLayOffButtonPressed() {
        this.meldLayOffButtonPressed.dispatch(this);
    }

    //-------------------------------------------------------------------------
    _onSortButtonPressed() {
        this.sortButtonPressed.dispatch(this);
    }

    //-------------------------------------------------------------------------
    _setDisplayObjectsVisibile(displayObjects: PIXI.DisplayObject[], isVisible: boolean) {
        displayObjects.forEach(function(displayObject) {
            if (displayObject) {
                displayObject.visible = isVisible;
            }
        });
    }
}
