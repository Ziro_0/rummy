import CardData from "./CardData";
import MeldImage from "./MeldImage";
import CardImage from "./CardImage";
import { MeldType } from "./MeldType";
import IGameState from "../interfaces/IGameState";

export default class Melds {
    onMeldImageInputDown: Phaser.Signal;

    /**
     * each group contains a Phaser.Group<MeldImage> as children
     */
    _meldRows: Phaser.Group[];

    _gameState: IGameState;

    //=========================================================================
    // constructor
    //=========================================================================
    /** @constructor */
    constructor(gameState: IGameState) {
        this._gameState = gameState;
        this.onMeldImageInputDown = new Phaser.Signal();
        this._meldRows = [];
    }

    //=========================================================================
    // public
    //=========================================================================

    //-------------------------------------------------------------------------
    /**
     * @method Melds#add
     * @param {CardData[]} cardsData 
     * @param {MeldType} [type] If this is a new meld, this param is required
     * @param {MeldImage} targetMeld
     * @returns {MeldImage} The meld image that was created or modifiied.
     */
    add(cardsData: CardData[], type?: MeldType, targetMeld?: MeldImage): MeldImage {
        const jsonData = this._gameState.metricsJsonData;
        const melds: MeldImage[] = this._removeAllMelds();
        this._meldRows.length = 0;
        
        let outMeld: MeldImage;
        if (targetMeld) {
            targetMeld.addIconPairs(cardsData);
            outMeld = targetMeld;
        } else {
            outMeld = this._createNewMeldImage(cardsData, type);
            melds.push(outMeld);
        }

        const game = this._gameState.game;

        let meldRow: Phaser.Group = game.add.group();
        this._meldRows.push(meldRow);
        meldRow.y = jsonData.meldIcons.yStartPosition;

        melds.forEach(function(meld, index) {
            let x = index === 0 ? 0 : meldRow.width + jsonData.meldIcons.meldMargin.x;
            if (x + meld.width >= game.world.width) {
                const newMeldRow: Phaser.Group = game.add.group();
                newMeldRow.y = meldRow.y + meldRow.height + jsonData.meldIcons.meldMargin.y;
                this._meldRows.push(newMeldRow);
                meld.x = 0;
                newMeldRow.add(meld);
                if (newMeldRow.width > game.world.width) {
                    newMeldRow.width = game.world.width;
                    newMeldRow.height = newMeldRow.height * newMeldRow.scale.x;
                }
                newMeldRow.x = (game.world.width - newMeldRow.width) / 2;
                meldRow = newMeldRow;
            } else {
                meld.x = x;
                meldRow.add(meld);
                meldRow.x = (game.world.width - meldRow.width) / 2;
            }
        }, this);

        return(outMeld);
    }

    //-------------------------------------------------------------------------
    clear() {
        const meldImages: MeldImage[] = [];
        this.forEach(function(meldImage: MeldImage) {
            meldImages.push(meldImage);
        });

        meldImages.forEach(function(meldImage) {
            meldImage.destroy();
        });
    }

    //-------------------------------------------------------------------------
    disable() {
        this.forEach(function(meldImage: MeldImage) {
            meldImage.enabled = false;
        });
    }

    //-------------------------------------------------------------------------
    dispose() {
        this.onMeldImageInputDown.dispose();
        
        const DESTROY_MELD_IMAGES = true;
        this._removeAllMelds(DESTROY_MELD_IMAGES);
    }

    //-------------------------------------------------------------------------
    /**
     * Calls a function on each meld image in each row of melds. The callback function
     * signature is:
     * `callback(meldImage: MeldImage, meldIndex: number): any`.
     * Return a truthy value to abort the function loop
     * @param {Function} callback The callback function to be called on each item.
     * @param {any} context The context (this) in which the callback function is called.
     */
    forEach(callback: Function, context?: any) {
        if (!callback) {
            return;
        }

        let meldIndex = 0;
        for (let rowIndex = 0; rowIndex < this._meldRows.length; rowIndex++) {
            const meldRow = this._meldRows[rowIndex];
            for (let column = 0; column < meldRow.length; column++) {
                const meldImage = meldRow.getAt(column) as MeldImage;
                if (meldImage instanceof MeldImage) {
                    if (!!callback.call(context, meldImage, meldIndex)) {
                        return;
                    }
                    meldIndex += 1;
                }
            }
        }
    }

    //-------------------------------------------------------------------------
    getMeldImage(index: number): MeldImage {
        let targetMeldImage: MeldImage = null;

        this.forEach(function(meldImage: MeldImage, meldIndex: number) {
            if (index === meldIndex) {
                targetMeldImage = meldImage;
                return(true);
            }
        });

        return(targetMeldImage);
    }

    //-------------------------------------------------------------------------
    getValidLayoffMelds(cards: CardImage[] | CardData[]): MeldImage[] {
        const layoffMelds: MeldImage[] = [];

        this.forEach(function(meldImage: MeldImage) {
            if (meldImage.isLayoffPossible(cards)) {
                layoffMelds.push(meldImage);
            }
        });
        
        return(layoffMelds);
    }

    //-------------------------------------------------------------------------
    isLayOffPossible(cards: CardImage[] | CardData[]): boolean {
        return(this.getValidLayoffMelds(cards).length > 0);
    }

    //-------------------------------------------------------------------------
    get numMelds(): number {
        let count = 0;
        this.forEach(function(meldImage: MeldImage) {
            count++;
        });
        return(count);
    }

    //=========================================================================
    // private
    //=========================================================================

    //-------------------------------------------------------------------------
    _createNewMeldImage(cardsData: CardData[], type: MeldType): MeldImage {
        const meldImage = new MeldImage(this._gameState, 0, 0, cardsData, type, null);
        meldImage.onDown.add(this._onMeldImageDown, this);
        return(meldImage);
    }

    //-------------------------------------------------------------------------
    _onMeldImageDown(meldImage: MeldImage) {
        if (this.onMeldImageInputDown) {
            this.onMeldImageInputDown.dispatch(this, meldImage);
        }
    }

    //-------------------------------------------------------------------------
    _removeAllMelds(destroyMeldImages = false): MeldImage[] {
        let melds: MeldImage[] = [];

        this._meldRows.forEach(function(meldRow) {
            if (!destroyMeldImages) {
                melds = melds.concat(meldRow.children as MeldImage[]);
            }
            meldRow.destroy(destroyMeldImages);
        });

        return(melds);
    }
}
