export enum CardSuit {
    HEART = 'h',
    DIAMOND = 'd',
    CLUB = 'c',
    SPADE = 's'
};

export const CardSuitMap = {
    'h': CardSuit.HEART,
    'd': CardSuit.DIAMOND,
    'c': CardSuit.CLUB,
    's': CardSuit.SPADE
};
