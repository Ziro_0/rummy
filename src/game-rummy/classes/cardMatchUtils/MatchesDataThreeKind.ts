import CardData from "../CardData";
import CardMatchUtils from "./CardMatchUtils";

export default class MatchesDataThreeKind {
    //=========================================================================
    // public
    //=========================================================================

    //-------------------------------------------------------------------------
    // ExecMatch
    //-------------------------------------------------------------------------
    static ExecMatch(cardsData: CardData[]): boolean {
        if (!cardsData) {
            //sanity check
            return(false);
        }

        const sortedCardsDes = CardMatchUtils.SortCardsByDescendingValue(cardsData);
        const NUM_CARDS_TO_MATCH = 3;
        if (CardMatchUtils.DoCardsHaveSameValue(sortedCardsDes, NUM_CARDS_TO_MATCH)) {
            return(true);
        }

        return(false);
    };
}
