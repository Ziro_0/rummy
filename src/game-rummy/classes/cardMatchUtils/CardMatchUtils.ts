import CardData from "../CardData";
import { CardValue } from "../CardValue";
import { CardSuit } from "../CardSuit";
import CardValueRanks from "../CardValueRanks";

export default class CardMatchUtils {
    //=========================================================================
    // public
    //=========================================================================

    //-------------------------------------------------------------------------
    // AreCardsFlush
    //-------------------------------------------------------------------------
    static AreCardsFlush(cardsData: CardData[]): boolean {
        //order of cards doesn't matter
        if (!cardsData) {
            return(false); //sanity check
        }
        
        let matchingSuit: CardSuit;
        for (let i_index = 0; i_index < cardsData.length; i_index += 1) {
            let cardData = cardsData[i_index];
            if (!cardData) {
                return(false); //sanity check
            }

            if (i_index === 0) {
                matchingSuit = cardData.suit;
                continue;
            }
            
            if (matchingSuit !== cardData.suit) {
                return(false);
            }
        }
        
        return(true);
    };

    //-------------------------------------------------------------------------
    // AreCardsStraightFlush
    //-------------------------------------------------------------------------
    static AreCardsStraightFlush(cardsSortedDesc: CardData[], minLength?: number,
    out?: CardData[]): boolean {
        //Cards should already be sorted by descending value before calling this function.
        if (!cardsSortedDesc || cardsSortedDesc.length === 0) {
            return(false);
        }

        const MIN_NUM_TO_MATCH = 3;
        if (cardsSortedDesc.length < MIN_NUM_TO_MATCH) {
            return(false);
        }
        
        let longestChain: CardData[] = [];
        let tmpChain: CardData[] = [];

        for (let index1 = 0; index1 < cardsSortedDesc.length; index1 += 1) {
            let cardData1 = cardsSortedDesc[index1];
            if (!cardData1) {
                return (false); //sanity check
            }

            let rank1 = CardValueRanks.RankOf(cardData1.value);

            let currentMatchCount = 1;

            //console.log('new base ', cardData1);

            tmpChain.length = 0;
            tmpChain.push(cardData1);

            let expectedRankDiff = 1;

            for (let index2 = index1 + 1; index2 < cardsSortedDesc.length; index2 += 1) {
                let cardData2 = cardsSortedDesc[index2];
                if (!cardData2) {
                    return(false); //sanity check
                }

                if (cardData2.suit !== cardData1.suit) {
                    //console.log('suits dont match');
                    continue;
                }

                let rank2 = CardValueRanks.RankOf(cardData2.value);
                let rankDiff = rank1 - rank2;
                if (rankDiff === 0) {
                    //skip cards with same rank
                    //console.log('same rank - skip');
                    continue;
                } else if (rankDiff !== expectedRankDiff) {
                    //next card's rank is too low, skip remaining cards
                    //console.log('rank to low - break');
                    break;
                }

                expectedRankDiff += 1;
                currentMatchCount = currentMatchCount + 1;
                
                tmpChain.push(cardData2);
            }

            if (tmpChain.length > longestChain.length) {
                longestChain = tmpChain.concat();
                //console.log('new longest chain ', longestChain)
            }
        }

        if (out) {
            out.length = 0;
            longestChain.forEach(function(cardData) {
                out.push(cardData);
            });
        }

        if (minLength === undefined) {
            minLength = MIN_NUM_TO_MATCH;
        }
        //console.log('final  chain ', longestChain)
        return(longestChain.length >= minLength);
    }

    //-------------------------------------------------------------------------
    // CountCardsByValue
    //-------------------------------------------------------------------------
    static CountCardsByValue(cardsData: CardData[], cardValue: CardValue): number {
        let i_numCardsFound = 0;
        
        if (cardsData) {
            cardsData.forEach(function(cardData) {
                if (cardData && cardData.value === cardValue) {
                    i_numCardsFound += 1;
                }
            });
        }
        
        return(i_numCardsFound);
    };

    //-------------------------------------------------------------------------
    // DoCardsHaveSameColor
    //-------------------------------------------------------------------------
    static DoCardsHaveSameColor(cardData1: CardData, cardData2: CardData): boolean {
        return(
            (CardMatchUtils.IsCardRed(cardData1) && CardMatchUtils.IsCardRed(cardData2)) ||
            (CardMatchUtils.IsCardBlack(cardData1) && CardMatchUtils.IsCardBlack(cardData2)));
    };

    //-------------------------------------------------------------------------
    // DoCardsHaveSameValue
    //-------------------------------------------------------------------------
    static DoCardsHaveSameValue(sortedCardsData: CardData[], u_numToMatch: number,
    out?: CardData[]): boolean {
        //cards are assumed to be sorted vard value (sort order doesn't matter)
        if (!Array.isArray(sortedCardsData) || !sortedCardsData.length) {
            return(false); //sanity checks
        }
        
        let u_baseCardValue: CardValue;
        let u_numMatched = 0;
        
        for (let u_index = 0; u_index < sortedCardsData.length; u_index +=1 ) {
            let cardData = sortedCardsData[u_index];
            if (!cardData) {
                return (false); //sanity check
            }

            if (u_index === 0) {
                //first card starts off the number of matches with a base card value
                u_baseCardValue = cardData.value;
                u_numMatched = 1;

                if (out) {
                    out.length = 0;
                    out.push(cardData);
                }
                continue;
            }
            
            if (u_baseCardValue === cardData.value) {
                //this card's value matches the base card value
                u_numMatched += 1;
                
                if (out) {
                    out.push(cardData);
                }

                if (u_numMatched === u_numToMatch) {
                    //specified number of cards have the same value - success
                    return(true);
                }
            } else {
                //encountered a different card value - reset
                u_baseCardValue = cardData.value;
                u_numMatched = 1;
                if (out) {
                    out.length = 0;
                    out.push(cardData);
                }
            }
        }
        
        //unable to satisfy a match - fail
        return(false);
    };

    //-------------------------------------------------------------------------
    // DoCardsMatchInOrder
    //-------------------------------------------------------------------------
    static DoCardsMatchInOrder(cardsData: CardData[],
    au_cardValuesToCheck: CardValue[]): boolean {
        //cards are assumed to be sorted by descending value
        if (!Array.isArray(cardsData) || !cardsData.length ||
            !Array.isArray(au_cardValuesToCheck) || !au_cardValuesToCheck.length) {
            return(false); //sanity checks
        }
        
        let u_numCardValues = au_cardValuesToCheck.length;
        if (u_numCardValues > cardsData.length) {
            //there must exist at least as many card values being check as there are cards available
            return(false);
        }
        
        for (let u_index = 0; u_index < u_numCardValues; u_index += 1) {
            let cardData = cardsData[u_index];
            if (!cardData) {
                return(false); //sanity check
            }
            
            if (cardData.value !== au_cardValuesToCheck[u_index]) {
                return(false);
            }
        }
        
        return(true);
    };

    //-------------------------------------------------------------------------
    // IsCardBlack
    //-------------------------------------------------------------------------
    static IsCardBlack(cardData: CardData): boolean {
        return(cardData &&
            (cardData.suit === CardSuit.CLUB || cardData.suit === CardSuit.SPADE));
    };

    //-------------------------------------------------------------------------
    // IsCardRed
    //-------------------------------------------------------------------------
    static IsCardRed(cardData: CardData): boolean {
        return(cardData &&
            (cardData.suit === CardSuit.HEART || cardData.suit === CardSuit.DIAMOND));
    };

    //-------------------------------------------------------------------------
    // IsDeuce
    //-------------------------------------------------------------------------
    static IsDeuce(cardData: CardData): boolean {
        return(cardData && cardData.value === CardValue.TWO);
    };

    //-------------------------------------------------------------------------
    // SortCardsByAscendingValue
    //-------------------------------------------------------------------------
    //Returns the array of sortec card Ui. The original array is unmodified.
    static SortCardsByAscendingValue(cardsData: CardData[]): CardData[] {
        if (!cardsData || cardsData.length === 0) {
            return(null); //sanity check
        }
        
        const sortedCardsData = cardsData.concat();
        sortedCardsData.sort(CardMatchUtils._OnSortCardsByAscendingValue);
        return(sortedCardsData);
    };

    //-------------------------------------------------------------------------
    // SortCardsByDescendingValue
    //-------------------------------------------------------------------------
    //Returns the array of sortec card Ui. The original array is unmodified.
    static SortCardsByDescendingValue(cardsData: CardData[]): CardData[] {
        if (!cardsData || cardsData.length === 0) {
            return(null); //sanity check
        }
        
        const sortedCardsData = cardsData.concat();
        sortedCardsData.sort(CardMatchUtils._OnSortCardsByDescendingValue);
        return(sortedCardsData);
    };

    //=========================================================================
    //"private"
    //=========================================================================

    //-------------------------------------------------------------------------
    // _OnSortCardsByAscendingValue
    //-------------------------------------------------------------------------
    static _OnSortCardsByAscendingValue(cardData1: CardData, cardData2: CardData): number {
        if (!cardData1 || !cardData2) {
            return(0);
        }

        const rank1 = CardValueRanks.RankOf(cardData1.value);
        const rank2 = CardValueRanks.RankOf(cardData2.value);

        if (rank1 < rank2) {
            return(-1);
        } else if (rank1 > rank2) {
            return(1);
        }
        
        return(0);
    };

    //-------------------------------------------------------------------------
    // _OnSortCardsByDescendingValue
    //-------------------------------------------------------------------------
    static _OnSortCardsByDescendingValue(cardData1: CardData, cardData2: CardData): number {
        if (!cardData1 || !cardData2) {
            return(0);
        }
        
        const rank1 = CardValueRanks.RankOf(cardData1.value);
        const rank2 = CardValueRanks.RankOf(cardData2.value);
        if (rank1 < rank2) {
            return(1);
        } else if (rank1 > rank2) {
            return(-1);
        }
        
        return(0);
    };
}
