import CardData from "../CardData";
import CardMatchUtils from "./CardMatchUtils";

export default class MatchesDataStraightFlush {
    //=========================================================================
    // public
    //=========================================================================

    //-------------------------------------------------------------------------
    // ExecMatch
    //-------------------------------------------------------------------------
    static ExecMatch(cardsData: CardData[], minLength?: number, out?: CardData[]): boolean {
        const vo_sortedCardsDes = CardMatchUtils.SortCardsByDescendingValue(cardsData);
        return(
            CardMatchUtils.AreCardsStraightFlush(vo_sortedCardsDes, minLength, out));
    };
}
