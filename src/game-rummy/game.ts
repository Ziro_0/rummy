// import pixi, p2 and phaser ce
import 'pixi';
import 'p2';
import * as Phaser from 'phaser-ce';

// import states
import BootState from './states/BootState';
import PreloadState from './states/PreloadState';
import GameState from './states/GameState';

/**
 * Main entry game class
 * @export
 * @class Game
 * @extends {Phaser.Game}
 */
export class Game extends Phaser.Game {
    config: any;
    listenerMapping: any = {};

    /**
     * Creates an instance of Game.
     * @memberof Game
     */
    constructor(width, height) {
        // call parent constructor
        // super(width, height, Phaser.CANVAS, 'game', null);
        super(768, 1024, Phaser.CANVAS, 'game', null);

        // add some game states
        this.state.add('BootState', new BootState(this));
        this.state.add('PreloadState', new PreloadState(this));
        this.state.add('GameState', new GameState(this));
    }

    endGame() {
        this.paused = true;
        setTimeout(() => {
            this.destroy();
        }, 100);
    }

    listen(listenValue: string, callback: Function) {
        this.listenerMapping[listenValue] = callback;
    }

    resurrect() {
        // this.core.respawn();
    }

    startGame(config: any) {
        console.log('game has started');
        
        this.config = config;
        
        console.log("config:");
        console.log(this.config);

        this.state.start('BootState');
    }

    /**
     * @private
     */
    _getCurrentGameState(): any {
        const state: any = this.state.getCurrentState();
        return(state && state.key === 'GameState' ? state : null);
    }
}
