import { Component } from '@angular/core';
import { Platform, MenuController, NavController, NavParams } from 'ionic-angular';
import { Game as GameRummy } from '../../game-rummy/game';

@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
export class HomePage {
    showAlertMessage: boolean = true;
    autoplayTimer: any = null;
    isDemo: boolean = true;
    isIos: boolean = false;
    isTutorial: boolean = false;
    finalStageNumber: number = 7;
    tutorialStage: number = 1;
    visibleState: string = 'visible';
    randomLeft: number = 0;
    opponentPoints: number = 0;
    opponentPointsLoading: boolean = true;
    bottomPosition: string = '-100px';
    staticTimerFn: any = null;
    internetAvailable: boolean = true;
    isBumper: boolean = false;
    bumperType: any = 1;
    problemText: string = null;
    loadingController: any = null;
    singleNav: any = null;
    resultsArray: any = {
        rightbubbles: 0,
        wrongbubbles: 0,
        rightTreats: 0,
        wrongTreats: 0,
        totalbubbles: 0
    };
    livesRemaining: boolean = false;
    resultDeclared: boolean = false;
    persistantLives: number = 10;
    perishableLives: number = 5;

    //Change bubble bottom position
    changeBottomIndex: number = 0;

    bubbles: any = [];
    bubblesSorted: any = [];
    declaredResultData: any = {};
    popIndex: number = 0;

    gamingIndex: number = 0;

    //Timer seconds
    timerNum: number = 60;
    points: any = 0;
    bumperPoints: number = 0;
    onEnded: boolean = false;
    onLeft: boolean = false;
    problemsOver: boolean = false;
    chosenColor: string = 'red';
    bubblePopRightIndex = 0;
    bubblePopWrongIndex = 0;
    level = 0;
    boxWidth = 0;
    boxHeight = 0;
    bubbleWidthParameter = 0;
    tableMoney = 5;
    levelIncreasing: boolean = true;
    freshlyLoaded: boolean = true;
    showLoading: boolean = false;

    staticTimer: number = 2;
    staticTimerBubbleFn: any;

    //For demo
    config: any = {};
    levelConfig: any = {
    };

    rummyConfig: any = {
        playerNames: [    
            'Player 1',
            'Player 2'
        ],

        /** number of cards to deal to each player when starting a game */
        numCardsToDeal: 7,

        /** if true, the tutorial is shown before the game starts */
        showTutorial: true,

        /** time per player turn, in ms */
        timePerTurn: 15 * 1000,

        /**
         * duration, in ms, te game will wait before setting the next player's turn, should the
         * player's turn time expire
         */
        timeExpiredDelay: 1 * 1000,

        /** set to false for production */
        debugCanControlAi: false,

        /** set to false for production */
        debugCanSeeOtherPlayersCards: false,

        /** set to false for production */
        debugDontShuffleDeck: false,

        /**
         * use this to control the order of cards in the deck
         * specify as many as you need, and the rest of the deck will be unaffected
         * cards are defined using an id with format '{value}{suit}' for example:
         * '4c', '10s', 'ah', 'kd'
         * values are 'a', '2', '3', ..., 'j', 'q', 'k'
         * suits are 'h', 'd', 'c', 's'
         * do not include the same id more than once
         * Set to null for production.
         * NOTE: CANNOT BE USED IN CONJUNCTION WITH DECK-RIGGING. In other words, if you want
         * to use this, set `isRiggedInPlayersFavor` to `null`.
         * @type {string[]}
         */
        debugDeckCardIds: [
        ],

        /**
         * When true, holding down the key will skip tutorial messages. No sense in getting
         * carpul tunnel by cliking thru all those messages when developing and testing.
         * Set to false for production
         * @type {boolean}
         */
        debugEnterKeyToSkipMessages: false,

        /**
         * When true, wait times during the toturial are skipped
         * Set to false for production
         * @type {boolean}
         */
        debugTutorialNoWaits: false,

        /** time, in ms, that the AI delays when making plays */
        aiSpeedMin: 500,
        aiSpeedMax: 2500,

        /**
         * Set to true to rig the deck in the player's favor. The rigger will use the
         * first sub array in the `riggedInPlayersFavorArrays` array.
         * 
         * Set to false to rig the deck in the ai's favor. The rigger will use the
         * second sub array in the `riggedInPlayersFavorArrays` array.
         * 
         * Set to null to disable deck-rigging altogether.
         * @type {?boolean}
         */
        isRiggedInPlayersFavor: false,

        /**
         * An array of two sub-arrays.
         * 
         * If a value is true in a sub-array, then the top card of the deck will be rigged
         * in the playter's favor. Otherwise, the value is rigged in the ai's favor.
         * @type {boolean[][]}
         */
        riggedInPlayersFavorArrays: [
            //player's favor
           [true],

           //ai's favor
           [false]
        ],

        /**
         * Time, in ms, that the game will automatically deal the opening cards. Otherwise,
         * set this to 0 to allow manual start by pressing the deal button.
         */
        autoDealDelay: 1500,

        /**
         * Specifies the language used in the tutorial.
         * Have a look at the `assets/game-rummy/data/tutorialSteps.json` file.
         * Those data blocks with a key of "message" are affected. The `value` property of
         * blocks will use the sub-object whose key that is identify by this setting.
         */
        tutorialLanguage: 'eng'
    };

    gameInstance: GameRummy;
    gameStyle: any = {
        width: '100%',
        height: '100%'
    };
    paramsGameId: any;

    constructor(public navCtrl: NavController, public navParams: NavParams, public platform: Platform, public menu: MenuController) {
        this.paramsGameId = navParams.get('paramsGameId');
    }

    ionViewDidLoad() {
        this.calculateBoxWidth();
        this.startNow(this.rummyConfig.startTimeSecs);
    }

    perfectCallback(gameInstance) {
        console.log('perfectCallback called in home.ts!');
        gameInstance.showPerfect();
    }

    addPointCallback(points) {
       console.log('Adding a point', points);
       this.points = this.points + points;
    }

    //===================================
    //---- BEGIN RUMMY-SPECIC GAME LOGIC
    //===================================

    rummyEngineListener() {
        this.gameInstance = new GameRummy(this.boxWidth, this.boxHeight);
        this.gameInstance.startGame(this.rummyConfig);

        this.gameInstance.listen('ready', this.onReady.bind(this));
        this.gameInstance.listen('gameOver', this.onGameOver.bind(this));
        this.gameInstance.listen('gameOverPlayerWonWholeThing',
            this.onGameOverPlayerWonWholeThing.bind(this));
        
        this.gameInstance.listen('gameStart', this.onGameStart.bind(this));
    }

    onGameOver(gameInstance) {
        console.log('game over callback!');
    }

    onGameOverPlayerWonWholeThing(gameInstance, winningPlayerIndex) {
        const playerName = this.rummyConfig.playerNames[winningPlayerIndex];
        console.log(`Player ${playerName ? playerName : winningPlayerIndex.toString()} wins!`);
        this.onEnd();
    }

    onGameStart(gameInstance) {
        console.log('game start callback!');
    }

    //===================================
    //---- END RUMMY-SPECIC GAME LOGIC
    //===================================

    onReady(gameInstance) {
        console.log('the game has finished loading and is ready to play');
    }

    startNow(num) {
        this.gameStyle = {
            width: this.boxWidth + 'px',
            height: this.boxHeight + 'px'
        };

        switch (this.paramsGameId) {
            case 19:
                this.rummyEngineListener();
                break;
        }

        //Information to be used!
        this.showAlertMessage = false;
        this.onEnded = false;
        this.onLeft = false;

        this.timerNum = num; //Value of the timer
        this.perishableLives = 5; //Initial per contest lives
        this.persistantLives = 0; //Final bought lives

        this.points = 0; //Points scored

        this.checkLivesAvailable(); //This function checks if lives are available or not! It ends your contest if your lives are over.
    }

    checkLivesAvailable() {
        if (this.perishableLives <= 0 && this.persistantLives <= 0) {
            this.livesRemaining = false;
            this.onEnd();
        } else {
            this.livesRemaining = true;
        }
    }

    calculateBoxWidth() {
        this.boxWidth = this.platform.width();
        this.boxHeight = this.platform.height();

        // if (this.isIos) {
        //     this.boxHeight = this.boxHeight - 50;
        // }
    }

    problemsOverFn(problemText?) {
        this.problemText = 'Some problem occurred, Please try again!';
        if (problemText) {
            this.problemText = problemText;
        }
        this.timerNum = 0;
        this.problemsOver = true;
        this.clearTimer();
    }

    nextStage() {
        this.tutorialStage = this.tutorialStage + 1;

        if (this.tutorialStage == 4) {
            this.perishableLives = 0;
        }
        if (this.tutorialStage == 5) {
            this.persistantLives = 0;
        }

        if (this.tutorialStage == this.finalStageNumber + 1) {
            this.dismiss();
        }
    }

    chooseLevel() {
        if (this.level < this.config.levels + 1 && this.levelIncreasing) {
            this.level++;
        }

        if (this.level > 0 && !this.levelIncreasing) {
            this.level--;
        }

        if (this.level == 0) {
            this.level = 2;
            this.levelIncreasing = true;
        }

        if (this.level == this.config.levels + 1) {
            this.level = this.config.levels - 1;
            this.levelIncreasing = false;
        }

        this.levelConfig = this.config[this.level];
    }

    generateRandom(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    dismiss() {
        this.navCtrl.pop();
    }

    confirmDismissal() {
        this.showAlertMessage = false;
        this.onEnded = true;
        this.onLeft = true;
        this.clearTimer();

        setTimeout(() => {
            this.clearTimer();
        }, 1000);

        this.menu.enable(true);
    }

    clearTimer() {
        if (this.staticTimerFn) {
            clearTimeout(this.staticTimerFn);
            this.staticTimerFn = null;
        }
    }

    ionViewDidEnter() {
        this.menu.enable(false);
    }

    ionViewCanLeave() {
        if (!this.onEnded && !this.problemsOver && !this.isTutorial) {
        } else {
            this.menu.enable(true);
        }
    }

    getBubbleParamsAccToWidth() {
        let width = this.boxWidth; //width of box
        let singleBoxParams = {};
        this.levelConfig.box.forEach(singleBox => {
            if (singleBox.minWidth) {
                if (singleBox.maxWidth) {
                    //Check width between minWidth and maxWidth
                    if (width >= singleBox.minWidth && width <= singleBox.maxWidth) {
                        singleBoxParams = singleBox;
                    }
                } else {
                    //Check width >= minWidth
                    if (width >= singleBox.minWidth) {
                        singleBoxParams = singleBox;
                    }
                }
            } else {
                //Check width between 0 and maxWidth
                if (width >= 0 && width <= singleBox.maxWidth) {
                    singleBoxParams = singleBox;
                }
            }
        });

        return singleBoxParams;
    }

    _arrayRandom(len, min, max, unique) {
        var len = len ? len : 10,
            min = min !== undefined ? min : 1,
            max = max !== undefined ? max : 100,
            unique = unique ? unique : false,
            toReturn = [],
            tempObj = {},
            i = 0;

        if (unique === true) {
            for (; i < len; i++) {
                var randomInt = Math.floor(Math.random() * (max - min + 1)) + min;
                if (tempObj['key_' + randomInt] === undefined) {
                    tempObj['key_' + randomInt] = randomInt;
                    toReturn.push(randomInt);
                } else {
                    i--;
                }
            }
        } else {
            for (; i < len; i++) {
                toReturn.push(Math.floor(Math.random() * (max - min + min)));
            }
        }

        return toReturn;
    }

    removeLoading() {
        if (this.showLoading) {
            this.showLoading = false;
            this.loadingController.dismiss();
        }
    }

    onTimerFinished() {
        this.onEnd();
    }

    onEnd() {
        if (!this.onEnded) {
            this.gameInstance.endGame();
        }
        this.clearTimer();
        this.onEnded = true;
        this.onLeft = true;

        if (this.isBumper || this.isDemo) {
            if (this.livesRemaining) {
                this.dismiss();
            }
        } else {
            if (this.livesRemaining) {
            }
        }
    }
}
